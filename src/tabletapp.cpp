#include <QtWidgets>
#include "tabletapp.h"

bool TabletApplication::event(QEvent* evt)
{
    if (evt->type() == QEvent::TabletEnterProximity || evt->type() == QEvent::TabletLeaveProximity) 
	{
        mPaintArea->setTabletDevice(static_cast<QTabletEvent*>(evt)->device());
        return true;
    }
    return QApplication::event(evt);
}

void TabletApplication::setCanvas(PaintArea* paintArea) 
{ 
	mPaintArea = paintArea; 
}
