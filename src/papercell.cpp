#define _TR1_C99

#include "papercell.h"
#include <math.h>


Cell::Cell()
{
	mHeight = 0.0;

	mColor = new unsigned char[4];
	mAddedColor_R = new unsigned char[4];
	mAddedColor_T = new unsigned char[4];

	mIsMarginalCell = false;
	mIsInWetCells = false;
	mIsInNeighbWetCells = false;
	mMarkedToDry = false;
	mIsContour = false;
	mGotInk = false;

	mWetnessFactor = 100;
	mHighestNeighbWater = -1;
	mDry_treshold = 20;
	mMeanFlowWetness = 0;
	mIter = 0;
	mStrokeNumber = 0;
    mLastUserUpdate = std::chrono::system_clock::now();

	mActualWetness = 0;

	mR_r = 0.0;
	mR_g = 0.0;
	mR_b = 0.0;
	mT_r = 0.5;
	mT_g = 0.5;
	mT_b = 0.5;
}

float Cell::fmax(float f1, float f2)
{
	if(f1 > f2)
	{
		return f1;
	}
	else
	{
		return f2;
	}
	return f1;
}

float Cell::fmin(float f1, float f2)
{
	if(f1 < f2)
	{
		return f1;
	}
	else
	{
		return f2;
	}
	return f1;
}

Cell::Cell(bool isMarginalCell, vector<Cell*> neighbCells, float height, unsigned char* driedColor, unsigned char* color, int dryingSpeed)
{
	mIsMarginalCell = isMarginalCell;
	mNeighbCells = neighbCells;
	mHeight = height;

	mColor = color; 
	mActualWetness = 0;
}

Cell::Cell(const Cell& rhs)
{
	mHeight = rhs.mHeight;
	mActualWetness = rhs.mActualWetness;

	mColor = rhs.mColor; 
	mAddedColor_R = rhs.mAddedColor_R;
	mAddedColor_T = rhs.mAddedColor_T;
	bool mIsMarginalCell = rhs.mIsMarginalCell;
	mNeighbCells = rhs.mNeighbCells;
}

Cell::~Cell()
{
	delete mColor; 
}

Cell& Cell::operator= (const Cell& rhs)
{
	if(this != &rhs) // protect against invalid self-assignment
	{
		// 1: allocate new memory and copy the elements
		mColor = new unsigned char[4]; 
 
		// 2: deallocate old memory
		delete rhs.mColor; 

		// 3: assign the new memory to the object
		mHeight = rhs.mHeight;
		mIsMarginalCell = rhs.mIsMarginalCell;
		mNeighbCells = rhs.mNeighbCells;
		mHeight = rhs.mHeight;

		mColor = rhs.mColor; 
		mAddedColor_R = rhs.mAddedColor_R;
		mAddedColor_T = rhs.mAddedColor_T;

		mActualWetness = rhs.mActualWetness;
		mNeighbCells = rhs.mNeighbCells;
	}
	// by convention, always return *this
	return *this;
}

void Cell::applyColor()
{

}

void Cell::addWetness( int addWetness)
{	
	mActualWetness = max(0, min(mActualWetness + addWetness, 255));
	
	if(mActualWetness > mDry_treshold)
	{
		mIsInWetCells = true;
	}
}

void Cell::update( int addedWetness, bool capillary, bool gotInk)
{	
	if(addedWetness < 0)
		return;

	if(addedWetness > 255)
	{
		addedWetness = 255;
	}

	if(	mAddedColor_T[0] > 0 || mAddedColor_T[1] > 0 || mAddedColor_T[2] > 0
	 || mAddedColor_R[0] > 0 || mAddedColor_R[1] > 0 || mAddedColor_R[2] > 0 )
	{
		if(!capillary)
		{
            /*
			SYSTEMTIME time;
			GetSystemTime(&time);
			mLastUserUpdate = (time.wSecond * 1000) + time.wMilliseconds;
            */
            mLastUserUpdate = std::chrono::system_clock::now();

			mMarkedToDry = false;
		}

		// ink was added
		mR_b1 = float(mAddedColor_R[0]) / 255.0;
		mR_g1 = float(mAddedColor_R[1]) / 255.0;
		mR_r1 = float(mAddedColor_R[2]) / 255.0;

		mT_b1 = float(mAddedColor_T[0]) / 255.0;
		mT_g1 = float(mAddedColor_T[1]) / 255.0;
		mT_r1 = float(mAddedColor_T[2]) / 255.0;

		addWetness(addedWetness);
		addInk( mT_r1, mT_g1, mT_b1, mR_r1, mR_g1, mR_b1, capillary);

		if(gotInk)
			mGotInk = true;

		mAddedColor_T[0] = 0;
		mAddedColor_T[1] = 0;
		mAddedColor_T[2] = 0;

		mAddedColor_R[0] = 0;
		mAddedColor_R[1] = 0;
		mAddedColor_R[2] = 0;
	}
	else
	{
		addedWetness = 0;
	}
}

void Cell::addInk(float addT_r, float addT_g, float addT_b, float addR_r, float addR_g, float addR_b, bool capillary)
{
	if(mWetnessFactor > 15)
	{
		mR_b2 = mDried_R_b;
		mR_g2 = mDried_R_g;
		mR_r2 = mDried_R_r;

		mT_b2 = mDried_T_b;
		mT_g2 = mDried_T_g;
		mT_r2 = mDried_T_r;
	}
	else
	{
		mDried_R_b = mR_b2 = mR_b;
		mDried_R_g = mR_g2 = mR_g;
		mDried_R_r = mR_r2 = mR_r;

		mDried_T_b = mT_b2 = mT_b;
		mDried_T_g = mT_g2 = mT_g;
		mDried_T_r = mT_r2 = mT_r;
	}

	if(addR_b * mR_b2 == 1.0) mR_b2 -= 0.01;
	if(addR_g * mR_g2 == 1.0) mR_g2 -= 0.01;
	if(addR_r * mR_r2 == 1.0) mR_r2 -= 0.01;

	mT_b = ( (addT_b * mT_b2) / (1.0 - addR_b * mR_b2) );
	mT_g = ( (addT_g * mT_g2) / (1.0 - addR_g * mR_g2) );
	mT_r = ( (addT_r * mT_r2) / (1.0 - addR_r * mR_r2) );

	if( addR_b * mR_b2 == 1.0 ) mR_b2 -= 0.01;
	if( addR_g * mR_g2 == 1.0 ) mR_g2 -= 0.01;
	if( addR_r * mR_r2 == 1.0 ) mR_r2 -= 0.01;

	mR_b = ( addR_b + ((addT_b * addT_b * mR_b2) / (1.0 - addR_b * mR_b2)) );
	mR_g = ( addR_g + ((addT_g * addT_g * mR_g2) / (1.0 - addR_g * mR_g2)) );
	mR_r = ( addR_r + ((addT_r * addT_r * mR_r2) / (1.0 - addR_r * mR_r2)) );

	mColor[0] = max(0.0, min( 255.0, mR_b * 255.0));
	mColor[1] = max(0.0, min( 255.0, mR_g * 255.0));
	mColor[2] = max(0.0, min( 255.0, mR_r * 255.0));
}

void Cell::doMeanFlowWater( vector<Cell*> &wetNeighbCells, bool addNeighb )
{
	mHighestNeighbWater = -1;
	//mActualWetness = max(0, mActualWetness - 10);
	int highest_wetness = mActualWetness;
	int wetness_sum = mActualWetness;

	for(int i = 0; i < mNeighbCells.size(); i++)	
	{
		wetness_sum += mNeighbCells[i]->mActualWetness;
		if(highest_wetness < mNeighbCells[i]->mActualWetness && !mNeighbCells[i]->mIsMarginalCell)
		{
			highest_wetness = mNeighbCells[i]->mActualWetness;
			mHighestNeighbWater = i;
		}
		if(!mNeighbCells[i]->mIsMarginalCell && !mNeighbCells[i]->mIsContour && !mNeighbCells[i]->mIsInNeighbWetCells && addNeighb)
		{
			int left, right = i;
			if( i - 1 < 0 ) {left = 7;} else {left = i - 1;}
			if( i + 1 > 7 ) {right = 0;} else {right = i + 1;}
			if(!(mNeighbCells[left]->mIsContour && mNeighbCells[right]->mIsContour))
			{
				mNeighbCells[i]->mIsInNeighbWetCells = true;
				wetNeighbCells.push_back(mNeighbCells[i]);
			}
		}
	}
	mMeanFlowWetness = wetness_sum / 9;
}

bool Cell::doMeanFlow( vector<Cell*> &markedToDryCells )
{
	//diffusion rate threshold
	float diff_rate_treshold = 7.0;
	float diff_rate = abs(mActualWetness - mMeanFlowWetness);
	mActualWetness = mMeanFlowWetness;

	//mHighestNeighbWater is the index of the neighb cell with the highest amount of water
	if(mHighestNeighbWater != -1 && mNeighbCells[mHighestNeighbWater]->mActualWetness > mActualWetness)
	{	
		mAddedColor_R[0] = mNeighbCells[mHighestNeighbWater]->mR_b1 * 255.0;
		mAddedColor_R[1] = mNeighbCells[mHighestNeighbWater]->mR_g1 * 255.0;
		mAddedColor_R[2] = mNeighbCells[mHighestNeighbWater]->mR_r1 * 255.0;

		mAddedColor_T[0] = mNeighbCells[mHighestNeighbWater]->mT_b1 * 255.0;
		mAddedColor_T[1] = mNeighbCells[mHighestNeighbWater]->mT_g1 * 255.0;
		mAddedColor_T[2] = mNeighbCells[mHighestNeighbWater]->mT_r1 * 255.0;

		//mGotInk = true;
		mStrokeNumber = mNeighbCells[mHighestNeighbWater]->mStrokeNumber;

		update(0, true, true);
	}

    /*
	SYSTEMTIME time;
	GetSystemTime(&time);
	WORD now = (time.wSecond * 1000) + time.wMilliseconds;
    */

    int foo = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - mLastUserUpdate).count();

    if(mActualWetness <= mDry_treshold || diff_rate < diff_rate_treshold || foo > 12 * mWetnessFactor)
	{
		if(!mMarkedToDry)
		{
			mMarkedToDry = true;
			markedToDryCells.push_back(this);
			//mColor[0] = 0;
			//mColor[1] = 0;
			//mColor[2] = 0;
		}
		return stopSimulation();
	}
	else
	{
		return true;
	}
}

void Cell::computeKubelkaVars(float wetness)
{
	float mc_b = ma_b * sinh(mb_b * mS_b * wetness) + mb_b * cosh( mb_b * mS_b * wetness);
	float mc_g = ma_g * sinh(mb_g * mS_g * wetness) + mb_g * cosh( mb_g * mS_g * wetness);
	float mc_r = ma_r * sinh(mb_r * mS_r * wetness) + mb_r * cosh( mb_r * mS_r * wetness);

	mR_b1 = sinh(mb_b * mS_b * wetness) / mc_b;
	mR_g1 = sinh(mb_g * mS_g * wetness) / mc_g;
	mR_r1 = sinh(mb_r * mS_r * wetness) / mc_r;

	mT_b1 = mb_b / mc_b;
	mT_g1 = mb_g / mc_g;
	mT_r1 = mb_r / mc_r;
}

void Cell::dry()
{
	//actualize dry vars
	mDried_R_b = mR_b;
	mDried_R_g = mR_g;
	mDried_R_r = mR_r;

	mDried_T_b = mT_b;
	mDried_T_g = mT_g;
	mDried_T_r = mT_r;

	mActualWetness = 0;
	mGotInk = false;
	//mColor[0] = 0;
	mMarkedToDry = false;
}

bool Cell::stopSimulation()
{
	mIsInWetCells = false;
	mIsInNeighbWetCells = false;
	return false;
}
