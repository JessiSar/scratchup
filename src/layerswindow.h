#pragma once

#include <QWidget>
#include <qgridlayout.h>
#include <qpushbutton.h>
#include <qtoolbutton.h>
#include <qgroupbox.h>
#include <qslider.h>
#include <qlabel.h>
#include <qspinbox.h>
#include <qcombobox.h>
#include <qlistwidget.h>
#include <qstack.h>

#include <iostream>
#include <fstream>

class LayersWindow : public QWidget
{
Q_OBJECT

public:
	LayersWindow(QWidget *parent = 0);
	~LayersWindow();

	void newProject();
	void newLayer(bool newImage);
	void setOpacityView(int opacity);
	void setMixingModeView(int index);

public slots:
	void newLayerSlot();
	void newSimulationLayerSlot();

protected:

private:
	std::ofstream mLogFile;

	QVBoxLayout* mLayersLayout;
	QHBoxLayout* mNewLayerLayout;
	QHBoxLayout* mLayerOptionsLayout;
	QHBoxLayout* mLayerOpacityLayout;
	QHBoxLayout* mLayersListLayout;
	//
	QGroupBox* mNewLayerGB;
	QGroupBox* mLayerOptionsGB;
	QGroupBox* mLayerOpacityGB;
	//
	QToolButton* mNewLayerButton;
	QToolButton* mNewPaperLayerButton;
	QToolButton* mRestoreLayerButton;

	QToolButton* mDeleteLayerButton;
	QToolButton* mUpButton;
	QToolButton* mDownButton;

	QComboBox* mMixingModeBox;

	QSlider* mLayerOpacitySlider;
	QSpinBox* mLayerOpacitySpinBox;

	QListWidget* mLayerList;

	int mSelectedLayer;
	int mLayerCount;
	int mLayerCountName;
	QStack<QString>* mLayerNameStack;

	void keyPressEvent(QKeyEvent* evt);

private slots:
	void restoreLayer();
	void deleteActualLayer();
	void raiseActualLayer();
	void lowerActualLayer();
	void layerClicked(QListWidgetItem*);
	void layerChanged(QListWidgetItem*);
    void quit();
};
