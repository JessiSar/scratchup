#pragma once

#include <vector>
#include <fstream>
#include <qcolor.h>
#include <qtimer.h>
#include <iostream>
#include <time.h>
//#include <windows.h>
#include <chrono>

using namespace std;

class Cell : public QObject
{
Q_OBJECT

public:
	Cell();
	Cell(const Cell& rhs);
	Cell(bool isMarginalCell, vector<Cell*> mNeighbCells, float height, unsigned char* driedColor, unsigned char* color, int dryingSpeed);
	~Cell();

	Cell& operator= (const Cell& rhs);
	float fmin(float f1, float f2);
	float fmax(float f1, float f2);

	void update(int addWetness, bool capillary, bool gotInk);
	void addInk(float addT_r, float addT_g, float addT_b, float addR_r, float addR_g, float addR_b, bool capillary);
	void doMeanFlowWater( vector<Cell*> &wetNeighbCells, bool addNeighb );
	bool doMeanFlow( vector<Cell*> &markToDryCells );
	bool stopSimulation();
	void computeKubelkaVars( float wetness );
	void addWetness( int addWetness);
	void applyColor();
	void dry();

	float mHeight;
	int mActualWetness;
	int mIter;
	int mMeanFlowWetness;
	int mHighestNeighbWater;
	int mWetnessFactor;
	int mStrokeNumber;
	bool mIsMarginalCell;
	bool mIsInWetCells;
	bool mIsInNeighbWetCells;
	bool mIsContour;
	bool mGotInk;
	int mDry_treshold;
	bool mMarkedToDry;

    std::chrono::time_point<std::chrono::system_clock> mLastUserUpdate;

	unsigned char* mColor; 
	unsigned char* mAddedColor_R; 
	unsigned char* mAddedColor_T; 

	vector<Cell*> mNeighbCells;
	vector<int> mNeighbWaterDiffs;

	float mR_r;
	float mR_g;
	float mR_b;

	float mT_r;
	float mT_g;
	float mT_b;

	float mDried_R_r;
	float mDried_R_g;
	float mDried_R_b;

	float mDried_T_r;
	float mDried_T_g;
	float mDried_T_b;

	float mR_r1;
	float mR_g1;
	float mR_b1;

	float mT_r1;
	float mT_g1;
	float mT_b1;

	//last save
	float mLast_R_r1;
	float mLast_R_g1;
	float mLast_R_b1;

	float mLast_T_r1;
	float mLast_T_g1;
	float mLast_T_b1;

	float mR_r2;
	float mR_g2;
	float mR_b2;

	float mT_r2;
	float mT_g2;
	float mT_b2;

	//K&M vars
	float mK_r;
	float mK_g;
	float mK_b;

	float mS_r;
	float mS_g;
	float mS_b;

	float ma_r;
	float ma_g;
	float ma_b;

	float mb_r;
	float mb_g;
	float mb_b;
};
