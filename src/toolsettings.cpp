#include "toolsettings.h"
#include "mainwindow.h"

ToolSettings::ToolSettings(QWidget *parent) : QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	this->setWindowFlags(Qt::Tool);
	this->setWindowTitle(tr("Tool Window"));

	//create buttons
	mPenButton = new QToolButton(this);
	mPenButton->setCheckable(true);
	mPenButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mPenButton->setIcon(QIcon("images/toolIcons/pen.png"));
	mPenButton->setIconSize(QSize(22, 22));
	mPenButton->setToolTip("Pen Tool");

	mFeltPenButton = new QToolButton(this);
	mFeltPenButton->setCheckable(true);
	mFeltPenButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mFeltPenButton->setIcon(QIcon("images/toolIcons/feltPen.png"));
	mFeltPenButton->setIconSize(QSize(22, 22));
	mFeltPenButton->setToolTip("Felt Pen Tool");

	mEraserButton = new QToolButton(this);
	mEraserButton->setCheckable(true);
	mEraserButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mEraserButton->setIcon(QIcon("images/toolIcons/eraser.png"));
	mEraserButton->setIconSize(QSize(22, 22));
	mEraserButton->setToolTip("Eraser Tool");

	mFillButton = new QToolButton(this);
	mFillButton->setCheckable(true);
	mFillButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mFillButton->setIcon(QIcon("images/toolIcons/fill.png"));
	mFillButton->setIconSize(QSize(22, 22));
	mFillButton->setToolTip("Fill Tool");

	mExpressiveButton = new QToolButton(this);
	mExpressiveButton->setCheckable(true);
	mExpressiveButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mExpressiveButton->setIcon(QIcon("images/toolIcons/expressive.png"));
	mExpressiveButton->setIconSize(QSize(22, 22));
	mExpressiveButton->setToolTip("Expressive Art Tool");

	mLineButton = new QToolButton(this);
	mLineButton->setCheckable(true);
	mLineButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mLineButton->setIcon(QIcon("images/toolIcons/line.png"));
	mLineButton->setIconSize(QSize(22, 22));
	mLineButton->setToolTip("Line Tool");

	mSplineButton = new QToolButton(this);
	mSplineButton->setCheckable(true);
	mSplineButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mSplineButton->setIcon(QIcon("images/toolIcons/spline.png"));
	mSplineButton->setIconSize(QSize(22, 22));
	mSplineButton->setToolTip("Spline Tool");

	mRectButton = new QToolButton(this);
	mRectButton->setCheckable(true);
	mRectButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mRectButton->setIcon(QIcon("images/toolIcons/rect.png"));
	mRectButton->setIconSize(QSize(22, 22));
	mRectButton->setToolTip("Rectangle Tool");

	mEllipseButton = new QToolButton(this);
	mEllipseButton->setCheckable(true);
	mEllipseButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mEllipseButton->setIcon(QIcon("images/toolIcons/ellipse.png"));
	mEllipseButton->setIconSize(QSize(22, 22));
	mEllipseButton->setToolTip("Ellipse Tool"); 

	mForegroundColor = new QPushButton(tr(""));
	mForegroundColor->setStyleSheet("QPushButton { background-color: blue; }");
	mBackgroundColor = new QPushButton(tr(""));
	mBackgroundColor->setStyleSheet("QPushButton { background-color: yellow; }");

	mKubelkaColorW = new QPushButton(tr(""));
	mKubelkaColorW->setStyleSheet("QPushButton { background-color: blue; }");

	setmKubelkaColorW(QColor( 165, 14, 83));
	mKubelkaColorB = new QPushButton(tr(""));
	mKubelkaColorB->setStyleSheet("QPushButton { background-color: yellow; }");
	setmKubelkaColorB(QColor( 10, 1, 4));

	mFeltPenColorA = new QToolButton(this);
	mFeltPenColorA->setCheckable(true);
	mFeltPenColorA->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mFeltPenColorA->setIcon(QIcon("images/toolIcons/feltPenColorA.png"));
	mFeltPenColorA->setIconSize(QSize(22, 22));
	mFeltPenColorA->setToolTip("Quinacridone Rose");

	mFeltPenColorB = new QToolButton(this);
	mFeltPenColorB->setCheckable(true);
	mFeltPenColorB->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mFeltPenColorB->setIcon(QIcon("images/toolIcons/feltPenColorB.png"));
	mFeltPenColorB->setIconSize(QSize(22, 22));
	mFeltPenColorB->setToolTip("Indian Red");

	mFeltPenColorC = new QToolButton(this);
	mFeltPenColorC->setCheckable(true);
	mFeltPenColorC->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mFeltPenColorC->setIcon(QIcon("images/toolIcons/feltPenColorC.png"));
	mFeltPenColorC->setIconSize(QSize(22, 22));
	mFeltPenColorC->setToolTip("Cadmium Yellow");

	mFeltPenColorD = new QToolButton(this);
	mFeltPenColorD->setCheckable(true);
	mFeltPenColorD->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mFeltPenColorD->setIcon(QIcon("images/toolIcons/feltPenColorD.png"));
	mFeltPenColorD->setIconSize(QSize(22, 22));
	mFeltPenColorD->setToolTip("Hookers Green");

	mFeltPenColorE = new QToolButton(this);
	mFeltPenColorE->setCheckable(true);
	mFeltPenColorE->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mFeltPenColorE->setIcon(QIcon("images/toolIcons/feltPenColorE.png"));
	mFeltPenColorE->setIconSize(QSize(22, 22));
	mFeltPenColorE->setToolTip("Cerulean Blue");

	mFeltPenColorF = new QToolButton(this);
	mFeltPenColorF->setCheckable(true);
	mFeltPenColorF->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mFeltPenColorF->setIcon(QIcon("images/toolIcons/feltPenColorF.png"));
	mFeltPenColorF->setIconSize(QSize(22, 22));
	mFeltPenColorF->setToolTip("Burnt Umber");

	mFeltPenColorG = new QToolButton(this);
	mFeltPenColorG->setCheckable(true);
	mFeltPenColorG->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mFeltPenColorG->setIcon(QIcon("images/toolIcons/feltPenColorG.png"));
	mFeltPenColorG->setIconSize(QSize(22, 22));
	mFeltPenColorG->setToolTip("Cadmium Red");

	mFeltPenColorH = new QToolButton(this);
	mFeltPenColorH->setCheckable(true);
	mFeltPenColorH->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mFeltPenColorH->setIcon(QIcon("images/toolIcons/feltPenColorH.png"));
	mFeltPenColorH->setIconSize(QSize(22, 22));
	mFeltPenColorH->setToolTip("Brilliant Orange");

	mFeltPenColorI = new QToolButton(this);
	mFeltPenColorI->setCheckable(true);
	mFeltPenColorI->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mFeltPenColorI->setIcon(QIcon("images/toolIcons/feltPenColorI.png"));
	mFeltPenColorI->setIconSize(QSize(22, 22));
	mFeltPenColorI->setToolTip("Hansa Yellow");

	mFeltPenColorJ = new QToolButton(this);
	mFeltPenColorJ->setCheckable(true);
	mFeltPenColorJ->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mFeltPenColorJ->setIcon(QIcon("images/toolIcons/feltPenColorJ.png"));
	mFeltPenColorJ->setIconSize(QSize(22, 22));
	mFeltPenColorJ->setToolTip("Phthalo Green");

	mFeltPenColorK = new QToolButton(this);
	mFeltPenColorK->setCheckable(true);
	mFeltPenColorK->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mFeltPenColorK->setIcon(QIcon("images/toolIcons/feltPenColorK.png"));
	mFeltPenColorK->setIconSize(QSize(22, 22));
	mFeltPenColorK->setToolTip("French Ultramarine");

	mFeltPenColorL = new QToolButton(this);
	mFeltPenColorL->setCheckable(true);
	mFeltPenColorL->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mFeltPenColorL->setIcon(QIcon("images/toolIcons/feltPenColorL.png"));
	mFeltPenColorL->setIconSize(QSize(22, 22));
	mFeltPenColorL->setToolTip("Interference Lilac");

	mFeltPenChoseColorLayout = new QGridLayout();
	mFeltPenChoseColorLayout->addWidget(mFeltPenColorA, 0, 0);
	mFeltPenChoseColorLayout->addWidget(mFeltPenColorB, 0, 1);
	mFeltPenChoseColorLayout->addWidget(mFeltPenColorC, 0, 2);
	mFeltPenChoseColorLayout->addWidget(mFeltPenColorD, 0, 3);
	mFeltPenChoseColorLayout->addWidget(mFeltPenColorE, 1, 0);
	mFeltPenChoseColorLayout->addWidget(mFeltPenColorF, 1, 1);
	mFeltPenChoseColorLayout->addWidget(mFeltPenColorG, 1, 2);
	mFeltPenChoseColorLayout->addWidget(mFeltPenColorH, 1, 3);
	mFeltPenChoseColorLayout->addWidget(mFeltPenColorI, 2, 0);
	mFeltPenChoseColorLayout->addWidget(mFeltPenColorJ, 2, 1);
	mFeltPenChoseColorLayout->addWidget(mFeltPenColorK, 2, 2);
	mFeltPenChoseColorLayout->addWidget(mFeltPenColorL, 2, 3);

	//create button connections
	connect( mPenButton, SIGNAL( clicked() ), this, SLOT( penButtonClicked() ) );
	connect( mFeltPenButton, SIGNAL( clicked() ), this, SLOT( feltPenButtonClicked() ) );
	connect( mEraserButton, SIGNAL( clicked() ), this, SLOT( eraserButtonClicked() ) );
	connect( mFillButton, SIGNAL( clicked() ), this, SLOT( fillButtonClicked() ) );
	connect( mExpressiveButton, SIGNAL( clicked() ), this, SLOT( exprButtonClicked() ) );
	connect( mLineButton, SIGNAL( clicked() ), this, SLOT( lineButtonClicked() ) );
	connect( mSplineButton, SIGNAL( clicked() ), this, SLOT( splineButtonClicked() ) );
	connect( mRectButton, SIGNAL( clicked() ), this, SLOT( rectButtonClicked() ) );
	connect( mEllipseButton, SIGNAL( clicked() ), this, SLOT( ellipseButtonClicked() ) );

	connect( mForegroundColor, SIGNAL( clicked() ), parent, SLOT( setToolColorFG() ) );
	connect( mBackgroundColor, SIGNAL( clicked() ), parent, SLOT( setToolColorBG() ) );
	
	connect( mKubelkaColorW, SIGNAL( clicked() ), parent, SLOT( setmFeltpenColorDialogW() ) );
	connect( mKubelkaColorB, SIGNAL( clicked() ), parent, SLOT( setmFeltpenColorDialogB() ) );

	connect( mFeltPenColorA, SIGNAL( clicked() ), this, SLOT( feltPenColorAClicked() ) );
	connect( mFeltPenColorB, SIGNAL( clicked() ), this, SLOT( feltPenColorBClicked() ) );
	connect( mFeltPenColorC, SIGNAL( clicked() ), this, SLOT( feltPenColorCClicked() ) );
	connect( mFeltPenColorD, SIGNAL( clicked() ), this, SLOT( feltPenColorDClicked() ) );
	connect( mFeltPenColorE, SIGNAL( clicked() ), this, SLOT( feltPenColorEClicked() ) );
	connect( mFeltPenColorF, SIGNAL( clicked() ), this, SLOT( feltPenColorFClicked() ) );
	connect( mFeltPenColorG, SIGNAL( clicked() ), this, SLOT( feltPenColorGClicked() ) );
	connect( mFeltPenColorH, SIGNAL( clicked() ), this, SLOT( feltPenColorHClicked() ) );
	connect( mFeltPenColorI, SIGNAL( clicked() ), this, SLOT( feltPenColorIClicked() ) );
	connect( mFeltPenColorJ, SIGNAL( clicked() ), this, SLOT( feltPenColorJClicked() ) );
	connect( mFeltPenColorK, SIGNAL( clicked() ), this, SLOT( feltPenColorKClicked() ) );
	connect( mFeltPenColorL, SIGNAL( clicked() ), this, SLOT( feltPenColorLClicked() ) );

	//Pen Width Settings Widgets
	mToolWidthSlider = new QSlider(Qt::Horizontal);
	mToolWidthSlider->setMinimum(1);
	mToolWidthSlider->setMaximum(50);
	//mPenWidthSlider->setTickPosition(QSlider::TicksBothSides);
	mToolWidthSlider->setTickInterval(10);
	mToolWidthSlider->setValue(5);

	mToolWidthSpinBox = new QSpinBox(this);
	mToolWidthSpinBox->setMinimum(1);
	mToolWidthSpinBox->setMaximum(50);
	mToolWidthSpinBox->setValue(5);
	
	mToolWidthLayout = new QHBoxLayout;
	mToolWidthLayout->addWidget(mToolWidthSlider);
	mToolWidthLayout->addWidget(mToolWidthSpinBox);

	mToolWidthGB = new QGroupBox("Tool's stroke width");
	mToolWidthGB->setLayout(mToolWidthLayout);

	connect( mToolWidthSlider, SIGNAL( valueChanged(int) ), parent, SLOT( setToolWidth(int) ) );
	connect( mToolWidthSlider, SIGNAL( valueChanged(int) ), mToolWidthSpinBox, SLOT( setValue(int) ) );
	connect( mToolWidthSpinBox, SIGNAL( valueChanged(int) ), mToolWidthSlider, SLOT( setValue(int) ) );

	//Pen Opacity Settings Widgets
	mToolOpacitySlider = new QSlider(Qt::Horizontal);
	mToolOpacitySlider->setMinimum(1);
	mToolOpacitySlider->setMaximum(100);
	//mPenOpacitySlider->setTickPosition(QSlider::TicksBothSides);
	mToolOpacitySlider->setTickInterval(10);
	mToolOpacitySlider->setValue(100);

	mToolOpacitySpinBox = new QSpinBox(this);
	mToolOpacitySpinBox->setMinimum(1);
	mToolOpacitySpinBox->setMaximum(100);
	mToolOpacitySpinBox->setValue(100);
	
	mToolOpacityLayout = new QHBoxLayout;
	mToolOpacityLayout->addWidget(mToolOpacitySlider);
	mToolOpacityLayout->addWidget(mToolOpacitySpinBox);

	mToolOpacityGB = new QGroupBox("Tool's Opacity");
	mToolOpacityGB->setLayout(mToolOpacityLayout);

	connect( mToolOpacitySlider, SIGNAL( valueChanged(int) ), parent, SLOT( setToolOpacity(int) ) );
	connect( mToolOpacitySlider, SIGNAL( valueChanged(int) ), mToolOpacitySpinBox, SLOT( setValue(int) ) );
	connect( mToolOpacitySpinBox, SIGNAL( valueChanged(int) ), mToolOpacitySlider, SLOT( setValue(int) ) );

	QLabel* pressureLabel = new QLabel("Pressure");
	QLabel* tiltLabel = new QLabel("Tilt");
	QLabel* widthLabel = new QLabel("Width");
	QLabel* opacityLabel = new QLabel("Opacity");
	QLabel* colorLabel = new QLabel("Color");
	mPressureWidth = new QCheckBox;
	connect( mPressureWidth, SIGNAL( stateChanged(int) ), this, SLOT( pressureWidthCBoxClicked(int) ) );
	mPressureOpacity = new QCheckBox;
	mPressureColor = new QCheckBox;
	connect( mPressureColor, SIGNAL( stateChanged(int) ), this, SLOT( pressureColorCBoxClicked(int) ) );
	mTiltWidth = new QCheckBox;
	connect( mTiltWidth, SIGNAL( stateChanged(int) ), this, SLOT( tiltWidthCBoxClicked(int) ) );
	mTiltOpacity = new QCheckBox;
	mTiltColor = new QCheckBox;
	connect( mTiltColor, SIGNAL( stateChanged(int) ), this, SLOT( tiltColorCBoxClicked(int) ) );
	QVBoxLayout* tabletLableLayout = new QVBoxLayout;
	tabletLableLayout->addStretch();
	tabletLableLayout->addWidget(pressureLabel);
	tabletLableLayout->addWidget(tiltLabel);
	QVBoxLayout* tabletWidthLayout = new QVBoxLayout;
	tabletWidthLayout->addWidget(widthLabel);
	tabletWidthLayout->addWidget(mPressureWidth);
	tabletWidthLayout->addWidget(mTiltWidth);
	QVBoxLayout* tabletOpacityLayout = new QVBoxLayout;
	tabletOpacityLayout->addWidget(opacityLabel);
	tabletOpacityLayout->addWidget(mPressureOpacity);
	tabletOpacityLayout->addWidget(mTiltOpacity);
	QVBoxLayout* tabletColorLayout = new QVBoxLayout;
	tabletColorLayout->addWidget(colorLabel);
	tabletColorLayout->addWidget(mPressureColor);
	tabletColorLayout->addWidget(mTiltColor);
	
	mTabletLayout = new QHBoxLayout;
	mTabletLayout->addLayout(tabletLableLayout);
	mTabletLayout->addLayout(tabletWidthLayout);
	//mTabletLayout->addLayout(tabletOpacityLayout);
	mTabletLayout->addLayout(tabletColorLayout);
	
	mTabletGB = new QGroupBox("Tablet properties");
	mTabletGB->setLayout(mTabletLayout);

	//Pen color layout and Group Box
	mColorLayout = new QHBoxLayout;
	mColorLayout->addWidget(mForegroundColor);
	mColorLayout->addWidget(mBackgroundColor);

	mToolColorGB = new QGroupBox("Pen Color");
	mToolColorGB->setLayout(mColorLayout);

	mWetnesSlider = new QSlider(Qt::Horizontal);
	mWetnesSlider->setMinimum(0);
	mWetnesSlider->setMaximum(100);
	mWetnesSlider->setTickInterval(10);
	mWetnesSlider->setValue(100);

	mWetnesSpinBox = new QSpinBox(this);
	mWetnesSpinBox->setMinimum(1);
	mWetnesSpinBox->setMaximum(100);
	mWetnesSpinBox->setValue(100);

	connect( mWetnesSlider, SIGNAL( valueChanged(int) ), parent, SLOT( setFeltpenWetness(int) ) );
	connect( mWetnesSlider, SIGNAL( valueChanged(int) ), mWetnesSpinBox, SLOT( setValue(int) ) );
	connect( mWetnesSpinBox, SIGNAL( valueChanged(int) ), mWetnesSlider, SLOT( setValue(int) ) );

	mSimulationLayout = new QHBoxLayout;
	mSimulationLayout->addWidget(mWetnesSlider);
	mSimulationLayout->addWidget(mWetnesSpinBox);

	mFeltpenSimulationGB = new QGroupBox("Feltpen wetness");
	mFeltpenSimulationGB->setLayout(mSimulationLayout);

	//FeltPen set color layout and Group Box
	mFeltPenChoseCustomColorLayout = new QHBoxLayout;
	mFeltPenChoseCustomColorLayout->addWidget(mKubelkaColorW);
	mFeltPenChoseCustomColorLayout->addWidget(mKubelkaColorB);

	//Kubelka color layout and Group Box
	mKubelkaPixmap = QPixmap(120, 120);
	QPainter painter(&mKubelkaPixmap);
	painter.setPen(QPen(Qt::transparent, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(QColor(255, 255, 255),Qt::SolidPattern));
	painter.drawRect(0, 0, 60, 120);
	painter.setBrush(QBrush(QColor(0, 0, 0),Qt::SolidPattern));
	painter.drawRect(60, 0, 60, 120);
	painter.end();

	//mKubelkaW = QColor(165, 14, 83);
	//mKubelkaB = QColor(10, 1, 4);
	
	drawFeltPenLine();

	mKubelkaLabel.setPixmap(mKubelkaPixmap);

	mFeltPenColorLayout = new QVBoxLayout;
	mFeltPenColorLayout->addLayout(mFeltPenChoseColorLayout);
	mFeltPenColorLayout->addLayout(mFeltPenChoseCustomColorLayout);
	mFeltPenColorLayout->addWidget(&mKubelkaLabel);

	mKubelkaColorGB = new QGroupBox("Feltpen color");
	mKubelkaColorGB->setLayout(mFeltPenColorLayout);
	mKubelkaColorGB->setVisible(false);

	//spline layout and group box
	mToolAdditionLayout = new QHBoxLayout;
	mToolAdditionGB = new QGroupBox("Spline options");
	mToolAdditionGB->setVisible(false);

	mEscButton = new QPushButton(tr("Accept Spline"));
	connect( mEscButton, SIGNAL( clicked() ), parent, SLOT( escButtonClicked() ) );
	mToolAdditionLayout->addWidget(mEscButton);
	mToolAdditionGB->setLayout(mToolAdditionLayout);
		
	mToolWindowSplitter = new QSplitter(Qt::Vertical, this);
	mToolWindowSplitter->setChildrenCollapsible(false);
	
	mToolsWidget = new QWidget(mToolWindowSplitter);
	mToolsLayout = new QGridLayout(mToolsWidget);
	mToolsLayout->addWidget(mPenButton, 0, 0);
	mToolsLayout->addWidget(mFeltPenButton, 0, 1);
	mToolsLayout->addWidget(mEraserButton, 0, 2);
	mToolsLayout->addWidget(mFillButton, 0, 3);
	mToolsLayout->addWidget(mExpressiveButton, 1, 0);
	mToolsLayout->addWidget(mLineButton, 2, 0);
	mToolsLayout->addWidget(mRectButton, 2, 1);
	mToolsLayout->addWidget(mEllipseButton, 2, 2);
	mToolsLayout->addWidget(mSplineButton, 2, 3);

	mPenButton->setChecked(true);
	mFeltPenColorA->setChecked(true);
	mToolsWidget->setLayout(mToolsLayout);

	mToolOptionsWidget = new QWidget(mToolWindowSplitter);
	mToolSettingsLayout = new QVBoxLayout(mToolOptionsWidget);
	mToolSettingsLayout->addWidget(mToolWidthGB);
	mToolSettingsLayout->addWidget(mToolOpacityGB);
	mToolSettingsLayout->addWidget(mFeltpenSimulationGB);
	mToolSettingsLayout->addWidget(mTabletGB);
	mToolSettingsLayout->addWidget(mToolColorGB);
	mToolSettingsLayout->addWidget(mToolAdditionGB);
	mToolSettingsLayout->addWidget(mKubelkaColorGB);
	mToolSettingsLayout->addStretch();
	mToolOptionsWidget->setLayout(mToolSettingsLayout);

	mToolWindowSplitter->addWidget(mToolsWidget);
	mToolWindowSplitter->addWidget(mToolOptionsWidget);
	mToolWindowSplitter->setHandleWidth(1);
	mToolWindowSplitter->setStyleSheet("QSplitterHandle { background-color: lightgrey;  }");

	mToolWindowLayout = new QVBoxLayout(this);
	mToolWindowLayout->addWidget(mToolWindowSplitter);

	setLayout(mToolWindowLayout);

	resize(170, 200);

	//initialize ToolSetting's logfile
	mLogFile.open("ToolSettingLog.txt");
	mLogFile << "ToolSetting's logfile created! \n";
}

ToolSettings::~ToolSettings()
{

}

void ToolSettings::hideOpacitySlide()
{
	mToolOpacityGB->setVisible(false);
}

void ToolSettings::showOpacitySlide()
{
	mToolOpacityGB->setVisible(true);
}

void ToolSettings::setForegroundButtonColor(QColor color)
{
	QString cssStatement = QString();
	cssStatement.append("QPushButton { background-color: ");
	cssStatement.append( color.name() );
	cssStatement.append("; }");
	mForegroundColor->setStyleSheet( cssStatement );
}

void ToolSettings::setBackgroundButtonColor(QColor color)
{
	QString cssStatement = QString();
	cssStatement.append("QPushButton { background-color: ");
	cssStatement.append( color.name() );
	cssStatement.append("; }");
	mBackgroundColor->setStyleSheet( cssStatement );
}

void ToolSettings::penButtonClicked()
{
	setButtonsUnchecked();
	mPenButton->setChecked(true);

	mTabletGB->setVisible(true);
	mToolColorGB->setVisible(true);
	mKubelkaColorGB->setVisible(false);
	mToolWidthGB->setVisible(true);
	mToolAdditionGB->setVisible(false);

	((MainWindow*)parentWidget())->setTool(0);
}

void ToolSettings::feltPenButtonClicked()
{
	setButtonsUnchecked();
	mFeltPenButton->setChecked(true);

	mTabletGB->setVisible(false);
	mToolColorGB->setVisible(false);
	mKubelkaColorGB->setVisible(true);
	mToolWidthGB->setVisible(false);
	mToolAdditionGB->setVisible(false);

	((MainWindow*)parentWidget())->setTool(7);
}

void ToolSettings::eraserButtonClicked()
{
	setButtonsUnchecked();
	mEraserButton->setChecked(true);

	mTabletGB->setVisible(true);
	mToolColorGB->setVisible(false);
	mKubelkaColorGB->setVisible(false);
	mToolWidthGB->setVisible(true);
	mToolAdditionGB->setVisible(false);

	((MainWindow*)parentWidget())->setTool(1);
}

void ToolSettings::fillButtonClicked()
{
	setButtonsUnchecked();
	mFillButton->setChecked(true);

	mTabletGB->setVisible(true);
	mToolColorGB->setVisible(true);
	mKubelkaColorGB->setVisible(false);
	mToolWidthGB->setVisible(true);
	mToolAdditionGB->setVisible(false);

	((MainWindow*)parentWidget())->setTool(8);
}

void ToolSettings::exprButtonClicked()
{
	setButtonsUnchecked();
	mExpressiveButton->setChecked(true);

	mTabletGB->setVisible(false);
	mToolColorGB->setVisible(true);
	mKubelkaColorGB->setVisible(false);
	mToolWidthGB->setVisible(false);
	mToolAdditionGB->setVisible(false);

	((MainWindow*)parentWidget())->setTool(6);
}

void ToolSettings::lineButtonClicked()
{
	setButtonsUnchecked();
	mLineButton->setChecked(true);

	mTabletGB->setVisible(false);
	mToolColorGB->setVisible(true);
	mKubelkaColorGB->setVisible(false);
	mToolWidthGB->setVisible(true);
	mToolAdditionGB->setVisible(false);

	((MainWindow*)parentWidget())->setTool(2);
}

void ToolSettings::splineButtonClicked()
{
	setButtonsUnchecked();
	mSplineButton->setChecked(true);

	mTabletGB->setVisible(false);
	mToolColorGB->setVisible(true);
	mKubelkaColorGB->setVisible(false);
	mToolWidthGB->setVisible(true);
	mToolAdditionGB->setVisible(true);

	((MainWindow*)parentWidget())->setTool(3);
}

void ToolSettings::rectButtonClicked()
{
	setButtonsUnchecked();
	mRectButton->setChecked(true);

	mTabletGB->setVisible(false);
	mToolColorGB->setVisible(true);
	mKubelkaColorGB->setVisible(false);
	mToolWidthGB->setVisible(true);
	mToolAdditionGB->setVisible(false);

	((MainWindow*)parentWidget())->setTool(4);
}

void ToolSettings::ellipseButtonClicked()
{
	setButtonsUnchecked();
	mEllipseButton->setChecked(true);

	mTabletGB->setVisible(false);
	mToolColorGB->setVisible(true);
	mKubelkaColorGB->setVisible(false);
	mToolWidthGB->setVisible(true);
	mToolAdditionGB->setVisible(false);

	((MainWindow*)parentWidget())->setTool(5);
}

void ToolSettings::setButtonsUnchecked()
{
	mPenButton->setChecked(false);
	mEraserButton->setChecked(false);
	mFillButton->setChecked(false);
	mExpressiveButton->setChecked(false);
	mLineButton->setChecked(false);
	mSplineButton->setChecked(false);
	mRectButton->setChecked(false);
	mEllipseButton->setChecked(false);
	mFeltPenButton->setChecked(false);
}

void ToolSettings::pressureWidthCBoxClicked(int n)
{
	((MainWindow*)parentWidget())->pressureWidthCBoxClicked( mPressureWidth->isChecked() );
	if(mTiltWidth->isChecked() && mPressureWidth->isChecked())
	{
		mTiltWidth->setChecked(false);
	}
}

void ToolSettings::pressureColorCBoxClicked(int n)
{
	((MainWindow*)parentWidget())->pressureColorCBoxClicked( mPressureColor->isChecked() );
	if(mTiltColor->isChecked() && mPressureColor->isChecked())
	{
		mTiltColor->setChecked(false);
	}
}

void ToolSettings::tiltWidthCBoxClicked(int n)
{
	((MainWindow*)parentWidget())->tiltWidthCBoxClicked( mTiltWidth->isChecked() );
	if(mPressureWidth->isChecked() && mTiltWidth->isChecked())
	{
		mPressureWidth->setChecked(false);
	}
}

void ToolSettings::tiltColorCBoxClicked(int n)
{
	((MainWindow*)parentWidget())->tiltColorCBoxClicked( mTiltColor->isChecked() );
	if(mPressureColor->isChecked() && mTiltColor->isChecked())
	{
		mPressureColor->setChecked(false);
	}
}

void ToolSettings::setmKubelkaColorW(QColor color)
{
	QString cssStatement = QString();
	cssStatement.append("QPushButton { background-color: ");
	cssStatement.append( color.name() );
	cssStatement.append("; }");
	mKubelkaColorW->setStyleSheet( cssStatement );

	mKubelkaW = color;
	drawFeltPenLine();
}

void ToolSettings::setmKubelkaColorB(QColor color)
{
	QString cssStatement = QString();
	cssStatement.append("QPushButton { background-color: ");
	cssStatement.append( color.name() );
	cssStatement.append("; }");
	mKubelkaColorB->setStyleSheet( cssStatement );

	mKubelkaB = color;
	drawFeltPenLine();
	
}

void ToolSettings::feltPenColorAClicked()
{
	setPenColorsUnchecked();
	mFeltPenColorA->setChecked(true);

	QColor wColor(165, 14, 83);
	setmKubelkaColorW(wColor);
	QColor bColor(10, 1, 4);
	setmKubelkaColorB(bColor);

	((MainWindow*)parentWidget())->setmFeltpenColorW(wColor);
	((MainWindow*)parentWidget())->setmFeltpenColorB(bColor);
}

void ToolSettings::feltPenColorBClicked()
{
	setPenColorsUnchecked();
	mFeltPenColorB->setChecked(true);
	
	QColor wColor(131, 49, 24);
	setmKubelkaColorW(wColor);
	QColor bColor(103, 32, 15);
	setmKubelkaColorB(bColor);

	((MainWindow*)parentWidget())->setmFeltpenColorW(wColor);
	((MainWindow*)parentWidget())->setmFeltpenColorB(bColor);
}

void ToolSettings::feltPenColorCClicked()
{
	setPenColorsUnchecked();
	mFeltPenColorC->setChecked(true);

	QColor wColor(211, 137, 2);
	setmKubelkaColorW(wColor);
	QColor bColor(116, 75, 1);
	setmKubelkaColorB(bColor);

	((MainWindow*)parentWidget())->setmFeltpenColorW(wColor);
	((MainWindow*)parentWidget())->setmFeltpenColorB(bColor);
}

void ToolSettings::feltPenColorDClicked()	
{	
	setPenColorsUnchecked();
	mFeltPenColorD->setChecked(true);

	QColor wColor(11, 76, 10);
	setmKubelkaColorW(wColor);
	QColor bColor(1, 2, 1);
	setmKubelkaColorB(bColor);

	((MainWindow*)parentWidget())->setmFeltpenColorW(wColor);
	((MainWindow*)parentWidget())->setmFeltpenColorB(bColor);
}

void ToolSettings::feltPenColorEClicked()
{
	setPenColorsUnchecked();
	mFeltPenColorE->setChecked(true);

	QColor wColor(16, 139, 160);
	setmKubelkaColorW(wColor);
	QColor bColor(5, 40, 59);
	setmKubelkaColorB(bColor);

	((MainWindow*)parentWidget())->setmFeltpenColorW(wColor);
	((MainWindow*)parentWidget())->setmFeltpenColorB(bColor);
}

void ToolSettings::feltPenColorFClicked()
{	
	setPenColorsUnchecked();
	mFeltPenColorF->setChecked(true);

	QColor wColor(62, 17, 4);
	setmKubelkaColorW(wColor);
	QColor bColor(11, 7, 1);
	setmKubelkaColorB(bColor);

	((MainWindow*)parentWidget())->setmFeltpenColorW(wColor);
	((MainWindow*)parentWidget())->setmFeltpenColorB(bColor);
}

void ToolSettings::feltPenColorGClicked()
{
	setPenColorsUnchecked();
	mFeltPenColorG->setChecked(true);

	QColor wColor(196, 30, 10);
	setmKubelkaColorW(wColor);
	QColor bColor(99, 2, 1);
	setmKubelkaColorB(bColor);

	((MainWindow*)parentWidget())->setmFeltpenColorW(wColor);
	((MainWindow*)parentWidget())->setmFeltpenColorB(bColor);
}

void ToolSettings::feltPenColorHClicked()	
{ 
	setPenColorsUnchecked();
	mFeltPenColorH->setChecked(true);

	QColor wColor(197, 51, 2);
	setmKubelkaColorW(wColor);
	QColor bColor(1, 1, 1);
	setmKubelkaColorB(bColor);

	((MainWindow*)parentWidget())->setmFeltpenColorW(wColor);
	((MainWindow*)parentWidget())->setmFeltpenColorB(bColor);
}

void ToolSettings::feltPenColorIClicked()
{	
	setPenColorsUnchecked();
	mFeltPenColorI->setChecked(true);

	QColor wColor(227, 175, 8);
	setmKubelkaColorW(wColor);
	QColor bColor(81, 101, 1);
	setmKubelkaColorB(bColor);

	((MainWindow*)parentWidget())->setmFeltpenColorW(wColor);
	((MainWindow*)parentWidget())->setmFeltpenColorB(bColor);
}

void ToolSettings::feltPenColorJClicked()
{
	setPenColorsUnchecked();
	mFeltPenColorJ->setChecked(true);

	QColor wColor(12, 101, 74);
	setmKubelkaColorW(wColor);
	QColor bColor(1, 8, 5);
	setmKubelkaColorB(bColor);

	((MainWindow*)parentWidget())->setmFeltpenColorW(wColor);
	((MainWindow*)parentWidget())->setmFeltpenColorB(bColor);
}

void ToolSettings::feltPenColorKClicked()
{
	setPenColorsUnchecked();
	mFeltPenColorK->setChecked(true);

	QColor wColor(46, 46, 226);
	setmKubelkaColorW(wColor);
	QColor bColor(1, 1, 20);
	setmKubelkaColorB(bColor);

	((MainWindow*)parentWidget())->setmFeltpenColorW(wColor);
	((MainWindow*)parentWidget())->setmFeltpenColorB(bColor);
}

void ToolSettings::feltPenColorLClicked()
{
	setPenColorsUnchecked();
	mFeltPenColorL->setChecked(true);

	QColor wColor(219, 206, 224);
	setmKubelkaColorW(wColor);
	QColor bColor(133, 68, 142);
	setmKubelkaColorB(bColor);

	((MainWindow*)parentWidget())->setmFeltpenColorW(wColor);
	((MainWindow*)parentWidget())->setmFeltpenColorB(bColor);
}

void ToolSettings::setPenColorsUnchecked()
{
	mFeltPenColorA->setChecked(false);
	mFeltPenColorB->setChecked(false);
	mFeltPenColorC->setChecked(false);
	mFeltPenColorD->setChecked(false);
	mFeltPenColorE->setChecked(false);
	mFeltPenColorF->setChecked(false);
	mFeltPenColorG->setChecked(false);
	mFeltPenColorH->setChecked(false);
	mFeltPenColorI->setChecked(false);
	mFeltPenColorJ->setChecked(false);
	mFeltPenColorK->setChecked(false);
	mFeltPenColorL->setChecked(false);
}

void ToolSettings::drawFeltPenLine()
{
	QPainter painter(&mKubelkaPixmap);
	painter.setPen(QPen(Qt::transparent, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	painter.setBrush(QBrush(mKubelkaB, Qt::SolidPattern));
	QPoint points[4] = {QPoint(119, 21), QPoint(112, 10), QPoint(111, 10), QPoint(118, 21)};
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(118, 21);
	points[1] = QPoint(111, 10);
	points[2] = QPoint(111, 10);
	points[3] = QPoint(118, 21);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(118, 21);
	points[1] = QPoint(111, 10);
	points[2] = QPoint(110, 10);
	points[3] = QPoint(117, 21);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(117, 21);
	points[1] = QPoint(110, 10);
	points[2] = QPoint(110, 10);
	points[3] = QPoint(117, 21);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(117, 21);
	points[1] = QPoint(110, 10);
	points[2] = QPoint(109, 10);
	points[3] = QPoint(116, 21);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(116, 21);
	points[1] = QPoint(109, 10);
	points[2] = QPoint(109, 11);
	points[3] = QPoint(116, 22);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(116, 22);
	points[1] = QPoint(109, 11);
	points[2] = QPoint(108, 11);
	points[3] = QPoint(115, 22);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(115, 22);
	points[1] = QPoint(108, 11);
	points[2] = QPoint(108, 11);
	points[3] = QPoint(115, 22);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(115, 22);
	points[1] = QPoint(108, 11);
	points[2] = QPoint(107, 12);
	points[3] = QPoint(114, 23);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(114, 23);
	points[1] = QPoint(107, 12);
	points[2] = QPoint(106, 12);
	points[3] = QPoint(113, 23);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(113, 23);
	points[1] = QPoint(106, 12);
	points[2] = QPoint(105, 13);
	points[3] = QPoint(112, 24);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(112, 24);
	points[1] = QPoint(105, 13);
	points[2] = QPoint(104, 14);
	points[3] = QPoint(111, 25);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(111, 25);
	points[1] = QPoint(104, 14);
	points[2] = QPoint(102, 14);
	points[3] = QPoint(109, 25);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(109, 25);
	points[1] = QPoint(102, 14);
	points[2] = QPoint(101, 15);
	points[3] = QPoint(108, 26);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(108, 26);
	points[1] = QPoint(101, 15);
	points[2] = QPoint(99, 17);
	points[3] = QPoint(106, 28);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(106, 28);
	points[1] = QPoint(99, 17);
	points[2] = QPoint(98, 18);
	points[3] = QPoint(105, 29);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(105, 29);
	points[1] = QPoint(98, 18);
	points[2] = QPoint(96, 19);
	points[3] = QPoint(103, 30);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(103, 30);
	points[1] = QPoint(96, 19);
	points[2] = QPoint(94, 20);
	points[3] = QPoint(101, 31);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(101, 31);
	points[1] = QPoint(94, 20);
	points[2] = QPoint(92, 21);
	points[3] = QPoint(99, 32);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(99, 32);
	points[1] = QPoint(92, 21);
	points[2] = QPoint(91, 23);
	points[3] = QPoint(98, 34);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(98, 34);
	points[1] = QPoint(91, 23);
	points[2] = QPoint(89, 24);
	points[3] = QPoint(96, 35);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(96, 35);
	points[1] = QPoint(89, 24);
	points[2] = QPoint(87, 26);
	points[3] = QPoint(94, 37);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(94, 37);
	points[1] = QPoint(87, 26);
	points[2] = QPoint(85, 28);
	points[3] = QPoint(92, 39);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(92, 39);
	points[1] = QPoint(85, 28);
	points[2] = QPoint(83, 29);
	points[3] = QPoint(90, 40);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(90, 40);
	points[1] = QPoint(83, 29);
	points[2] = QPoint(81, 31);
	points[3] = QPoint(88, 42);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(88, 42);
	points[1] = QPoint(81, 31);
	points[2] = QPoint(79, 33);
	points[3] = QPoint(86, 44);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(86, 44);
	points[1] = QPoint(79, 33);
	points[2] = QPoint(77, 34);
	points[3] = QPoint(84, 45);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(84, 45);
	points[1] = QPoint(77, 34);
	points[2] = QPoint(76, 36);
	points[3] = QPoint(83, 47);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(83, 47);
	points[1] = QPoint(76, 36);
	points[2] = QPoint(74, 37);
	points[3] = QPoint(81, 48);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(81, 48);
	points[1] = QPoint(74, 37);
	points[2] = QPoint(72, 38);
	points[3] = QPoint(79, 49);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(79, 49);
	points[1] = QPoint(72, 38);
	points[2] = QPoint(71, 40);
	points[3] = QPoint(78, 51);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(78, 51);
	points[1] = QPoint(71, 40);
	points[2] = QPoint(70, 41);
	points[3] = QPoint(77, 52);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(77, 52);
	points[1] = QPoint(70, 41);
	points[2] = QPoint(69, 42);
	points[3] = QPoint(76, 53);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(76, 53);
	points[1] = QPoint(69, 42);
	points[2] = QPoint(67, 43);
	points[3] = QPoint(74, 54);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(74, 54);
	points[1] = QPoint(67, 43);
	points[2] = QPoint(66, 44);
	points[3] = QPoint(73, 55);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(73, 55);
	points[1] = QPoint(66, 44);
	points[2] = QPoint(66, 46);
	points[3] = QPoint(73, 57);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(73, 57);
	points[1] = QPoint(66, 46);
	points[2] = QPoint(65, 47);
	points[3] = QPoint(72, 58);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(72, 58);
	points[1] = QPoint(65, 47);
	points[2] = QPoint(64, 48);
	points[3] = QPoint(71, 59);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(71, 59);
	points[1] = QPoint(64, 48);
	points[2] = QPoint(63, 49);
	points[3] = QPoint(70, 60);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(70, 60);
	points[1] = QPoint(63, 49);
	points[2] = QPoint(63, 49);
	points[3] = QPoint(70, 60);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(70, 60);
	points[1] = QPoint(63, 49);
	points[2] = QPoint(63, 50);
	points[3] = QPoint(70, 61);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(70, 61);
	points[1] = QPoint(63, 50);
	points[2] = QPoint(63, 51);
	points[3] = QPoint(70, 62);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(70, 62);
	points[1] = QPoint(63, 51);
	points[2] = QPoint(63, 51);
	points[3] = QPoint(70, 62);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(70, 62);
	points[1] = QPoint(63, 51);
	points[2] = QPoint(63, 52);
	points[3] = QPoint(70, 63);
	painter.drawConvexPolygon(points, 4); 
 
	points[0] = QPoint(70, 63);
	points[1] = QPoint(63, 52);
	points[2] = QPoint(63, 52);
	points[3] = QPoint(70, 63);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(70, 63);
	points[1] = QPoint(63, 52);
	points[2] = QPoint(63, 52);
	points[3] = QPoint(70, 63);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(70, 63);
	points[1] = QPoint(63, 52);
	points[2] = QPoint(63, 53);
	points[3] = QPoint(70, 64);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(70, 64);
	points[1] = QPoint(63, 53);
	points[2] = QPoint(63, 53);
	points[3] = QPoint(70, 64);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(70, 64);
	points[1] = QPoint(63, 53);
	points[2] = QPoint(64, 54);
	points[3] = QPoint(71, 65);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(71, 65);
	points[1] = QPoint(64, 54);
	points[2] = QPoint(64, 54);
	points[3] = QPoint(71, 65);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(71, 65);
	points[1] = QPoint(64, 54);
	points[2] = QPoint(65, 55);
	points[3] = QPoint(72, 66);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(72, 66);
	points[1] = QPoint(65, 55);
	points[2] = QPoint(66, 55);
	points[3] = QPoint(73, 66);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(73, 66);
	points[1] = QPoint(66, 55);
	points[2] = QPoint(67, 55);
	points[3] = QPoint(74, 66);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(74, 66);
	points[1] = QPoint(67, 55);
	points[2] = QPoint(69, 56);
	points[3] = QPoint(76, 67);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(76, 67);
	points[1] = QPoint(69, 56);
	points[2] = QPoint(70, 56);
	points[3] = QPoint(77, 67);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(77, 67);
	points[1] = QPoint(70, 56);
	points[2] = QPoint(71, 57);
	points[3] = QPoint(78, 68);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(78, 68);
	points[1] = QPoint(71, 57);
	points[2] = QPoint(72, 57);
	points[3] = QPoint(79, 68);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(79, 68);
	points[1] = QPoint(72, 57);
	points[2] = QPoint(74, 57);
	points[3] = QPoint(81, 68);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(81, 68);
	points[1] = QPoint(74, 57);
	points[2] = QPoint(74, 58);
	points[3] = QPoint(81, 69);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(81, 69);
	points[1] = QPoint(74, 58);
	points[2] = QPoint(75, 58);
	points[3] = QPoint(82, 69);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(82, 69);
	points[1] = QPoint(75, 58);
	points[2] = QPoint(76, 59);
	points[3] = QPoint(83, 70);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(83, 70);
	points[1] = QPoint(76, 59);
	points[2] = QPoint(76, 60);
	points[3] = QPoint(83, 71);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(83, 71);
	points[1] = QPoint(76, 60);
	points[2] = QPoint(76, 61);
	points[3] = QPoint(83, 72);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(83, 72);
	points[1] = QPoint(76, 61);
	points[2] = QPoint(76, 62);
	points[3] = QPoint(83, 73);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(83, 73);
	points[1] = QPoint(76, 62);
	points[2] = QPoint(76, 63);
	points[3] = QPoint(83, 74);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(83, 74);
	points[1] = QPoint(76, 63);
	points[2] = QPoint(76, 64);
	points[3] = QPoint(83, 75);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(83, 75);
	points[1] = QPoint(76, 64);
	points[2] = QPoint(76, 65);
	points[3] = QPoint(83, 76);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(83, 76);
	points[1] = QPoint(76, 65);
	points[2] = QPoint(76, 66);
	points[3] = QPoint(83, 77);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(83, 77);
	points[1] = QPoint(76, 66);
	points[2] = QPoint(76, 67);
	points[3] = QPoint(83, 78);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(83, 78);
	points[1] = QPoint(76, 67);
	points[2] = QPoint(76, 68);
	points[3] = QPoint(83, 79);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(83, 79);
	points[1] = QPoint(76, 68);
	points[2] = QPoint(75, 69);
	points[3] = QPoint(82, 80);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(82, 80);
	points[1] = QPoint(75, 69);
	points[2] = QPoint(74, 72);
	points[3] = QPoint(81, 81);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(81, 81);
	points[1] = QPoint(74, 72);
	points[2] = QPoint(73, 73);
	points[3] = QPoint(80, 82);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(80, 82);
	points[1] = QPoint(73, 73);
	points[2] = QPoint(72, 74);
	points[3] = QPoint(79, 83);
	painter.drawConvexPolygon(points, 4); 
 
	points[0] = QPoint(79, 83);
	points[1] = QPoint(72, 74);
	points[2] = QPoint(71, 76);
	points[3] = QPoint(78, 85);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(78, 85);
	points[1] = QPoint(71, 76);
	points[2] = QPoint(69, 77);
	points[3] = QPoint(76, 86);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(76, 86);
	points[1] = QPoint(69, 77);
	points[2] = QPoint(68, 79);
	points[3] = QPoint(75, 88);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(75, 88);
	points[1] = QPoint(68, 79);
	points[2] = QPoint(66, 80);
	points[3] = QPoint(73, 89);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(73, 89);
	points[1] = QPoint(66, 80);
	points[2] = QPoint(64, 82);
	points[3] = QPoint(71, 91);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(71, 91);
	points[1] = QPoint(64, 82);
	points[2] = QPoint(63, 83);
	points[3] = QPoint(70, 92);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(70, 92);
	points[1] = QPoint(63, 83);
	points[2] = QPoint(61, 84);
	points[3] = QPoint(68, 93);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(68, 93);
	points[1] = QPoint(61, 84);
	points[2] = QPoint(60, 86);
	points[3] = QPoint(60, 100);
	painter.drawConvexPolygon(points, 4); 

	painter.setBrush(QBrush(mKubelkaW, Qt::SolidPattern));

	points[0] = QPoint(60, 100);
	points[1] = QPoint(60, 86);
	points[2] = QPoint(51, 93);
	points[3] = QPoint(58, 102);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(58, 102);
	points[1] = QPoint(51, 93);
	points[2] = QPoint(49, 94);
	points[3] = QPoint(56, 103);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(56, 103);
	points[1] = QPoint(49, 94);
	points[2] = QPoint(48, 95);
	points[3] = QPoint(55, 104);
	painter.drawConvexPolygon(points, 4); 
 
	points[0] = QPoint(55, 104);
	points[1] = QPoint(48, 95);
	points[2] = QPoint(46, 97);
	points[3] = QPoint(53, 106);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(53, 106);
	points[1] = QPoint(46, 97);
	points[2] = QPoint(44, 98);
	points[3] = QPoint(51, 107);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(51, 107);
	points[1] = QPoint(44, 98);
	points[2] = QPoint(43, 100);
	points[3] = QPoint(50, 109);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(50, 109);
	points[1] = QPoint(43, 100);
	points[2] = QPoint(41, 102);
	points[3] = QPoint(48, 111);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(48, 111);
	points[1] = QPoint(41, 102);
	points[2] = QPoint(39, 103);
	points[3] = QPoint(46, 112);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(46, 112);
	points[1] = QPoint(39, 103);
	points[2] = QPoint(36, 105);
	points[3] = QPoint(43, 114);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(43, 114);
	points[1] = QPoint(36, 105);
	points[2] = QPoint(34, 107);
	points[3] = QPoint(41, 116);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(41, 116);
	points[1] = QPoint(34, 107);
	points[2] = QPoint(32, 108);
	points[3] = QPoint(39, 117);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(39, 117);
	points[1] = QPoint(32, 108);
	points[2] = QPoint(31, 110);
	points[3] = QPoint(38, 119);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(38, 119);
	points[1] = QPoint(31, 110);
	points[2] = QPoint(29, 111);
	points[3] = QPoint(36, 120);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(36, 120);
	points[1] = QPoint(29, 111);
	points[2] = QPoint(27, 112);
	points[3] = QPoint( 4, 121);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(34, 121);
	points[1] = QPoint(27, 112);
	points[2] = QPoint(26, 113);
	points[3] = QPoint(33, 122);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(33, 122);
	points[1] = QPoint(26, 113);
	points[2] = QPoint(24, 114);
	points[3] = QPoint(31, 123);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(31, 123);
	points[1] = QPoint(24, 114);
	points[2] = QPoint(23, 115);
	points[3] = QPoint(30, 124);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(30, 124);
	points[1] = QPoint(23, 115);
	points[2] = QPoint(22, 116);
	points[3] = QPoint(29, 125);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(29, 125);
	points[1] = QPoint(22, 116);
	points[2] = QPoint(21, 117);
	points[3] = QPoint(28, 126);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(28, 126);
	points[1] = QPoint(21, 117);
	points[2] = QPoint(20, 118);
	points[3] = QPoint(27, 127);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(27, 127);
	points[1] = QPoint(20, 118);
	points[2] = QPoint(20, 119);
	points[3] = QPoint(27, 128);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(27, 128);
	points[1] = QPoint(20, 119);
	points[2] = QPoint(19, 120);
	points[3] = QPoint(26, 129);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(26, 129);
	points[1] = QPoint(19, 120);
	points[2] = QPoint(19, 120);
	points[3] = QPoint(26, 129);
	painter.drawConvexPolygon(points, 4); 

	points[0] = QPoint(26, 129);
	points[1] = QPoint(19, 120);
	points[2] = QPoint(19, 121);
	points[3] = QPoint(26, 130);
	painter.drawConvexPolygon(points, 4); 

	painter.end();

	mKubelkaLabel.setPixmap(mKubelkaPixmap);
}

void ToolSettings::quit()
{
	this->hide();
}
