#include "mainwindow.h"

MainWindow::MainWindow(PaintArea* paintArea)
{
	mLogFile.open("MainWindowLog.txt");
	mLogFile << "MainWindow's logfile created! \n";

	setWindowTitle("ScratchUp");

	newAction = new QAction(tr("&New"), this);
	openAction = new QAction(tr("&Open"), this);
	saveAction = new QAction(tr("&Save"), this);
	saveAsAction = new QAction(tr("&Save as..."), this);
	printAct = new QAction(tr("&Print..."), this);
	exitAction = new QAction(tr("E&xit"), this);

	toolSettingsAction = new QAction(tr("&Tool Settings"), this);
	layersWindowAction = new QAction(tr("&Layers Window"), this);

	smoothingFilterAction = new QAction(tr("&Mean Filter"), this);

	stackMergeAction = new QAction(tr("&Merge Images (Alpha Merge)"), this);

	connect( newAction, SIGNAL(triggered()), this, SLOT(newDocument()) );
	connect( openAction, SIGNAL(triggered()), this, SLOT(open()) );
    connect( saveAction, SIGNAL(triggered()), this, SLOT(save()) );
	connect( saveAsAction, SIGNAL(triggered()), this, SLOT(saveAs()) );
	connect( printAct, SIGNAL(triggered()), this, SLOT(print()));
    connect( exitAction, SIGNAL(triggered()), qApp, SLOT(quit()) );
	
	connect( toolSettingsAction, SIGNAL(triggered()), this, SLOT(showToolSettings()) );
	connect( layersWindowAction, SIGNAL(triggered()), this, SLOT(showLayersWindow()) );

	connect( smoothingFilterAction, SIGNAL(triggered()), this, SLOT(smoothingFilter()) );
	connect( stackMergeAction, SIGNAL(triggered()), this, SLOT(stackMerge()) );

    fileMenu = menuBar()->addMenu(tr("&File"));
	fileMenu->addAction(newAction);
    fileMenu->addAction(openAction);
    fileMenu->addAction(saveAction);
	fileMenu->addAction(saveAsAction);
	fileMenu->addAction(printAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAction);

	viewMenu = menuBar()->addMenu(tr("&View"));
	viewMenu->addAction(toolSettingsAction);
	viewMenu->addAction(layersWindowAction);

	filterMenu = menuBar()->addMenu(tr("&Filter"));
	filterMenu->addAction(smoothingFilterAction);

	stackMenu = menuBar()->addMenu(tr("&Stack"));
	stackMenu->addAction(stackMergeAction);

	mPaintArea = paintArea;
	mScrollArea = new QScrollArea;
	mPaintArea->show();
	mPaintArea->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	mScrollArea->setWidget(mPaintArea);

	//din a4 mm: x=210, y=148
	//din a4 inch: y=8.2, x=11.6
	//dpi = 150
	int dpi = 30;
	mScrollArea->resize(8.2 * dpi, 11.6 * dpi);
	int x = 80 * dpi;
	int y = 110 * dpi;
	x /= 6;
	y /= 6;
	mPaintArea->resizeImagesAndLayers(QSize(x, y));
	mPaintArea->resize(QSize(x, y));
			
	//mPaintArea->resizeImagesAndLayers(QSize(1000, 1000));
	//mPaintArea->resize(QSize(1000, 1000));
	resize(800, 500);

	mScrollArea->setBackgroundRole(QPalette::Dark);
	setCentralWidget(mScrollArea);
	
	mNewDocumentButton = new QToolButton();
	mNewDocumentButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mNewDocumentButton->setIcon(QIcon("images/toolBarIcons/newDocument.png"));
	mNewDocumentButton->setToolTip("new");
	connect( mNewDocumentButton, SIGNAL(clicked()), this, SLOT(newDocument()) );

	mSaveButton = new QToolButton();
	mSaveButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mSaveButton->setIcon(QIcon("images/toolBarIcons/save.png"));
	mSaveButton->setToolTip("save");
	connect( mSaveButton, SIGNAL(clicked()), this, SLOT(save()) );

	mZoomInButton = new QToolButton();
	mZoomInButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mZoomInButton->setIcon(QIcon("images/toolBarIcons/zoomIn.png"));
	mZoomInButton->setToolTip("zoom in");
	connect( mZoomInButton, SIGNAL(clicked()), this, SLOT(zoomIn()) );

	mZoomOutButton = new QToolButton();
	mZoomOutButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mZoomOutButton->setIcon(QIcon("images/toolBarIcons/zoomOut.png"));
	mZoomOutButton->setToolTip("zoom out");
	connect( mZoomOutButton, SIGNAL(clicked()), this, SLOT(zoomOut()) );

	//main windows zoom count variable, positive values are counting zoomIns, negative are counting zoomOuts
	mZoomCount = 0;
	
	mResetSizeButton = new QToolButton();
	mResetSizeButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mResetSizeButton->setIcon(QIcon("images/toolBarIcons/resetSize.png"));
	mResetSizeButton->setToolTip("zoom back to original size");
	connect( mResetSizeButton, SIGNAL(clicked()), this, SLOT(resetSize()) );

	mUndoButton = new QToolButton();
	mUndoButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mUndoButton->setIcon(QIcon("images/toolBarIcons/undo.png"));
	mUndoButton->setToolTip("undo");
	connect( mUndoButton, SIGNAL(clicked()), this, SLOT(undo()) );

	mRedoButton = new QToolButton();
	mRedoButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mRedoButton->setIcon(QIcon("images/toolBarIcons/redo.png"));
	mRedoButton->setToolTip("redo");
	connect( mRedoButton, SIGNAL(clicked()), this, SLOT(redo()) );

	mUndoLabel = new QLabel("Possible \nundo \nsteps: ");

	mUndoSpinBox = new QSpinBox(this);
	mUndoSpinBox->setMinimum(3);
	mUndoSpinBox->setMaximum(100);
	mUndoSpinBox->setValue(20);
	connect( mUndoSpinBox, SIGNAL(valueChanged(int)), this, SLOT(setUndoNumber(int)) );

	mIncrWidthButton = new QToolButton();
	mIncrWidthButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mIncrWidthButton->setIcon(QIcon("images/toolBarIcons/incrWidth.png"));
	mIncrWidthButton->setToolTip("increase document's width");
	connect( mIncrWidthButton, SIGNAL(clicked()), this, SLOT(incrWidth()) );
	
	mDecrWidthButton = new QToolButton();
	mDecrWidthButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mDecrWidthButton->setIcon(QIcon("images/toolBarIcons/decrWidth.png"));
	mDecrWidthButton->setToolTip("decrease document's width");
	connect( mDecrWidthButton, SIGNAL(clicked()), this, SLOT(decrWidth()) );
	
	mIncrHeightButton = new QToolButton();
	mIncrHeightButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mIncrHeightButton->setIcon(QIcon("images/toolBarIcons/incrHeight.png"));
	mIncrHeightButton->setToolTip("increase document's height");
	connect( mIncrHeightButton, SIGNAL(clicked()), this, SLOT(incrHeight()) );
		
	mDecrHeightButton = new QToolButton();
	mDecrHeightButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mDecrHeightButton->setIcon(QIcon("images/toolBarIcons/decrHeight.png"));
	mDecrHeightButton->setToolTip("decrease document's height");
	connect( mDecrHeightButton, SIGNAL(clicked()), this, SLOT(decrHeight()) );

	mSizeLabel = new QLabel("Increase / \ndecrease \nstep-size");

	mSizeSpinBox = new QSpinBox(this);
	mSizeSpinBox->setMinimum(1);
	mSizeSpinBox->setMaximum(50);
	mSizeSpinBox->setValue(5);

	mToolBar = new QToolBar();
	mToolBar->setIconSize(QSize(40, 40));
	mToolBar->addWidget(mNewDocumentButton);
	mToolBar->addWidget(mSaveButton);
	mToolBar->addSeparator();
	mToolBar->addWidget(mZoomInButton);
	mToolBar->addWidget(mZoomOutButton);
	mToolBar->addWidget(mResetSizeButton);
	mToolBar->addSeparator();
	mToolBar->addWidget(mUndoButton);
	mToolBar->addWidget(mRedoButton);
	mToolBar->addWidget(mUndoLabel);
	mToolBar->addWidget(mUndoSpinBox);
	mToolBar->addSeparator();
	mToolBar->addWidget(mIncrWidthButton);
	mToolBar->addWidget(mDecrWidthButton);
	mToolBar->addWidget(mIncrHeightButton);
	mToolBar->addWidget(mDecrHeightButton);
	mToolBar->addWidget(mSizeLabel);
	mToolBar->addWidget(mSizeSpinBox);

	addToolBar(mToolBar);
	mToolBar->show();

	mFileName = "";
	mFileFormat = "";

	statusBar()->showMessage(QString("Ready"));
	qApp->installEventFilter(this);
	setFocusPolicy(Qt::StrongFocus);

	mToolSettings = new ToolSettings(this);
	mToolSettings->move(100, 100);

	mLayersWindow = new LayersWindow(this);
	mLayersWindow->move(110, 100);

	mLayersWindow->newLayer(true);

	mToolSettings->show();
	mLayersWindow->show();
}

MainWindow::~MainWindow()
{
	delete mPaintArea;
	delete mScrollArea;
}

void MainWindow::keyPressEvent( QKeyEvent* evt )
{
	mLogFile << "keyPresEvent entered! \n" << endl;

	QKeySequence s1 = QKeySequence(evt->modifiers() + evt->key());

    if(s1.matches(QKeySequence("Ctrl+Z")))
	{
		mLogFile << "Strg+Z pressed! \n" << endl;
		mPaintArea->undo();
	}

	if(s1.matches(QKeySequence("Ctrl+S")))
	{
		mLogFile << "Strg+S pressed! \n" << endl;
		save();
	}

	if( evt->key() == Qt::Key_Escape )
	{
		mPaintArea->escape();
	}
}

bool MainWindow::eventFilter(QObject* obj, QEvent* evt)
{
	if (evt->type() == QEvent::MouseMove)
	{
		QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(evt);
		statusBar()->showMessage(QString("Mouse move (%1,%2)").arg(mouseEvent->pos().x()).arg(mouseEvent->pos().y()));
	}
	
	return false;
}

void MainWindow::setStatusMessage(QString message)
{
	statusBar()->showMessage(message);
}

void MainWindow::newDocument()
{
	mLayersWindow->newProject();
	mPaintArea->newProject();
}

void MainWindow::open()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open Image File"), QDir::currentPath());
	if (!fileName.isNull())
	{
		mPaintArea->openImage(fileName);
		mLayersWindow->newLayer(false);
	}
}

bool MainWindow::save()
{
	if(mFileName == "")
	{
		saveAs();
	}
	else
	{
		return mPaintArea->saveImage(mFileName, mFileFormat.toLatin1());
	}
	//this line is never executed
	return false;
}

bool MainWindow::saveAs()
{
    /*
    QString initialPath = QDir::currentPath() + "/untitled";
	QFileDialog fileDialog(this, tr("Save As"), initialPath);
	fileDialog.setAcceptMode(QFileDialog::AcceptSave);
	fileDialog.setFilter("*.png;;*.bmp;;*.jpg;;*.jpeg;;*.ppm;;*.ico;;*.tiff");

	if(fileDialog.exec() == QDialog::Accepted)
	{
		mFileName = fileDialog.selectedFiles()[0];
		mFileFormat = fileDialog.selectedNameFilter();
	}

	if(mFileName.isEmpty()) 
	{
		mLogFile << "save called, fileName was empty! \n" << endl;
		return false;
    } 
	else 
	{
		mFileFormat.remove("*.");
		return mPaintArea->saveImage(mFileName, mFileFormat.toLatin1());
    }
    */
    return false;
}

void MainWindow::print()
{
	mPaintArea->print();
}

void MainWindow::setTool(int toolNumber)
{
	mPaintArea->setTool(toolNumber);
	if(toolNumber == 3)
	{
		setStatusMessage(QString("Tool info: Draw a stroke and edit, press Escape to save the curve!"));
	}
	mLogFile << "Tool changed to " << toolNumber;
}

void MainWindow::escButtonClicked()
{
	mPaintArea->escape();
}

void MainWindow::pressureWidthCBoxClicked(bool checked)
{
	mPaintArea->setPressureWidth(checked);
	mLogFile << "pressureWidthCBoxClicked() called \n" << endl;
}

void MainWindow::pressureColorCBoxClicked(bool checked)
{
	mPaintArea->setPressureColor(checked);
	mLogFile << "pressureColorCBoxClicked() called \n" << endl;
}

void MainWindow::tiltWidthCBoxClicked(bool checked)
{
	mPaintArea->setTiltWidth(checked);
	mLogFile << "tiltWidthCBoxClicked() called \n" << endl;
}

void MainWindow::tiltColorCBoxClicked(bool checked)
{
	mPaintArea->setTiltColor(checked);
	mLogFile << "tiltColorCBoxClicked() called \n" << endl;
}

void MainWindow::setToolWidth( int penWidth )
{
	mPaintArea->setToolWidth(penWidth);
}

void MainWindow::setToolOpacity( int penOpacity )
{
	mPaintArea->setToolOpacity(penOpacity);
}

// function to set paint areas foreground paint color
void MainWindow::setToolColorFG()
{
	// open qColor dialog with the actual fg color as default value
	QColor newColor = QColorDialog::getColor(mPaintArea->getToolColorFG());
    if(newColor.isValid())
	{
		mPaintArea->setToolColorFG(newColor);
		mToolSettings->setForegroundButtonColor(newColor);
	}
}

// function to set paint areas background paint color
void MainWindow::setToolColorBG()
{
	// open qColor dialog with the actual bg color as default value
	QColor newColor = QColorDialog::getColor(mPaintArea->getToolColorBG());
    if(newColor.isValid())
	{
		mPaintArea->setToolColorBG(newColor);
		mToolSettings->setBackgroundButtonColor(newColor);
	}
}

void MainWindow::setmFeltpenColorDialogW()
{
	// open qColor dialog with the actual bg color as default value
	QColor newColor = QColorDialog::getColor(mPaintArea->getKubelkaColorW());
    if(newColor.isValid())
	{
		mPaintArea->setKubelkaColorW(newColor);
		mToolSettings->setmKubelkaColorW(newColor);
	}
}

void MainWindow::setmFeltpenColorDialogB()
{
	// open qColor dialog with the actual bg color as default value
	QColor newColor = QColorDialog::getColor(mPaintArea->getKubelkaColorB());
    if(newColor.isValid())
	{
		mPaintArea->setKubelkaColorB(newColor);
		mToolSettings->setmKubelkaColorB(newColor);
	}
}

void MainWindow::setmFeltpenColorW(QColor newColor)
{
	mPaintArea->setKubelkaColorW(newColor);
	mToolSettings->setmKubelkaColorW(newColor);
}

void MainWindow::setmFeltpenColorB(QColor newColor)
{
	mPaintArea->setKubelkaColorB(newColor);
	mToolSettings->setmKubelkaColorB(newColor);
}

void MainWindow::setActiveLayer( int activeLayer )
{
	if(mPaintArea->setActiveLayer(activeLayer))
	{
		//active layer is simulation layer
		mToolSettings->hideOpacitySlide();
	}
	else
	{
		//active layer is normal layer
		mToolSettings->showOpacitySlide();
	}
	mLayersWindow->setOpacityView(mPaintArea->getLayerOpacity());
	mLayersWindow->setMixingModeView(mPaintArea->getLayersMixingMode());
}

void MainWindow::changeLayerVisibility( int layer, bool visible )
{
	mLogFile << "changeLayerVisability() entered! \n";
	mPaintArea->setLayerVisability(layer, visible);
}

void MainWindow::addLayer()
{
	mLogFile << "MainWindows addLayer called! \n";
	mPaintArea->addLayer();
	mToolSettings->showOpacitySlide();
}

void MainWindow::addSimulationLayer()
{
	mLogFile << "MainWindows addSimulationLayer called! \n";
	mPaintArea->addSimulationLayer();
	mToolSettings->feltPenButtonClicked();
	mToolSettings->hideOpacitySlide();
}

void MainWindow::restoreLayer()
{
	mLogFile << "MainWindows restoreLayer called! \n";
	mPaintArea->restoreLayer();
}

void MainWindow::deleteActualLayer()
{
	mLogFile << "MainWindow's deleteActualLayer() function called! \n";
	mPaintArea->deleteActiveLayer();
}

void MainWindow::mixingModeChanged(int index)
{
	mLogFile << "mixingModeChanged(int): called!";
	switch(index)
	{
		case 0:
			mPaintArea->setLayersMixingMode(QPainter::CompositionMode_SourceOver);
		break;
		
		case 1:
			mPaintArea->setLayersMixingMode(QPainter::CompositionMode_Plus);
		break;
		
		case 2:
			mPaintArea->setLayersMixingMode(QPainter::CompositionMode_Difference);
		break;
		
		case 3:
			mPaintArea->setLayersMixingMode(QPainter::CompositionMode_Multiply);
		break;
		
		case 4:
			mPaintArea->setLayersMixingMode(QPainter::CompositionMode_Screen);
		break;
		
		case 5:
			mPaintArea->setLayersMixingMode(QPainter::CompositionMode_Darken);
		break;
		
		case 6:
			mPaintArea->setLayersMixingMode(QPainter::CompositionMode_Lighten);
		break;
				
		case 7:
			mPaintArea->setLayersMixingMode(QPainter::CompositionMode_ColorDodge);
		break;
				
		case 8:
			mPaintArea->setLayersMixingMode(QPainter::CompositionMode_ColorBurn);
		break;
				
		case 9:
			mPaintArea->setLayersMixingMode(QPainter::CompositionMode_HardLight);
		break;
				
		case 10:
			mPaintArea->setLayersMixingMode(QPainter::CompositionMode_SoftLight);
		break;
	}	
}

void MainWindow::setLayerOpacity( int opacity )
{
	mPaintArea->setLayerOpacity(opacity);
}

void MainWindow::raiseActualLayer()
{
	mPaintArea->raiseActualLayer();
}

void MainWindow::lowerActualLayer()
{
	mPaintArea->lowerActualLayer();
}

void MainWindow::showToolSettings()
{
	mToolSettings->show();
}

void MainWindow::showLayersWindow()
{
	mLayersWindow->show();
}

void MainWindow::smoothingFilter()
{
	mPaintArea->smoothingFilter(5);
}

void MainWindow::stackMerge()
{
	mPaintArea->stackMerge();
}

void MainWindow::undo()
{	
	mPaintArea->undo();
	mLogFile << "undo \n " << endl;
}

void MainWindow::redo()
{	
	mPaintArea->redo();
	mLogFile << "redo \n " << endl;
}

void MainWindow::zoomIn()
{
	if(mZoomCount < 5)
	{
		//mPaintArea->resize(mPaintArea->width() * 1.25, mPaintArea->height() * 1.25);
		mPaintArea->zoomIn();
		mZoomCount++;
	}
}

void MainWindow::zoomOut()
{	
	if(mZoomCount > -5)
	{
		mPaintArea->resize(mPaintArea->width() * 0.8, mPaintArea->height() * 0.8);
		mPaintArea->zoomOut();
		mZoomCount--;
	}
}

void MainWindow::resetSize()
{
	mPaintArea->resetSize();
	mZoomCount = 0;
}

void MainWindow::incrHeight()
{
	resetSize();
	QSize newSize(mPaintArea->size().width(), 
				  mPaintArea->size().height()+mSizeSpinBox->value());
	mPaintArea->resizeImagesAndLayers(newSize);
	mPaintArea->resize(newSize);
	mPaintArea->setNewOriginalSize(newSize);
}

void MainWindow::incrWidth()
{
	resetSize();
	QSize newSize(mPaintArea->size().width()+mSizeSpinBox->value(), 
				  mPaintArea->size().height());
	mPaintArea->resizeImagesAndLayers(newSize);
	mPaintArea->resize(newSize);
	mPaintArea->setNewOriginalSize(newSize);
}

void MainWindow::decrHeight()
{
	resetSize();
	QSize newSize(mPaintArea->size().width(), 
				  mPaintArea->size().height()-mSizeSpinBox->value());
	mPaintArea->resizeImagesAndLayers(newSize);
	mPaintArea->resize(newSize);
	mPaintArea->setNewOriginalSize(newSize);
}

void MainWindow::decrWidth()
{
	resetSize();
	QSize newSize(mPaintArea->size().width()-mSizeSpinBox->value(), 
				  mPaintArea->size().height());
	mPaintArea->resizeImagesAndLayers(newSize);
	mPaintArea->resize(newSize);
	mPaintArea->setNewOriginalSize(newSize);
}

void MainWindow::setUndoNumber(int undoNumber)
{
	mPaintArea->setUndoNumber(undoNumber);
}

void MainWindow::deactivateSizeButtons()
{
	mIncrHeightButton->setDisabled(true);
	mDecrHeightButton->setDisabled(true);
	mIncrWidthButton->setDisabled(true);
	mDecrWidthButton->setDisabled(true);
	mUndoButton->setDisabled(true);
	mRedoButton->setDisabled(true);
	mZoomInButton->setDisabled(true);
	mZoomOutButton->setDisabled(true);
	mResetSizeButton->setDisabled(true);
}

void MainWindow::activateSizeButtons()
{
	mIncrHeightButton->setDisabled(false);
	mDecrHeightButton->setDisabled(false);
	mIncrWidthButton->setDisabled(false);
	mDecrWidthButton->setDisabled(false);
	mUndoButton->setDisabled(false);
	mRedoButton->setDisabled(false);
	mZoomInButton->setDisabled(false);
	mZoomOutButton->setDisabled(false);
	mResetSizeButton->setDisabled(false);
}

void MainWindow::setFeltpenWetness(int wetness)
{
	mPaintArea->setFeltpenWetness(wetness);
}

void MainWindow::quit()
{

}
