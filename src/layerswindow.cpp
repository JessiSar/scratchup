#include "layerswindow.h"
#include "mainwindow.h"
#include <qlistwidget.h>


LayersWindow::LayersWindow(QWidget *parent) : QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	this->setWindowFlags(Qt::Tool);
	this->setWindowTitle(tr("Layers"));

	mNewLayerButton = new QToolButton(this);
	mNewLayerButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mNewLayerButton->setIcon(QIcon("images/layerIcons/layer_new_transparency.png"));
	mNewLayerButton->setIconSize(QSize(22, 22));
	mNewLayerButton->setToolTip("Add new transparency layer");
	connect( mNewLayerButton, SIGNAL(clicked()), this, SLOT(newLayerSlot()) );

	mNewPaperLayerButton = new QToolButton(this);
	mNewPaperLayerButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mNewPaperLayerButton->setIcon(QIcon("images/layerIcons/layer_new_paper.png"));
	mNewPaperLayerButton->setIconSize(QSize(22, 22));
	mNewPaperLayerButton->setToolTip("Add new paper simulation layer");
	connect( mNewPaperLayerButton, SIGNAL(clicked()), this, SLOT(newSimulationLayerSlot()) );

	mRestoreLayerButton = new QToolButton(this);
	mRestoreLayerButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mRestoreLayerButton->setIcon(QIcon("images/layerIcons/layer_new_restore.png"));
	mRestoreLayerButton->setToolTip("Add new layer from last deleted layer");
	mRestoreLayerButton->setIconSize(QSize(22, 22));
	connect( mRestoreLayerButton, SIGNAL(clicked()), this, SLOT(restoreLayer()) );
	
	mDeleteLayerButton = new QToolButton(this);
	mDeleteLayerButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mDeleteLayerButton->setIcon(QIcon("images/layerIcons/layer_delete.png"));
	mDeleteLayerButton->setIconSize(QSize(22, 22));
	mDeleteLayerButton->setToolTip("delete selected layer");
	connect( mDeleteLayerButton, SIGNAL(clicked()), this, SLOT(deleteActualLayer()) );

	mUpButton = new QToolButton(this);
	mUpButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mUpButton->setIcon(QIcon("images/layerIcons/layer_up.png"));
	mUpButton->setIconSize(QSize(22, 22));
	mUpButton->setToolTip("up");
	connect( mUpButton, SIGNAL(clicked()), this, SLOT(raiseActualLayer()) );

	mDownButton = new QToolButton(this);
	mDownButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
	mDownButton->setIcon(QIcon("images/layerIcons/layer_down.png"));
	mDownButton->setIconSize(QSize(22, 22));
	mUpButton->setToolTip("down");
	connect( mDownButton, SIGNAL(clicked()), this, SLOT(lowerActualLayer()) );

	mNewLayerLayout = new QHBoxLayout;
	mNewLayerLayout->addWidget(mNewLayerButton);
	mNewLayerLayout->addWidget(mNewPaperLayerButton);
	mNewLayerLayout->addWidget(mRestoreLayerButton);

	mNewLayerGB = new QGroupBox("New");
	mNewLayerGB->setLayout(mNewLayerLayout);

	mLayerOptionsLayout = new QHBoxLayout;
	mLayerOptionsLayout->addWidget(mDeleteLayerButton);
	mLayerOptionsLayout->addWidget(mUpButton);
	mLayerOptionsLayout->addWidget(mDownButton);

	mLayerOptionsGB = new QGroupBox("Layer Options");
	mLayerOptionsGB->setLayout(mLayerOptionsLayout);

	//Slider to change selected layers opacity
	mLayerOpacitySlider = new QSlider(Qt::Horizontal);
	mLayerOpacitySlider->setMinimum(0);
	mLayerOpacitySlider->setMaximum(100);
	mLayerOpacitySlider->setTickInterval(10);
	mLayerOpacitySlider->setValue(100);

	mLayerOpacitySpinBox = new QSpinBox(this);
	mLayerOpacitySpinBox->setMinimum(0);
	mLayerOpacitySpinBox->setMaximum(100);
	mLayerOpacitySpinBox->setValue(100);

	mLayerOpacityLayout = new QHBoxLayout;
	mLayerOpacityLayout->addWidget(mLayerOpacitySlider);
	mLayerOpacityLayout->addWidget(mLayerOpacitySpinBox);

	mLayerOpacityGB = new QGroupBox("Layer Opacity");
	mLayerOpacityGB->setLayout(mLayerOpacityLayout);

	connect( mLayerOpacitySlider, SIGNAL(valueChanged(int)), parent, SLOT(setLayerOpacity(int)) );
	connect( mLayerOpacitySlider, SIGNAL(valueChanged(int)), mLayerOpacitySpinBox, SLOT(setValue(int)) );
	connect( mLayerOpacitySpinBox, SIGNAL(valueChanged(int)), mLayerOpacitySlider, SLOT(setValue(int)) );

	mMixingModeBox = new QComboBox();
	mMixingModeBox->addItem(QString("normal"));			//QPainter::CompositionMode_SourceOver
	mMixingModeBox->addItem("add");						//QPainter::CompositionMode_Plus
	mMixingModeBox->addItem("subtract");				//QPainter::CompositionMode_Difference
	mMixingModeBox->addItem("multiply");				//QPainter::CompositionMode_Multiply
	mMixingModeBox->addItem("multiply negative");		//QPainter::CompositionMode_Screen
	mMixingModeBox->addItem("darken");					//QPainter::CompositionMode_Darken
	mMixingModeBox->addItem("lighten");					//QPainter::CompositionMode_Lighten
	mMixingModeBox->addItem("Color dodge");				//QPainter::CompositionMode_ColorDodge
	mMixingModeBox->addItem("Color burn");				//QPainter::CompositionMode_ColorBurn
	mMixingModeBox->addItem("Hard light");				//QPainter::CompositionMode_HardLight
	mMixingModeBox->addItem("Soft light");				//QPainter::CompositionMode_SoftLight
	mMixingModeBox->setEditable(false);

	connect(mMixingModeBox, SIGNAL( currentIndexChanged(int) ), parent, SLOT( mixingModeChanged(int) ));

	mLayerList = new QListWidget();
	mLayerList->setStyleSheet("QListWidget::indicator:checked { image: url(images/layerIcons/layer_visible.png);} \
                               QListWidget::indicator:unchecked { image: url(images/layerIcons/layer_invisible.png)}\
                               QListWidget::indicator:indeterminate { image: url(images/layerIcons/layer_indet.png);}"
	                         );
	mLayerList->setSelectionMode(QAbstractItemView::SingleSelection);
	connect(mLayerList, SIGNAL( itemClicked(QListWidgetItem*) ), this, SLOT( layerClicked(QListWidgetItem*) ) );
	connect(mLayerList, SIGNAL( itemChanged(QListWidgetItem*) ), this, SLOT( layerChanged(QListWidgetItem*) ) );

	mLayersLayout = new QVBoxLayout(this);
	mLayersLayout->addWidget(mNewLayerGB);
	mLayersLayout->addWidget(mLayerOptionsGB);
	mLayersLayout->addWidget(mLayerOpacityGB);
	mLayersLayout->addWidget(mMixingModeBox);
	mLayersLayout->addWidget(mLayerList);
	
	setLayout(mLayersLayout);

	mLogFile.open("LayersWindowLog.txt");
	mLogFile << "Logfile created! \n";

	mLayerCount = 0;
	mLayerCountName = 0;
	mSelectedLayer = -1;

	mLayerNameStack = new QStack<QString>();

	resize(130, 200);
}

LayersWindow::~LayersWindow()
{
	delete mMixingModeBox;
}

void LayersWindow::newProject()
{
	while(mLayerList->count() > 0)
	{
		mLayerList->setCurrentRow(0);
		layerClicked(mLayerList->currentItem());

		deleteActualLayer();
	}
	mLayerCount = 0;
	mLayerCountName = 0;
	newLayer(true);
}

void LayersWindow::setOpacityView(int opacity)
{
	mLayerOpacitySlider->setValue(opacity);
}

void LayersWindow::setMixingModeView(int index)
{
	mMixingModeBox->setCurrentIndex(index);
}

void LayersWindow::newLayer(bool newImage)
{
	if(mLayerList->count() < 30)
	{
		QListWidgetItem* newLayer = new QListWidgetItem;
	
		//create newLayer's name
		QString layerName = QString();
		layerName.append( QString("%1").arg(mLayerCountName) );
		layerName.append(" - Foil");

		//set newLayer's name
		newLayer->setText(layerName);
		newLayer->setCheckState(Qt::Checked);

		//put newLayer on the top of the layer list
		mLayerList->insertItem(0, newLayer);
		mLayerList->setCurrentItem(newLayer);

		mUpButton->setDisabled(false);
		mDownButton->setDisabled(false);

		((MainWindow*)parentWidget())->activateSizeButtons();

		mLayerCount++;
		mLayerCountName++;

		if(newImage)
		{
			//if newImage == true, then add new Image in PaintArea for that layer
			((MainWindow*)parentWidget())->addLayer();
		}
		mSelectedLayer = mLayerList->currentIndex().row();

		mLayerOpacitySlider->setValue(100);
		mMixingModeBox->setCurrentIndex(0);
	}
	else
	{
		((MainWindow*)parentWidget())->setStatusMessage(QString("You cannot have more than 30 layers at a time!"));
	}
}

void LayersWindow::newLayerSlot()
{
	newLayer(true);
}

void LayersWindow::newSimulationLayerSlot()
{
	if(mLayerList->count() < 30)
	{
		QListWidgetItem* newSimulationLayer = new QListWidgetItem;
	
		//create newLayer's name
		QString layerName = QString();
		layerName.append( QString("%1").arg(mLayerCountName) );
		layerName.append(" - Paper");

		//set newLayer's name
		newSimulationLayer->setText(layerName);
		newSimulationLayer->setCheckState(Qt::Checked);

		//put newLayer on the top of the layer list
		mLayerList->insertItem(0, newSimulationLayer);
		mLayerList->setCurrentItem(newSimulationLayer);

		mUpButton->setDisabled(true);
		mDownButton->setDisabled(true);

		((MainWindow*)parentWidget())->deactivateSizeButtons();

		mLayerCount++;
		mLayerCountName++;

		//if newImage == true, then add new Image in PaintArea for that layer
		((MainWindow*)parentWidget())->addSimulationLayer();

		mSelectedLayer = mLayerList->currentIndex().row();

		mLayerOpacitySlider->setValue(100);
		mMixingModeBox->setCurrentIndex(0);
	}
	else
	{
		((MainWindow*)parentWidget())->setStatusMessage(QString("You cannot have more than 30 layers at a time!"));
	}
}

void LayersWindow::restoreLayer()
{
	if(mLayerList->count() < 30)
	{
		if(!mLayerNameStack->isEmpty())
		{
			QListWidgetItem* newLayer = new QListWidgetItem;
			//restore last deleted layer's name
			QString layerName = mLayerNameStack->pop();
			newLayer->setText(layerName);
			newLayer->setCheckState(Qt::Checked);

			//put newLayer on the top of the layer list
			mLayerList->insertItem(0, newLayer);
			//and set restored layer as actual layer
			mLayerList->setCurrentItem(newLayer);
			layerClicked(newLayer);
			mSelectedLayer = mLayerList->currentIndex().row();

			//adjust layer vars
			mLayerCount++;

			//restore deleted layer
			((MainWindow*)parentWidget())->restoreLayer();
		}
		else
		{
			((MainWindow*)parentWidget())->setStatusMessage(QString("There is no delted layer to restore!"));
		}
	}
	else
	{
		((MainWindow*)parentWidget())->setStatusMessage(QString("You cannot have more than 30 layers at a time!"));
	}
}

void LayersWindow::deleteActualLayer()
{
	if(mLayerCount > 0)
	{
		mLogFile << "deleteActualLayer() function called! \n";
		QListWidgetItem* actualLayer;
		actualLayer = mLayerList->takeItem(mLayerList->row( mLayerList->currentItem() ));
		mLayerNameStack->push(actualLayer->text());
		//QListWidgetItem has to be deleted manually
		delete actualLayer;

		((MainWindow*)parentWidget())->deleteActualLayer();
		mLayerCount--;

		if(mLayerCount > 0)
		{
			layerClicked(mLayerList->currentItem());
		}
	}
	else
	{
		mLogFile << "deleteActualLayer() : LayerList is empty! \n";
	}
}

void LayersWindow::raiseActualLayer()
{
	if(mLayerList->count() == 0)
	{
		//layer list is empty
		return;
	}
	if(mLayerList->currentRow() == 0)
	{	
		//top item can not be raised
		return;
	}
		
	((MainWindow*)parentWidget())->raiseActualLayer();

	int currentRow = mLayerList->currentRow();

	QListWidgetItem* currentItem = mLayerList->takeItem(currentRow);
	mLayerList->insertItem(currentRow - 1, currentItem);
	mLayerList->setCurrentRow(currentRow - 1);
	layerClicked(mLayerList->currentItem());
}

void LayersWindow::lowerActualLayer()
{
	if(mLayerList->count() == 0)
	{
		return;
	}
	if(mLayerList->currentRow() == mLayerList->count()-1)
	{	
		//bottom item can not be lowered
		return;
	}

	((MainWindow*)parentWidget())->lowerActualLayer();

	int currentRow = mLayerList->currentRow();

	QListWidgetItem* currentItem = mLayerList->takeItem(currentRow);
	mLayerList->insertItem(currentRow + 1, currentItem);
	mLayerList->setCurrentRow(currentRow + 1);
	layerClicked(mLayerList->currentItem());
}

void LayersWindow::layerClicked( QListWidgetItem* currentListItem )
{
	if(mLayerList->currentItem()->text().contains("Paper"))
	{
		//paper layer chosen
		mUpButton->setDisabled(true);
		mDownButton->setDisabled(true);
		((MainWindow*)parentWidget())->deactivateSizeButtons();
	}
	else
	{
		//foil layer chosen
		mUpButton->setDisabled(false);
		mDownButton->setDisabled(false);
		((MainWindow*)parentWidget())->activateSizeButtons();
	}

	//if mLayerList is empty, do not change selected layer
	if(mLayerList->count() == 0)
	{
		mSelectedLayer = -1;
		return;
	}

	mSelectedLayer = mLayerList->currentIndex().row();
	this->clearFocus();
	mLogFile << "layerClicked(): Selected Layer is now " << mLayerList->currentIndex().row() << " \n";
	((MainWindow*)parentWidget())->setActiveLayer(mSelectedLayer);
}

void LayersWindow::layerChanged( QListWidgetItem* currentListItem )
{
	mLogFile << "Layer at row " << mLayerList->currentIndex().row() << " changed! \n";
	
	int changedLayerIndex = mLayerList->row(currentListItem);
	bool checked = currentListItem->checkState();

	((MainWindow*)parentWidget())->changeLayerVisibility( changedLayerIndex, checked );
}

void LayersWindow::keyPressEvent(QKeyEvent* evt)
{
	mLogFile << "keyPresEvent entered! \n" << endl;
	QKeySequence s1 = QKeySequence(evt->modifiers() + evt->key());
    QKeySequence s2 = QKeySequence("Ctrl+Z");
    if(s1.matches(s2))
	{
		((MainWindow*)parentWidget())->undo();
		mLogFile << "undo! \n" << endl;
	}
}

void LayersWindow::quit()
{
	this->hide();
}
