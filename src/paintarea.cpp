#include <qkeysequence.h>
#include <qstringlist.h>
#include <math.h>
#include "mainwindow.h"
#include "paintarea.h"

PaintArea::PaintArea()
{
    setAttribute(Qt::WA_StaticContents);

	//initialize PaintArea's member vars
	mModified = false;
	mPainting = false;
	mDeviceDown = false;
	mPressureWidth = false;
	mPressureColor = false;
	mTiltWidth = false;
	mTiltColor = false;
	mStrokeWidth = 5;
	mStrokeWidthTablet = 5;
	mStrokeOpacity = 100;
	mToolColorFG = Qt::blue;
	mToolColorBG = Qt::yellow;
	mTabletPenColor = mToolColorFG;
	mFeltpenWetness = 100;

	//kubelka munk properties
	mRw_r = 165;
	mRw_g = 14;
	mRw_b = 83;

	mRb_r = 10;
	mRb_g = 1;
	mRb_b = 4;

	mRw_r /= 255.0;
	mRw_g /= 255.0;
	mRw_b /= 255.0;
	mRb_r /= 255.0;
	mRb_g /= 255.0;
	mRb_b /= 255.0;

	mKubelkaColorW = QColor(165, 14, 83);
	mKubelkaColorB = QColor(10, 1, 4);

	computeKubelkaValues();

	//no button pressed
	mButton = 0;

	//mTool is initialized with 0 (Pen Tool)
	mTool = 0;

	mBackgroundImage = QImage();
	mViewImage = QImage();
	mPaintImage = QImage();
	mPaintPreviewImage = QImage();
	//mLayerList = std::vector<Layer>();
	mRestoreStack = new QStack<Layer>();
	mPointList = QList<QPoint>();

	//create new image
	QImage image = QImage(0, 0, QImage::Format_ARGB32);
	//resize new image
	resizeImage(&image, this->size());
	//clear new image
	image.fill(qRgba(0, 0, 0, 0));

	//spline vars
	mSplineMove = false;
	mSplinePoint1 = QPoint(0, 0);
	mSplinePoint2 = QPoint(0, 0);
	mSplinePoint3 = QPoint(0, 0);

	//undoCount saves the user prefered number of possible undo steps
	mUndoNumber = 20;

	//set initial zoom count
	mZoomCount = 0;
	//set initial zoom factor
	mZoomFactor = 1.0;
	//set initial unzoom factor
	mUnZoomFactor = 1.0;
	//zoomSize for QImage's scaled() function
	QSize mZoomSize = QSize(0, 0);
	QSize mOriginalSize = QSize(0, 0);

	//initialize PaintAreas logfile
	mLogFile.open("paintareaLog.txt");
	mLogFile << "PaintArea's logfile created! \n";

	std::cout << "paintarea constructor called!" << std::endl;

	mBGpattern.load("images/bg_pattern.png");

	mStackInput1Dir = QDir("images/stackInput1/");
	mStackInput2Dir = QDir("images/stackInput2/");
	mStackOutputDir = QDir("images/stackOutput/");

	mViewPaperImage.load("images/textureViewLR.png");
	mPatternImage.load("images/heightMapLR.png");
	mLogFile << "mPatternImage is grayscale? " << mPatternImage.isGrayscale() << "\n" << endl;
	mPaperViewBrush.setTextureImage(mViewPaperImage);

	//create new layer from new image
	mMaxLayerArraySize = 30;
	mActiveLayerNum = -1;
	mLayerArraySize = 0;
	//addLayer();

	mSimulationTimer = new QTimer(this);
	connect(mSimulationTimer, SIGNAL(timeout()), this, SLOT(simulationFunc()));
	connect(mSimulationTimer, SIGNAL(timeout()), this, SLOT(simulationFunc()));
	//mSimulationTimer->start(1000);

	mSimulating = false;
}

PaintArea::~PaintArea()
{
	mLogFile.close();
}

void PaintArea::newProject()
{
	mRestoreStack->clear();
}

void PaintArea::setTabletDevice( QTabletEvent::TabletDevice device ) 
{ 
	mTabletDevice = device; 
}

void PaintArea::tabletEvent(QTabletEvent* evt)
{
	if(mActiveLayerNum == -1 || mLayerArray[mActiveLayerNum].getVisibility() == false)
	{
		return;
	}
	else
	{
	switch (evt->type()) {

		case QEvent::TabletPress:
			if(!mDeviceDown) 
			{
				mDeviceDown = true;	
				if(mTool == 7)
				{
					//by using felt pen tool use the evt as TabletEvent
					mLastFeltPenPos = evt->pos();
					mLastFeltPenPressure = evt->pressure();
					mLastFeltPenXtilt = evt->xTilt();
					mLastFeltPenYtilt = evt->yTilt();

					mLayerArray[mActiveLayerNum].newStroke();

					drawFeltPenPoint();
				}
				else
				{
					mousePressEvent( (QMouseEvent*) evt );
				}
			}
			break;
		case QEvent::TabletRelease:
			if(mDeviceDown)
			{
				if(mTool == 7)
				{
					int offset = 20;
					int x1 = max(1, (mLastFeltPenPos.x() -offset));
					int y1 = max(1, mLastFeltPenPos.y() -offset);
					int x2 = min(this->width()-1, (mLastFeltPenPos.x() +offset)); 
					int y2 = min(this->height()-1, mLastFeltPenPos.y() +offset);
					mLayerArray[mActiveLayerNum].endStroke();

					if(mLayerArray[mActiveLayerNum].isSimulationLayer())
					{
						mLayerArray[mActiveLayerNum].updateCells(QPoint(x1, y1), QPoint(x2, y2), 500, QColor(0, 0, 0));
					}
					else
					{
						mPaintImage = mLayerArray[mActiveLayerNum].getImageRef().copy();
						QPainter painter(&mPaintImage);
						painter.setOpacity(mStrokeOpacity / 100.0);
						painter.drawImage(0, 0, mPaintPreviewImage);
						
						if(mLayerArray[mActiveLayerNum].getUndoStackCount() == mUndoNumber)
						{
							mLayerArray[mActiveLayerNum].pushBackToUndoStack();
						}
						else
						{
							mLayerArray[mActiveLayerNum].pushToUndoStack();
						}

						mLayerArray[mActiveLayerNum].setImage(mPaintImage);
						//clear redoStack of the active layer
						mLayerArray[mActiveLayerNum].clearRedoStack();
						//add ink on activeLayer's ink array
						mPaintPreviewImage.fill(qRgba(0, 0, 0, 0));

						update();
					}
				}
				else
				{
					mouseReleaseEvent( (QMouseEvent*) evt );
				}
				if(evt->pointerType() == QTabletEvent::Eraser)
				{
					mTool = 0;
				}
				mDeviceDown = false;
			}
            break;
		case QEvent::TabletMove:

			if(mDeviceDown)
			{
				if(evt->pointerType() == QTabletEvent::Eraser)
				{
					//set tool to eraser
					mTool = 1;
				}
				if(mPressureWidth)
				{
					mStrokeWidthTablet = mStrokeWidth * evt->pressure();
				}
				if(mPressureColor)
				{
					mTabletPenColor.setRed( mToolColorFG.red() * (1.0 - evt->pressure()) + mToolColorBG.red() * evt->pressure() );
					mTabletPenColor.setGreen( mToolColorFG.green() * (1.0 - evt->pressure()) + mToolColorBG.green() * evt->pressure() );
					mTabletPenColor.setBlue( mToolColorFG.blue() * (1.0 - evt->pressure()) + mToolColorBG.blue() * evt->pressure() );
				}

				if(mTiltWidth)
				{
					if( (abs(evt->xTilt())) > (abs(evt->yTilt())) )
					{
						mStrokeWidthTablet = mStrokeWidth * abs(evt->xTilt())/30;
					}
					else
					{
						mStrokeWidthTablet = mStrokeWidth * abs(evt->yTilt())/30;
					}
				}
				if(mTiltColor)
				{
					if( (abs(evt->xTilt())) > (abs(evt->yTilt())) )
					{
						mTabletPenColor.setRed( mToolColorFG.red() * (1.0 - float(abs(evt->xTilt())/60.0)) + mToolColorBG.red() * float(abs(evt->xTilt())/60.0) );
						mTabletPenColor.setGreen( mToolColorFG.green() * (1.0 - float(abs(evt->xTilt())/60.0)) + mToolColorBG.green() * float(abs(evt->xTilt())/60.0) );
						mTabletPenColor.setBlue( mToolColorFG.blue() * (1.0 - float(abs(evt->xTilt())/60.0)) + mToolColorBG.blue() * float(abs(evt->xTilt())/60.0) );
						mLogFile << "xTilt = " << float(abs(evt->xTilt())/60.0) << " \n" << endl;
					}
					else
					{
						mTabletPenColor.setRed( mToolColorFG.red() * (1.0 - float(abs(evt->yTilt())/60.0)) + mToolColorBG.red() * float(abs(evt->yTilt())/60.0) );
						mTabletPenColor.setGreen( mToolColorFG.green() * (1.0 - float(abs(evt->yTilt())/60.0)) + mToolColorBG.green() * float(abs(evt->yTilt())/60.0) );
						mTabletPenColor.setBlue( mToolColorFG.blue() * (1.0 - float(abs(evt->yTilt())/60.0)) + mToolColorBG.blue() * float(abs(evt->yTilt())/60.0) );
						mLogFile <<"yTilt = " << float(abs(evt->yTilt())/60.0) << " \n" << endl;
					}
				}

				if(mTool == 7)
				{
					//by using felt pen tool use the evt as TabletEvent
					drawFeltPenLine(evt);
					mLogFile << "Tilt: " << evt->xTilt() << std::endl;
				}
				else
				{
					mouseMoveEvent( (QMouseEvent*) evt );
				}
            }
			else
			{
				//update status bar coordinates
				((MainWindow*)parentWidget())->eventFilter(NULL, evt);
			}
            break;
         default:
            break;
		}
		update();
	}
}

void PaintArea::simulationFunc()
{

}

void PaintArea::setTool( int toolNumber )
{
	mTool = toolNumber;
}

void PaintArea::setUndoNumber( int undoNumber )
{
	mUndoNumber = undoNumber;
}

void PaintArea::setToolWidth( int strokeWidth )
{
	mStrokeWidth = strokeWidth;
	mStrokeWidthTablet = strokeWidth;
}

void PaintArea::setToolOpacity( int penOpacity )
{
	mStrokeOpacity = penOpacity;
}

void PaintArea::setFeltpenWetness(int wetness)
{
	mFeltpenWetness = wetness;
	mLayerArray[mActiveLayerNum].setWetnessFactor(mFeltpenWetness);
}

void PaintArea::setPressureWidth(bool checked)
{
	mPressureWidth = checked;
	if(!checked)
	{
		mStrokeWidthTablet = mStrokeWidth;
	}
	mLogFile << "mPressureWidth = " << checked << "\n" << endl;
}

void PaintArea::setPressureColor(bool checked)
{
	mPressureColor = checked;
	if(!checked)
	{
		mTabletPenColor = mToolColorFG;
	}
	mLogFile << "mPressureColor = " << checked << "\n" << endl;
}

void PaintArea::setTiltWidth(bool checked)
{
	mTiltWidth = checked; 
	if(!checked)
	{
		mStrokeWidthTablet = mStrokeWidth;
	}
	mLogFile << "mTiltWidth = " << checked << "\n" << endl;
}

void PaintArea::setTiltColor(bool checked)
{
	mTiltColor = checked;
	if(!checked)
	{
		mTabletPenColor = mToolColorFG;
	}
	mLogFile << "mTiltColor = " << checked << "\n" << endl;
}

void PaintArea::setToolColorFG( const QColor& color )
{
	mToolColorFG = color;
	mTabletPenColor = color;
}

void PaintArea::setToolColorBG( const QColor& color )
{
	mToolColorBG = color;
}

void PaintArea::setKubelkaColorW( const QColor& color )
{
	mKubelkaColorW = color;
	mRw_b = float(color.blue()) / 255.0;
	mRw_g = float(color.green()) / 255.0;
	mRw_r = float(color.red()) / 255.0;

	computeKubelkaValues();
}

void PaintArea::setKubelkaColorB( const QColor& color )
{
	mKubelkaColorB = color;
	mRb_b = float(color.blue()) / 255.0;
	mRb_g = float(color.green()) / 255.0;
	mRb_r = float(color.red()) / 255.0;

	computeKubelkaValues();
}

void PaintArea::computeKubelkaValues()
{
	float next_r = (mRb_r - mRw_r + 1.0) / mRb_r;
	float next_g = (mRb_g - mRw_g + 1.0) / mRb_g;
	float next_b = (mRb_b - mRw_b + 1.0) / mRb_b;

	ma_r = 0.5 * (mRw_r + next_r); 
	ma_g = 0.5 * (mRw_g + next_g); 
	ma_b = 0.5 * (mRw_b + next_b); 
	
	mb_r = ( sqrt(ma_r * ma_r - 1.0) );
	mb_g = ( sqrt(ma_g * ma_g - 1.0) );
	mb_b = ( sqrt(ma_b * ma_b - 1.0) );

	float xR = ( (mb_r * mb_r - (ma_r - mRw_r) * (ma_r - 1.0))
					/ (mb_r * (1.0 - mRw_r)) );
	float xG = ( (mb_g * mb_g - (ma_g - mRw_g) * (ma_g - 1.0))
					/ (mb_g * (1.0 - mRw_g)) );
	float xB = ( (mb_b * mb_b - (ma_b - mRw_b) * (ma_b - 1.0))
					/ (mb_b * (1.0 - mRw_b)) );

	mS_r = ( 1.0 / mb_r * (0.5 * log( (1.0 + (1.0 / xR) ) / (1.0 - (1.0 / xR)) ) ) ); 
	mS_g = ( 1.0 / mb_g * (0.5 * log( (1.0 + (1.0 / xG) ) / (1.0 - (1.0 / xG)) ) ) ); 

	float x1 = (1.0 + (1.0 / xB) ); 
	float x2 = (1.0 - (1.0 / xB)); 
	float x3 = (0.5 * log( x1 / x2 ) );
	float x4 = ( 1.0 / mb_b * x3 );

	mS_b = x4; 

	double x = 0.5;
	double mc_g_lh = ma_g * sinh(mb_g * mS_g * x);
	double mc_b_rh = mb_b * cosh(mb_b * mS_b * x); 
	double mc_b_lh = ma_b * sinh(mb_b * mS_b * x);
	double mc_r_rh = mb_r * cosh(mb_r * mS_r * x);
	double mc_r_lh = ma_r * sinh(mb_r * mS_r * x);
	double mc_g_rh = mb_g * cosh(mb_g * mS_g * x);

	mc_r = (mc_r_lh + mc_r_rh);
	mc_g = (mc_g_lh + mc_g_rh);
	mc_b = (mc_b_lh + mc_b_rh);

	if(mc_r == 0.0) mc_r = 0.001;
	if(mc_g == 0.0) mc_g = 0.001;
	if(mc_b == 0.0) mc_b = 0.001;

	mK_r = ( mS_r * (ma_r - 1.0) ); 
	mK_g = ( mS_g * (ma_g - 1.0) ); 
	mK_b = ( mS_b * (ma_b - 1.0) ); 

	mR_r = ( sinh(mb_r * mS_r * x / mc_r));
	mR_g = ( sinh(mb_g * mS_g * x / mc_g));
	mR_b = ( sinh(mb_b * mS_b * x / mc_b));

	mT_r = ( mb_r / mc_r ); 
	mT_g = ( mb_g / mc_g );
	mT_b = ( mb_b / mc_b );

	for(int i = 0; i < mMaxLayerArraySize; i++)
	{
		if(mLayerArray[i].isSimulationLayer())
		{
			mLayerArray[i].setKubelkaColorVars(mK_r, mK_g, mK_b, mS_r, mS_g, mS_b, ma_r, ma_g, ma_b, mb_r, mb_g, mb_b);
		}
	}
}

void PaintArea::setNewOriginalSize(QSize newSize)
{
	mOriginalSize = newSize;
}

void PaintArea::undo()
{
	mLogFile << "paintarea undo! \n" << endl;
	mLayerArray[mActiveLayerNum].undo();
	update();
}

void PaintArea::redo()
{
	mLogFile << "paintarea undo! \n" << endl;
	mLayerArray[mActiveLayerNum].redo();
	update();
}

void PaintArea::zoomIn()
{
	mZoomCount++;
	mZoomFactor *= 1.25;
	mUnZoomFactor *= 0.8;
	this->resize(mOriginalSize.width() * mZoomFactor,  mOriginalSize.height() * mZoomFactor );
	//adjust mZoomSize for the QImage's scaled() function
	mZoomSize.setWidth( mOriginalSize.width() * mZoomFactor );
	mZoomSize.setHeight( mOriginalSize.height() * mZoomFactor );
	resizeImage(&mViewImage, this->size());
	update();
}

void PaintArea::zoomOut()
{
	mZoomCount--;
	mZoomFactor *= 0.8;
	mUnZoomFactor *= 1.25;
	this->resize(mOriginalSize.width() * mZoomFactor,  mOriginalSize.height() * mZoomFactor );
	//adjust mZoomSize for the QImage's scaled() function
	mZoomSize.setWidth( mOriginalSize.width() * mZoomFactor );
	mZoomSize.setHeight( mOriginalSize.height() * mZoomFactor );
	resizeImage(&mViewImage, this->size());
	update();
}

void PaintArea::resetSize()
{
	mZoomCount = 0;
	mZoomFactor = 1.0;
	mUnZoomFactor = 1.0;
	this->resize(mOriginalSize);
	//adjust mZoomSize for the QImage's scaled() function
	resizeImage(&mViewImage, this->size());
	mZoomSize = mOriginalSize;
	update();
}

void PaintArea::mousePressEvent( QMouseEvent* evt )
{
	if( mActiveLayerNum == -1 || mLayerArray[mActiveLayerNum].getVisibility() == false)
	{
		return;
	}

	if(mZoomCount != 0)
	{	
        QMouseEvent* zoomEvt = new QMouseEvent(evt->type(), QPoint(evt->pos().x()*mUnZoomFactor, evt->pos().y()*mUnZoomFactor), evt->button(), evt->buttons(), evt->modifiers());
		evt = zoomEvt;
	}

	//save the mouse button
	if (evt->button() == Qt::LeftButton) 
	{
		mButton = 1;
	}
	else if (evt->button() == Qt::RightButton) 
	{
		mButton = 2;
	}
		
	mPainting = true;
	mModified = true;
	mLastPoint = evt->pos();

	switch(mTool)
	{
		case 0:
			//pen tool
			drawPoint(mLastPoint);
		break;
					
		case 1:
			//eraser tool
			drawEraserPoint(mLastPoint);
		break;

		case 3:
			//spline tool
			if(!mSplineMove)
			{
				mSplinePoint1 = evt->pos();
			}
		case 8:
			//fill tool
			fillContour(mLastPoint, mLayerArray[mActiveLayerNum].getImageRef().pixel(evt->x(),evt->y()));
		break;
	}
}

void PaintArea::mouseMoveEvent( QMouseEvent* evt )
{
	((MainWindow*)parentWidget())->eventFilter(NULL, evt);

    if( mPainting )
	{
		if(mZoomCount != 0)
		{	
			//user has zoomed
            QMouseEvent* zoomEvt = new QMouseEvent(evt->type(), QPoint(evt->pos().x()*mUnZoomFactor, evt->pos().y()*mUnZoomFactor), evt->button(), evt->buttons(), evt->modifiers());
			evt = zoomEvt;
		}
		switch(mTool)
		{
			case 0:
				drawLineTo(evt->pos());
			break;

			case 1:
				drawEraserLineTo(evt->pos());
			break;

			case 2:
				drawLine(evt->pos());
			break;

			case 3:
				if(mSplineMove)
				{
					mSplinePoint2 = evt->pos();
				}
				drawSpline(evt->pos());
			break;

			case 4:
				drawRect(evt->pos());
			break;

			case 5:
				drawEllipse(evt->pos());
			break;

			case 6:
				drawExpressive(evt->pos());
			break;
		}
	}
}

void PaintArea::mouseReleaseEvent( QMouseEvent* evt )
{
    if(mPainting) 
	{
		if(mTool == 3)
		{
			if(!mSplineMove)
			{
				if(mZoomCount != 0)
				{	
                    QMouseEvent* zoomEvt = new QMouseEvent(evt->type(), QPoint(evt->pos().x()*mUnZoomFactor, evt->pos().y()*mUnZoomFactor), evt->button(), evt->buttons(), evt->modifiers());
					evt = zoomEvt;
				}
				mSplinePoint3 = evt->pos();
				mSplineMove = true;
			}
		}
		
		if(mTool == 0 || mTool == 1)
		{
			if(!mLayerArray[mActiveLayerNum].isSimulationLayer())
			{
				mPaintImage = mLayerArray[mActiveLayerNum].getImageRef().copy();
				QPainter painter(&mPaintImage);
				painter.setOpacity(mStrokeOpacity / 100.0);
				if(mTool == 0)
				{	//pen mode
					painter.setCompositionMode(mLayerArray[mActiveLayerNum].getMixingMode());
				}
				if(mTool == 1)
				{	//eraser mode
					painter.setCompositionMode(QPainter::CompositionMode_DestinationOut);
				}
				painter.drawImage(0, 0, mPaintPreviewImage);

				//store the old image for undo function
				if(mLayerArray[mActiveLayerNum].getUndoStackCount() == mUndoNumber)
				{
					mLayerArray[mActiveLayerNum].pushBackToUndoStack();
				}
				else
				{
					mLayerArray[mActiveLayerNum].pushToUndoStack();
				}
				mLayerArray[mActiveLayerNum].setImage(mPaintImage);
				//clear redoStack of the active layer
				mLayerArray[mActiveLayerNum].clearRedoStack();

				mPaintPreviewImage.fill(qRgba(0, 0, 0, 0));

				update();
			}
		}

		if(mTool == 2 || mTool == 4 || mTool == 5 || mTool == 6)
		{
			mPaintImage = mLayerArray[mActiveLayerNum].getImageRef().copy();
			QPainter painter(&mPaintImage);
			painter.setOpacity(mStrokeOpacity / 100.0);
			painter.setCompositionMode(mLayerArray[mActiveLayerNum].getMixingMode());
			if(mTool == 6)
			{
				//expressive tool
				QPainterPath path;
				if(!mPointList.isEmpty())
				{
					path.moveTo(mPointList.at(0));
				}
				for( int i = 0; i < mPointList.length(); i++ )
				{
					if( (i + 12) < mPointList.length() )
					{
						path.cubicTo(	mPointList.at(i).x(), mPointList.at(i).y(), 
										mPointList.at(i+6).x(), mPointList.at(i+6).y(), 
										mPointList.at(i+12).x(), mPointList.at(i+12).y() );
					}
					else
					{
						break;
					}
				}
				mPointList.clear();
				//path.simplified();
				if(mButton == 1 || mDeviceDown)
				{	//left button pressed, painting with left color
					painter.setPen(QPen(mToolColorFG, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
				}
				else if(mButton == 2)
				{	//right button pressed, painting with right color
					painter.setPen(QPen(mToolColorBG, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
				}
				painter.drawPath(path);
			}
			else
			{
				painter.drawImage(0, 0, mPaintPreviewImage);
			}
			//store the old image for undo function
			if(mLayerArray[mActiveLayerNum].getUndoStackCount() == mUndoNumber)
			{
				mLayerArray[mActiveLayerNum].pushBackToUndoStack();
			}
			else
			{
				mLayerArray[mActiveLayerNum].pushToUndoStack();
			}
			mLayerArray[mActiveLayerNum].setImage(mPaintImage);
			//clear redoStack of the active layer
			mLayerArray[mActiveLayerNum].clearRedoStack();

			mPaintPreviewImage.fill(qRgba(0, 0, 0, 0));
			update();
		}

		//reset mouse button and mPainting var
		mButton = 0;
		mPainting = false;
    }
}

void PaintArea::paintEvent( QPaintEvent* evt )
{
	QRect changedRect = evt->rect();
	mViewImage.fill(qRgba(0, 0, 0, 0));
	QPainter viewImagePainter(&mViewImage);

	//draw all layers from bottom to top
	for( int i = 0; i < mLayerArraySize; i++ )
	{  
		if(i == mActiveLayerNum)
		{
			if(mLayerArray[mActiveLayerNum].getVisibility() == true)
			{	
				viewImagePainter.setCompositionMode(mLayerArray[mActiveLayerNum].getMixingMode());
				viewImagePainter.setOpacity(mLayerArray[mActiveLayerNum].getOpacity() / 100.0);
				if(mLayerArray[mActiveLayerNum].isSimulationLayer())
				{
					viewImagePainter.drawImage(changedRect, mLayerArray[mActiveLayerNum].getInkImageRef(), changedRect);
				}else{
					viewImagePainter.drawImage(changedRect, mLayerArray[mActiveLayerNum].getImageRef().scaled(mZoomSize), changedRect);
				}
				viewImagePainter.setOpacity( (mStrokeOpacity / 100.0) * (float)(mLayerArray[mActiveLayerNum].getOpacity() / 100.0) );
				viewImagePainter.drawImage(changedRect, mPaintPreviewImage.scaled(mZoomSize), changedRect);
			}
		}
		else
		{
			if(mLayerArray[i].getVisibility() == true)
			{	
				viewImagePainter.setCompositionMode(mLayerArray[i].getMixingMode());
				viewImagePainter.setOpacity(mLayerArray[i].getOpacity() / 100.0);
				if(mLayerArray[i].isSimulationLayer())
				{
					viewImagePainter.drawImage(changedRect, mLayerArray[i].getInkImageRef(), changedRect);
				}else{
					viewImagePainter.drawImage(changedRect, mLayerArray[i].getImageRef().scaled(mZoomSize), changedRect);
				}
				viewImagePainter.setOpacity((float)(mLayerArray[i].getOpacity() / 100.0) );	
			}
		}
	}
	viewImagePainter.end();

	QPainter viewPainter(this);
		viewPainter.setCompositionMode(QPainter::CompositionMode_SourceOver);
		viewPainter.drawImage(changedRect, mBackgroundImage.scaled(mZoomSize), changedRect);
		viewPainter.drawImage(changedRect, mViewImage, changedRect);
	viewPainter.end();
}

void PaintArea::drawPoint( const QPoint& newPoint )
{
	if(mLayerArray[mActiveLayerNum].isSimulationLayer())
	{
		QPainter painter(&mLayerArray[mActiveLayerNum].getInkImageRef());
		QColor paintColor;

		if(mDeviceDown)
		{
			paintColor = mToolColorFG;
			painter.setPen(QPen(paintColor, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		}
		if(mButton == 1)
		{
			//left button pressed
			paintColor = mToolColorFG;
			painter.setPen(QPen(paintColor, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		}
		else if(mButton == 2)
		{
			//right button pressed, painting with right color
			paintColor = mToolColorBG;
			painter.setPen(QPen(paintColor, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		}

		painter.drawPoint(newPoint);
		painter.end();

		int rad = (mStrokeWidth) + 2;
		update(QRect(newPoint, newPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
		mLayerArray[mActiveLayerNum].updateCells(QPoint(newPoint.x()-rad, newPoint.y()-rad), 
												 QPoint(newPoint.x()+rad, newPoint.y()+rad), 
												 -1, 
												 paintColor
												);
	}
	else
	{
		QPainter painter(&mPaintPreviewImage);

		//left button pressed, painting with left color
		if(mDeviceDown)
		{
			painter.setPen(QPen(mTabletPenColor, mStrokeWidthTablet, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		}
		if(mButton == 1)
		{
			painter.setPen(QPen(mToolColorFG, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		}
		else if(mButton == 2)
		{
			//right button pressed, painting with right color
			painter.setPen(QPen(mToolColorBG, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		}

		painter.drawPoint(newPoint);
		int rad = (mStrokeWidth / 2) + 2;

		if(mZoomCount != 0)
		{
			update(QRect(newPoint*mZoomFactor, newPoint*mZoomFactor).normalized().adjusted(-mZoomFactor*rad, -mZoomFactor*rad, +mZoomFactor*rad, +mZoomFactor*rad));
		}
		else
		{
			update(QRect(newPoint, newPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
		}
	}
}

void PaintArea::drawLineTo( const QPoint& endPoint )
{
	if(mLayerArray[mActiveLayerNum].isSimulationLayer())
	{
		QPainter painter(&mLayerArray[mActiveLayerNum].getInkImageRef());
		QColor paintColor;
		//left button pressed, painting with left color
		if(mDeviceDown)
		{
			paintColor = mToolColorFG;
			painter.setPen(QPen(paintColor, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		}
		if(mButton == 1)
		{
			//left button pressed
			paintColor = mToolColorFG;
			painter.setPen(QPen(paintColor, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		}
		else if(mButton == 2)
		{
			//right button pressed, painting with right color
			paintColor = mToolColorBG;
			painter.setPen(QPen(paintColor, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		}

		painter.drawLine(mLastPoint, endPoint);
		painter.end();

		int rad = (mStrokeWidth / 2) + 2;
		update(QRect(mLastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
		mLayerArray[mActiveLayerNum].updateCells(QPoint(max(1, min(mLastPoint.x(), endPoint.x()) -rad), 
														max(1, min(mLastPoint.y(), endPoint.y()) -rad)), 
									 QPoint(min(this->width()-1, max(mLastPoint.x(), endPoint.x()) +rad), 
											min(this->height()-1, max(mLastPoint.y(), endPoint.y()) +rad)), 
											-1, 
											paintColor);
		mLastPoint = endPoint;
	}
	else
	{
		QPainter painter(&mPaintPreviewImage);

		//left button pressed, painting with left color
		if(mDeviceDown)
		{
			painter.setPen(QPen(mTabletPenColor, mStrokeWidthTablet, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		}
		if(mButton == 1)
		{
			painter.setPen(QPen(mToolColorFG, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		}
		else if(mButton == 2)
		{
			//right button pressed, painting with right color
			painter.setPen(QPen(mToolColorBG, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		}
		
		painter.drawLine(mLastPoint, endPoint);

		int rad = (mStrokeWidth / 2) + 2;
		if(mZoomCount != 0)
		{
			update(QRect(mLastPoint*mZoomFactor, endPoint*mZoomFactor).normalized().adjusted(-mZoomFactor*rad, -mZoomFactor*rad, +mZoomFactor*rad, +mZoomFactor*rad));
		}
		else
		{
			update(QRect(mLastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
		}
		mLastPoint = endPoint;
	}
}

void PaintArea::drawFeltPenPoint()
{
}

void PaintArea::drawFeltPenLine( QTabletEvent* evt )
{
	float tiltFac = 0.15;
	unsigned int addWetness;
	int rad = (mStrokeWidth / 2) + 2;
	qreal q;
	
	QPoint points[4] = 
	{
		QPoint(mLastFeltPenPos.x()*mUnZoomFactor + tiltFac * mLastFeltPenXtilt, mLastFeltPenPos.y()*mUnZoomFactor + tiltFac * mLastFeltPenYtilt),
		QPoint(mLastFeltPenPos.x()*mUnZoomFactor - tiltFac * mLastFeltPenXtilt, mLastFeltPenPos.y()*mUnZoomFactor - tiltFac * mLastFeltPenYtilt),
		QPoint(evt->pos().x()*mUnZoomFactor - tiltFac * evt->xTilt(), evt->pos().y()*mUnZoomFactor - tiltFac * evt->yTilt()),
		QPoint(evt->pos().x()*mUnZoomFactor + tiltFac * evt->xTilt(), evt->pos().y()*mUnZoomFactor + tiltFac * evt->yTilt())
	};

	QPainter painter;
	float A = 1;
	//compute square area
	A = max(99.0, 50.0 + 0.5 * abs(
			(float(points[0].y())-float(points[2].y()))
		* (float(points[3].x())-float(points[1].x()))
		+ (float(points[1].y())-float(points[3].y()))
		* (float(points[0].x())-float(points[2].x()))
										)
				);

	q = (100.0 / (A + 1.0) * evt->pressure());

	if(mLayerArray[mActiveLayerNum].isSimulationLayer())
	{
		qreal x = 0.2 * q;
			
		mc_b = ma_b * sinh(mb_b * mS_b * x) + mb_b * cosh( mb_b * mS_b * x);
		mc_g = ma_g * sinh(mb_g * mS_g * x) + mb_g * cosh( mb_g * mS_g * x);
		mc_r = ma_r * sinh(mb_r * mS_r * x) + mb_r * cosh( mb_r * mS_r * x);

		mR_b = sinh(mb_b * mS_b * x) / mc_b;
		mR_g = sinh(mb_g * mS_g * x) / mc_g;
		mR_r = sinh(mb_r * mS_r * x) / mc_r;

		mT_b = mb_b / mc_b;
		mT_g = mb_g / mc_g;
		mT_r = mb_r / mc_r;

		painter.begin(&mLayerArray[mActiveLayerNum].getAddInkImageRef_R());
			painter.setPen(QPen(Qt::transparent, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
			QColor color_R = QColor(mR_r * 255.0, mR_g * 255.0, mR_b * 255.0);
			painter.setBrush(QBrush(color_R, Qt::SolidPattern));
			painter.drawConvexPolygon(points, 4);
		painter.end();

		painter.begin(&mLayerArray[mActiveLayerNum].getAddInkImageRef_T());
			painter.setPen(QPen(Qt::transparent, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
			QColor color_T = QColor(mT_r * 255, mT_g * 255, mT_b * 255);
			painter.setBrush(QBrush(color_T, Qt::SolidPattern));
			painter.drawConvexPolygon(points, 4);
		painter.end();

		if(mFeltpenWetness > 15)
		{
			float wetnessFactor = mFeltpenWetness / 100.0;
			A *= wetnessFactor;
			addWetness = (int)A;
		}
		else
		{
			addWetness = 0;
		}

		int offset = 20;
		mLayerArray[mActiveLayerNum].updateCells(QPoint(max(1, min(min(points[0].x(), points[1].x()), min(points[2].x(), points[3].x())) -offset), 
										max(1, min(min(points[0].y(), points[1].y()), min(points[2].y(), points[3].y())) -offset)), 
									QPoint(min(this->width()-1, max(max(points[0].x(), points[1].x()), max(points[2].x(), points[3].x())) +offset), 
										min(this->height()-1, max(max(points[0].y(), points[1].y()), max(points[2].y(), points[3].y())) +offset)), 
										addWetness, 
										QColor(0, 0, 0)
								);
	}
	else
	{
		painter.begin(&mPaintPreviewImage);
		painter.setPen(QPen(Qt::transparent, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		painter.setOpacity(q);
		painter.setBrush(QBrush(mKubelkaColorW, Qt::SolidPattern));
		painter.drawConvexPolygon(points, 4);
		painter.end();
	}

	mLastFeltPenPos = evt->pos();
	mLastFeltPenPressure = evt->pressure();
	mLastFeltPenXtilt = evt->xTilt();
	mLastFeltPenYtilt = evt->yTilt();
}

void PaintArea::drawEraserPoint( const QPoint& newPoint )
{
	if(mLayerArray[mActiveLayerNum].isSimulationLayer())
	{
		return;
	}

	QPainter painter(&mPaintPreviewImage);

	if(mDeviceDown)
	{
		painter.setPen(QPen(Qt::gray, mStrokeWidthTablet, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	}
	else
	{
		painter.setPen(QPen(Qt::gray, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	}

	painter.drawPoint(newPoint);

	int rad = (mStrokeWidth / 2) + 2;
		
	if(mZoomCount != 0)
	{
		update(QRect(newPoint*mZoomFactor, newPoint*mZoomFactor).normalized().adjusted(-mZoomFactor*rad, -mZoomFactor*rad, +mZoomFactor*rad, +mZoomFactor*rad));
	}
	else
	{
		update(QRect(newPoint, newPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	}
 }

void PaintArea::drawEraserLineTo( const QPoint& endPoint )
{
	if(mLayerArray[mActiveLayerNum].isSimulationLayer())
	{
		return;
	}

	QPainter painter(&mPaintPreviewImage);

	if(mDeviceDown)
	{
		painter.setPen(QPen(Qt::gray, mStrokeWidthTablet, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	}
	else
	{
		painter.setPen(QPen(Qt::gray, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	}

	painter.drawLine(mLastPoint, endPoint);

	int rad = (mStrokeWidth / 2) + 2;

	if(mZoomCount != 0)
	{
		update(QRect(mLastPoint*mZoomFactor, endPoint*mZoomFactor).normalized().adjusted(-mZoomFactor*rad, -mZoomFactor*rad, +mZoomFactor*rad, +mZoomFactor*rad));
	}
	else
	{
		update(QRect(mLastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	}

	mLastPoint = endPoint;
}

void PaintArea::drawLine(const QPoint& secondPoint)
{
	if(mLayerArray[mActiveLayerNum].isSimulationLayer())
	{
		return;
	}

	//draw the line on mPaintPreviewImage to get a preview
	mPaintPreviewImage.fill(qRgba(0, 0, 0, 0));
	QPainter painter(&mPaintPreviewImage);

	if(mButton == 1 || mDeviceDown)
	{
		//left button pressed, painting with left color
		painter.setPen(QPen(mToolColorFG, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	}
	else if(mButton == 2)
	{
		//right button pressed, painting with right color
		painter.setPen(QPen(mToolColorBG, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	}

	painter.drawLine(mLastPoint, secondPoint);

	int rad = (mStrokeWidth / 2) + 200;
	update(QRect(mLastPoint, secondPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
}

void PaintArea::drawSpline(const QPoint& secondPoint)
{
	if(mLayerArray[mActiveLayerNum].isSimulationLayer())
	{
		return;
	}

	//draw the spline on mPaintPreviewImage to get a preview
	mPaintPreviewImage.fill(qRgba(0, 0, 0, 0));

	if(mSplineMove)
	{
		QPainterPath path;
		path.moveTo(mSplinePoint1);
		path.cubicTo( mSplinePoint1, mSplinePoint2, mSplinePoint3);

		QPainter painter(&mPaintPreviewImage);
		if(mButton == 1 || mDeviceDown)
		{
			//left button pressed, painting with left color
			painter.setPen(QPen(mToolColorFG, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		}
		else if(mButton == 2)
		{
			//right button pressed, painting with right color
			painter.setPen(QPen(mToolColorBG, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		}
		painter.drawPath(path);

		int rad = (mStrokeWidth / 2) + 200;
		update();
	}
	else
	{
		mPaintPreviewImage.fill(qRgba(0, 0, 0, 0));
		QPainter painter(&mPaintPreviewImage);

		if(mButton == 1 || mDeviceDown)
		{
			//left button pressed, painting with left color
			painter.setPen(QPen(mToolColorFG, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		}
		else if(mButton == 2)
		{
			//right button pressed, painting with right color
			painter.setPen(QPen(mToolColorBG, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		}

		painter.drawLine(mLastPoint, secondPoint);

		int rad = (mStrokeWidth / 2) + 200;
		update(QRect(mLastPoint, secondPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	}
}

void PaintArea::drawRect(const QPoint& secondPoint)
{
	if(mLayerArray[mActiveLayerNum].isSimulationLayer())
	{
		return;
	}

	//draw the rectangle on mPaintPreviewImage to get a preview
	mPaintPreviewImage.fill(qRgba(0, 0, 0, 0));
	QPainter painter(&mPaintPreviewImage);

	if(mButton == 1 || mDeviceDown)
	{
		//left button pressed, painting with left color
		painter.setPen(QPen(mToolColorFG, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	}
	else if(mButton == 2)
	{
		//right button pressed, painting with right color
		painter.setPen(QPen(mToolColorBG, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	}

	if(mLastPoint.x() <= secondPoint.x() && mLastPoint.y() <= secondPoint.y())
	{
		painter.drawRect(mLastPoint.x(), mLastPoint.y(), abs(mLastPoint.x()-secondPoint.x()), abs(mLastPoint.y()-secondPoint.y()));
	}
	if(mLastPoint.x() >= secondPoint.x() && mLastPoint.y() >= secondPoint.y())
	{
		painter.drawRect(secondPoint.x(), secondPoint.y(), abs(mLastPoint.x()-secondPoint.x()), abs(mLastPoint.y()-secondPoint.y()));
	}
	if(mLastPoint.x() >= secondPoint.x() && mLastPoint.y() <= secondPoint.y())
	{
		painter.drawRect(secondPoint.x(), mLastPoint.y(), abs(mLastPoint.x()-secondPoint.x()), abs(mLastPoint.y()-secondPoint.y()));
	}
	if(mLastPoint.x() <= secondPoint.x() && mLastPoint.y() >= secondPoint.y())
	{
		painter.drawRect(mLastPoint.x(), secondPoint.y(), abs(mLastPoint.x()-secondPoint.x()), abs(mLastPoint.y()-secondPoint.y()));
	}

	int rad = (mStrokeWidth / 2) + 200;
	update(QRect(mLastPoint, secondPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
}

void PaintArea::drawEllipse(const QPoint& secondPoint)
{
	if(mLayerArray[mActiveLayerNum].isSimulationLayer())
	{
		return;
	}

	//draw the ellipse on mPaintPreviewImage to get a preview
	mPaintPreviewImage.fill(qRgba(0, 0, 0, 0));
	QPainter painter(&mPaintPreviewImage);

	if(mButton == 1 || mDeviceDown)
	{
		//left button pressed, painting with left color
		painter.setPen(QPen(mToolColorFG, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	}
	else if(mButton == 2)
	{
		//right button pressed, painting with right color
		painter.setPen(QPen(mToolColorBG, mStrokeWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	}

	if(mLastPoint.x() <= secondPoint.x() && mLastPoint.y() <= secondPoint.y())
	{
		painter.drawEllipse(mLastPoint.x(), mLastPoint.y(), abs(mLastPoint.x()-secondPoint.x()), abs(mLastPoint.y()-secondPoint.y()));
	}
	if(mLastPoint.x() >= secondPoint.x() && mLastPoint.y() >= secondPoint.y())
	{
		painter.drawEllipse(secondPoint.x(), secondPoint.y(), abs(mLastPoint.x()-secondPoint.x()), abs(mLastPoint.y()-secondPoint.y()));
	}
	if(mLastPoint.x() >= secondPoint.x() && mLastPoint.y() <= secondPoint.y())
	{
		painter.drawEllipse(secondPoint.x(), mLastPoint.y(), abs(mLastPoint.x()-secondPoint.x()), abs(mLastPoint.y()-secondPoint.y()));
	}
	if(mLastPoint.x() <= secondPoint.x() && mLastPoint.y() >= secondPoint.y())
	{
		painter.drawEllipse(mLastPoint.x(), secondPoint.y(), abs(mLastPoint.x()-secondPoint.x()), abs(mLastPoint.y()-secondPoint.y()));
	}

	int rad = (mStrokeWidth / 2) + 200;
	update(QRect(mLastPoint, secondPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
}

void PaintArea::drawExpressive(const QPoint& nextPoint)
{
	if(mLayerArray[mActiveLayerNum].isSimulationLayer())
	{
		return;
	}

	//draw the scribble on mPaintPreviewImage to get a preview
	QPainter painter(&mPaintPreviewImage);

	if(mButton == 1 || mDeviceDown)
	{
		//left button pressed, painting with left color
		painter.setPen(QPen(mToolColorFG, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	}
	else if(mButton == 2)
	{
		//right button pressed, painting with right color
		painter.setPen(QPen(mToolColorBG, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	}
	painter.drawLine(mLastPoint, nextPoint);
	
	int rad = (mStrokeWidth / 2) + 2;
		
	if(mZoomCount != 0)
	{
		update(QRect(mLastPoint*mZoomFactor, nextPoint*mZoomFactor).normalized().adjusted(-mZoomFactor*rad, -mZoomFactor*rad, +mZoomFactor*rad, +mZoomFactor*rad));
	}
	else
	{
		update(QRect(mLastPoint, nextPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	}
	
	mPointList.append(mLastPoint);
	mLastPoint = nextPoint;
}

void PaintArea::fillContour(const QPoint& point, QColor interiorColor)
{
	if(mLayerArray[mActiveLayerNum].isSimulationLayer())
	{
		return;
	}
	else
	{
		QColor fillColor = mToolColorFG;
		if(mButton == 2)
		{
			//left button pressed
			fillColor = mToolColorBG;
		}

		QStack<QPoint> stack;
		stack.push(point);
		QPoint actualPoint;

 		if(interiorColor == fillColor)
		{
			return;
		}

		while(!stack.isEmpty())
		{
			actualPoint = stack.pop();
			QColor actualColor = mLayerArray[mActiveLayerNum].getImageRef().pixel(actualPoint);

			if( actualColor == interiorColor) 
			{     
				mLayerArray[mActiveLayerNum].getImageRef().setPixel(actualPoint, fillColor.rgba());
				if(actualPoint.y() < mPaintImage.height()-1) stack.push(QPoint(actualPoint.x(), actualPoint.y() + 1));
				if(actualPoint.y() > 0) stack.push(QPoint(actualPoint.x(), actualPoint.y() - 1));
				if(actualPoint.x() < mPaintImage.width()-1) stack.push(QPoint(actualPoint.x() + 1, actualPoint.y()));
				if(actualPoint.x() > 0) stack.push(QPoint(actualPoint.x() - 1, actualPoint.y()));
			}
		}
		update(0, 0, mPaintImage.width(), mPaintImage.height());
	}
}

void PaintArea::resizeImagesAndLayers( QSize size )
{	
	//the offset is the area where you can paint out of the shown frame
	int offset = 500;

	resizeBackgroundImage(&mBackgroundImage, size);
	resizeImage(&mPaintImage, size);
	resizeImage(&mPaintPreviewImage, size);
	resizeImage(&mViewImage, size);

	for( int i = 0; i < mLayerArraySize; i++ )
	{
		if(i != mActiveLayerNum)
		{
			Layer newLayer = mLayerArray[i];
			newLayer.resize(size);
			mLayerArray[i] = newLayer;
			mLogFile << "resizeEvent(): mLayerList's size is " << mLayerArraySize << " \n";
		}
		else
		{
			mLayerArray[mActiveLayerNum].resize(size);
		}
	}
	mOriginalSize = size;
	mZoomSize = mOriginalSize;
	update();
	mLogFile << "resizeEvent(): mLayerList's size is " << mLayerArraySize << " \n";
}

void PaintArea::resizeImage( QImage* image, const QSize& newSize )
{
	//function (re)paints the image, it is also called when the PaintArea is being created
	if(image->size() == newSize)
	{
		return;
	}

	QImage newImage(newSize, QImage::Format_ARGB32);
	//set the newImage to transparent
	newImage.fill(qRgba(0, 0, 0, 0));

	//draw old image in the new image with the new size
	QPainter viewPainter(&newImage);
		viewPainter.drawImage(0, 0, *image);
	viewPainter.end();

	//old image = new Image
    *image = newImage;
}

void PaintArea::resizeBackgroundImage( QImage* image, const QSize& newSize )
{
	//function (re)paints the image, it is also called when the PaintArea is being created
    if(image->size() == newSize)
	{
        return;
	}

    QImage newImage(newSize, QImage::Format_ARGB32);
	QBrush bgPatternBrush;
	bgPatternBrush.setTexture(mBGpattern);

    QPainter painter(&newImage);
		painter.setBrush(bgPatternBrush);
		painter.drawRect( 0, 0, newImage.width(), newImage.height() );
	painter.end();

    *image = newImage;
}

bool PaintArea::openImage( const QString& fileName )
{
	QImage newImage(this->width(), this->height(), QImage::Format_ARGB32);
	//clear new image
	newImage.fill(qRgba(0, 0, 0, 0));

	QImage loadedImage;
	if(!loadedImage.load(fileName))
	{
		return false;
	}

	QSize newSize = loadedImage.size().expandedTo(size());

	if(loadedImage.width() <= newImage.width() && loadedImage.height() <= newImage.height())
	{
		//paint loadedImage on newImage
		QPainter viewPainter(&newImage);
		viewPainter.drawImage(0, 0, loadedImage);
		viewPainter.end();
	}
	else
	{
		QErrorMessage* tooLargeImageLoaded = new QErrorMessage(this);
		tooLargeImageLoaded->setForegroundRole(QPalette::Dark);
		tooLargeImageLoaded->showMessage("The loaded image was too large for this document! \nResize your document's size or decrease loaded images size. ");
		tooLargeImageLoaded->show();
	}

	//resizeImage(&image, this->size());

	//create new layer from new image
	addLayer();

	update();
	return true;
}

bool PaintArea::saveImage( QString& fileName, const char* fileFormat )
{
	
	mLogFile << "saveImageFunction called! fileFormat = " << fileFormat << " \n" <<endl;
	update();

	if(fileFormat == QString("bmp") || fileFormat == QString("jpg") || fileFormat == QString("jpeg") || fileFormat == QString("ppm"))
	{
		//put a white image under mViewImage
		QImage whiteImage(mViewImage.width(), mViewImage.height(), QImage::Format_ARGB32);
		whiteImage.fill(qRgba(255, 255, 255, 255));

		QPainter savePainter(&whiteImage);
			savePainter.setCompositionMode(QPainter::CompositionMode_SourceOver);
			savePainter.drawImage(0, 0, mViewImage);
		savePainter.end();

		mViewImage = whiteImage;
	}

	if(!fileName.contains(fileFormat))
	{
		fileName += ".";
		fileName += fileFormat;
	}

	if(mViewImage.save(fileName, fileFormat)) 
	{
		mLogFile << "saveImage(): Successful saved " << fileFormat << " format. \n";
        mModified = false;
        return true;
    }
	else 
	{
        return false;
    }
}

bool PaintArea::setActiveLayer( int activeLayerNum )
{
	//std::cout << mActiveLayerNum << std::endl;
	mLogFile << "setActiveImage(): Active Image is now " << mActiveLayerNum << "! \n";

	mActiveLayerNum = mLayerArraySize - 1 - activeLayerNum;

	cout << "ActiveLayerNum: " << mActiveLayerNum << endl;

	if(mLayerArray[mActiveLayerNum].isSimulationLayer())
	{
		return true;
	}
	else
	{
		return false;
	}
}

void PaintArea::addLayer()
{
	mLogFile << "addLayer(): PaintArea's addLayer() called! \n";

	if(mLayerArraySize < mMaxLayerArraySize)
	{
		//create new image
		QImage image = QImage(0, 0, QImage::Format_ARGB32);
		//resize new image
		resizeImage(&image, mPaintImage.size());
		//clear new image
		image.fill(qRgba(0, 0, 0, 0));
		//create new layer from new image
		Layer newLayer(image, false);
		//insert new layer at queue's head
		mActiveLayerNum = mLayerArraySize;
		mLayerArraySize++;
		mLayerArray[mActiveLayerNum] = newLayer;
		
		mLogFile << "addLayer(): Image List's size is now " << mLayerArraySize << " \n";
	}
}

void PaintArea::addSimulationLayer()
{
	mLogFile << "addSimulationLayer(): PaintArea's addSimulationLayer() called! \n";

	if(mLayerArraySize < mMaxLayerArraySize)
	{
		//create new image
		QImage image = QImage(0, 0, QImage::Format_ARGB32);
		//resize new image
		resizeImage(&image, mPaintImage.size());
		//clear new image
		image.fill(qRgba(0, 0, 0, 0));
		//create new layer from new image
		Layer newLayer(image, mPatternImage, mPaperViewBrush, true);
		//insert new layer at queue's head
		mActiveLayerNum = mLayerArraySize;
		mLayerArraySize++;
		mLayerArray[mActiveLayerNum] = newLayer;

		update();

		mLogFile << "addLayer(): Image List's size is now " << mLayerArraySize << " \n";

		mSimulating = true;
	}
}

void PaintArea::restoreLayer()
{
	mLogFile << "restoreLayer(): PaintArea's addLayer() called! \n";

	if(mLayerArraySize < mMaxLayerArraySize)
	{
		//create new layer from top of restore layer if stack is not empty
		if(!mRestoreStack->isEmpty())
		{
			//take top layer from restore stack
			if( mRestoreStack->isEmpty() )
			{
				mLogFile << "fail" << std::endl;
			}
			else
			{
				Layer newLayer = mRestoreStack->pop();
				//resize image from restored layer
				resizeImage(&newLayer.getImageRef(), this->size());
				//insert new layer at queue's head
				mActiveLayerNum = mLayerArraySize;
				mLayerArraySize++;
				mLayerArray[mActiveLayerNum] = newLayer;
				
				update();
			}
		}

		mLogFile << "restoreLayer(): Image List's size is now " << mLayerArraySize << " \n";
	}
}

void PaintArea::deleteActiveLayer()
{
	mLogFile << "deleteActiveImage(): " << mActiveLayerNum << " deleted! \n";
	
	if(mActiveLayerNum != -1)
	{
		mRestoreStack->push(mLayerArray[mActiveLayerNum]);
		
		for(int i = mActiveLayerNum; i+1 < mLayerArraySize; i++)
		{
			mLayerArray[i] = mLayerArray[i+1];
		}
		mLayerArraySize--;
		if(mLayerArraySize == 0)
		{
			setActiveLayer(-1);
		}
		else
		{
			setActiveLayer(0);
		}

		mLogFile << "deleteActiveImageLayer(): Image List's size is now " << mLayerArraySize << " \n";
		update();
	}
}

void PaintArea::raiseActualLayer()
{
	if(mLayerArray[mActiveLayerNum].isSimulationLayer())
	{
		mLogFile << "Simulation Layer cannot be moved!" << std::endl;
		return;
	}

	mLogFile << "raiseActualLayer() called! \n";
	if( mActiveLayerNum == mLayerArraySize-1 )
	{
		mLogFile << "raiseActualLayer(): top layer cannot be raised! \n";
	}
	else
	{
		Layer storeLayer = mLayerArray[mActiveLayerNum];
		mLayerArray[mActiveLayerNum] = mLayerArray[mActiveLayerNum+1];
		mLayerArray[mActiveLayerNum+1] = storeLayer;

		mActiveLayerNum++;
		update();

		mLogFile << "raiseActualLayer(): Active Image is now " << mActiveLayerNum << "! \n";
	}
}

void PaintArea::lowerActualLayer()
{
	if(mLayerArray[mActiveLayerNum].isSimulationLayer())
	{
		mLogFile << "Simulation Layer cannot be moved!" << std::endl;
		return;
	}

	mLogFile << "lowerActualLayer() called! \n";
	if( mActiveLayerNum == 0 )
	{
		mLogFile << "lowerActualLayer(): bottom layer cannot be lowered! \n";
	}
	else
	{
		Layer storeLayer = mLayerArray[mActiveLayerNum];
		mLayerArray[mActiveLayerNum] = mLayerArray[mActiveLayerNum-1];
		mLayerArray[mActiveLayerNum-1] = storeLayer;
		
		mActiveLayerNum--;
		update();
		
		mLogFile << "lowerActualLayer(): Active Image is now " << mActiveLayerNum << "! \n";
	}
}

void PaintArea::setLayerVisability(int layer, bool visible)
{
	//mLogFile << "setLayerVisability() entered! Layer " << layer << "s visibility is now " << visible << ". \n";

	layer = mLayerArraySize - 1 - layer;
	mLayerArray[layer].setVisibility(visible);
	
	mLogFile << "setLayerVisability() entered! Layer " << layer << "s visibility is now " << mLayerArray[mActiveLayerNum].getVisibility() << ". \n";
	update();
}

void PaintArea::setLayerOpacity( int opacity )
{
	mLayerArray[mActiveLayerNum].setOpacity(opacity);
	update();
}

int PaintArea::getLayerOpacity()
{
	return mLayerArray[mActiveLayerNum].getOpacity();
}

void PaintArea::setLayersMixingMode(QPainter::CompositionMode mixingMode)
{
	mLayerArray[mActiveLayerNum].setMixingMode(mixingMode);

	mLogFile << "setLayersMixingMode() entered! Layer " << mActiveLayerNum << "s mixingMode is now: " << mixingMode << ". \n";
	update();
}

int PaintArea::getLayersMixingMode()
{
	QPainter::CompositionMode mixingMode = mLayerArray[mActiveLayerNum].getMixingMode();	

	switch(mixingMode)
	{
		case QPainter::CompositionMode_SourceOver:
			return 0;
		break;
		
		case QPainter::CompositionMode_Plus:
			return 1;
		break;
		
		case QPainter::CompositionMode_Difference:
			return 2;
		break;
		
		case QPainter::CompositionMode_Multiply:
			return 3;
		break;
		
		case QPainter::CompositionMode_Screen:
			return 4;
		break;
		
		case QPainter::CompositionMode_Darken:
			return 5;
		break;
		
		case QPainter::CompositionMode_Lighten:
			return 6;
		break;
				
		case QPainter::CompositionMode_ColorDodge:
			return 7;
		break;
				
		case QPainter::CompositionMode_ColorBurn:
			return 8;
		break;
				
		case QPainter::CompositionMode_HardLight:
			return 9;
		break;
				
		case QPainter::CompositionMode_SoftLight:
			return 10;
		break;
		
		default: 
			return -1;
	}
}

void PaintArea::smoothingFilter(int radius)
{
	mPaintImage = mLayerArray[mActiveLayerNum].getImageRef();
	mPaintPreviewImage = mPaintImage;

	int red = 0; 
	int green = 0; 
	int blue = 0; 
	int alpha = 0;
	QColor color(0, 0, 0, 0);

	int fm1, fm2;

	for(int i = 0; i < mPaintImage.height(); i++)
	{
		for(int j = 0; j < mPaintImage.width(); j++)
		{
			for(int f1 = -radius; f1 <= radius; f1++)
			{
				for(int f2 = -radius; f2 <= radius; f2++)
				{
					fm1 = f1;
					fm2 = f2;
					if(j+f1 < 0 || j+f1 >= mPaintImage.width())
					{
						fm1 = -f1;
					}
					if(i+f2 < 0 || i+f2 >= mPaintImage.height())
					{
						fm2 = -f2;
					}
					color = mPaintPreviewImage.pixel(j+fm1, i+fm2);
					red += color.red();
					green += color.green(); 
					blue += color.blue();
					QRgb pixel = mPaintPreviewImage.pixel(j+fm1, i+fm2);
					alpha += qAlpha(pixel);
				}
			}	
			red /= (2*radius+1)*(2*radius+1);
			green /= (2*radius+1)*(2*radius+1);
			blue /= (2*radius+1)*(2*radius+1);
			alpha /= (2*radius+1)*(2*radius+1);
			color.setRed(red);
			color.setGreen(green);
			color.setBlue(blue);
			color.setAlpha(alpha);
			mPaintImage.setPixel(j, i, color.rgba());
			//color = mPaintImage.pixel(j, i);
			red = 0;
			green = 0; 
			blue = 0;
			alpha = 0;
		}
	}

	//store the old image for undo function
	if(mLayerArray[mActiveLayerNum].getUndoStackCount() == mUndoNumber)
	{
		mLayerArray[mActiveLayerNum].pushBackToUndoStack();
	}
	else
	{
		mLayerArray[mActiveLayerNum].pushToUndoStack();
	}
	mLayerArray[mActiveLayerNum].setImage(mPaintImage);

	mPaintImage.fill(qRgba(0, 0, 0, 0));
	mPaintPreviewImage.fill(qRgba(0, 0, 0, 0));
	//mouseReleaseEvent(NULL);
	update();
}

void PaintArea::stackMerge()
{
	/*
	int red = 0; 
	int green = 0; 
	int blue = 0; 
	int alpha = 0;
	QColor color(0, 0, 0, 0);

	QStringList inputList1 = mStackInput1Dir.entryList(QDir::AllEntries);
	inputList1.pop_front();
	inputList1.pop_front();
	QStringList inputList2 = mStackInput2Dir.entryList(QDir::AllEntries);
	inputList2.pop_front();
	inputList2.pop_front();

	for(int i = 0; i < inputList1.size(); i++)
	{
		QString fileName("C:/Users/Jessi/Documents/Visual Studio 2010/Projects/ScratchUp - scrollarea/images/stackInput1/");
		fileName.append(inputList1.at(i));
		if(fileName.contains(".tga"))
		{
			fileName.chop(4);
		}
		QImage I1 = QImage();
		resizeImage(&I1, this->size());
		if(!I1.load(fileName))
		{
			QErrorMessage* tooLargeImageLoaded = new QErrorMessage(this);
			tooLargeImageLoaded->setForegroundRole(QPalette::Dark);
			tooLargeImageLoaded->showMessage("Image could not be loaded. ");
			tooLargeImageLoaded->show();
		}

		QImage I2 = QImage();
		//resizeImage(&I2, this->size());
		fileName = "C:/Users/Jessi/Documents/Visual Studio 2010/Projects/ScratchUp - scrollarea/images/stackInput2/";
		fileName.append(inputList2.at(i));
		if(fileName.contains(".tga"))
		{
			fileName.chop(4);
		}
		if(!I2.load(fileName))
		{
			QErrorMessage* tooLargeImageLoaded = new QErrorMessage(this);
			tooLargeImageLoaded->setForegroundRole(QPalette::Dark);
			tooLargeImageLoaded->showMessage("Image could not be loaded. ");
			tooLargeImageLoaded->show();
		}
		else
		{
			// comment this
			QImage OutputImage = QImage(I2.width(), I2.height(), I2.format());
			OutputImage.fill(qRgb(255, 255, 255));
			for(int i = 0; i < OutputImage.height(); i++)
			{
				for(int j = 0; j < OutputImage.width(); j++)
				{
					color = I2.pixel(j, i);
					OutputImage.setPixel(j, i, color.rgb());
				}
			}
			// until here

			QPainter painter(&I2);
				painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
				painter.drawImage(0, 0, I1);
			painter.end();

			fileName = "C:/Users/Jessi/Documents/Visual Studio 2010/Projects/ScratchUp - scrollarea/images/stackOutput/";
			fileName.append("test");
			fileName.append( QString("%1").arg(i) );
			fileName.append(".png");

			const char* fileFormat = "png";

			if(I2.save(fileName, fileFormat)) 
			{

			}
			else 
			{
				mLogFile << "I2.width = " << I2.width() << ", height = " << I2.height() << "\n";
				QErrorMessage* tooLargeImageLoaded = new QErrorMessage(this);
				tooLargeImageLoaded->setForegroundRole(QPalette::Dark);
				tooLargeImageLoaded->showMessage("Saving output image failed! ");
				tooLargeImageLoaded->show();
			}
		}

	}
	*/
}

void PaintArea::escape()
{
	if(mSplineMove)
	{
		mLogFile << "Esc key pressed! \n" << endl;
	
		mSplineMove = false;

		mPaintImage = mLayerArray[mActiveLayerNum].getImageRef().copy();
		QPainter painter(&mPaintImage);
		painter.setOpacity(mStrokeOpacity / 100.0);
		painter.setCompositionMode(mLayerArray[mActiveLayerNum].getMixingMode());

		painter.drawImage(0, 0, mPaintPreviewImage);
			
		//store the old image for undo function
		if(mLayerArray[mActiveLayerNum].getUndoStackCount() == mUndoNumber)
		{
			mLayerArray[mActiveLayerNum].pushBackToUndoStack();
		}
		else
		{
			mLayerArray[mActiveLayerNum].pushToUndoStack();
		}
		mLayerArray[mActiveLayerNum].setImage(mPaintImage);
	
		mPaintPreviewImage.fill(qRgba(0, 0, 0, 0));
		update();
	}
}

#define QT_NO_PRINTER 1
void PaintArea::print()
{
#ifndef QT_NO_PRINTER
	QPrinter printer(QPrinter::HighResolution);

	QPrintDialog *printDialog = new QPrintDialog(&printer, this);
	if (printDialog->exec() == QDialog::Accepted) 
	{

		resizeImage( &mViewImage, size() );

		QPainter viewPainter(&mViewImage);
		viewPainter.setCompositionMode(QPainter::CompositionMode_SourceOver);
		float layerOpacity = 1.0;

		mViewImage.fill(qRgba(255, 255, 255, 255));
		//draw all layers in the right order
		for( int i = mLayerArraySize-1; i >= 0 ; i-- )
		{
			if(mLayerArray[i].getVisibility() == true)
			{	
				viewPainter.setCompositionMode(mLayerArray[i].getMixingMode());
				layerOpacity = mLayerArray[i].getOpacity() / 100.0;
				viewPainter.setOpacity(layerOpacity);
				viewPainter.drawImage(0, 0, mLayerArray[i].getImageRef());
			}
		}

        QPainter painter(&printer);
        QRect rect = painter.viewport();
        QSize size = mViewImage.size();
        size.scale(rect.size(), Qt::KeepAspectRatio);
        painter.setViewport(rect.x(), rect.y(), size.width(), size.height());
        painter.setWindow(mViewImage.rect());
        painter.drawImage(0, 0, mViewImage);
    }
#endif // QT_NO_PRINTER
}

