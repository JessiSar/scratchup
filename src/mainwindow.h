#pragma once

#include <QtWidgets>
#include <iostream>
#include <fstream>
#include <qdir.h>

#include "paintarea.h"
#include "layerswindow.h"
#include "toolsettings.h"

//#include "ui_scratchup.h"

class PaintArea;

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(PaintArea* paintArea);
	~MainWindow();

	bool eventFilter(QObject *obj, QEvent *event);

	void setStatusMessage(QString message);

	void setTool(int toolNumber);
	void pressureWidthCBoxClicked(bool checked);
	void pressureColorCBoxClicked(bool checked);
	void tiltWidthCBoxClicked(bool checked);
	void tiltColorCBoxClicked(bool checked);

	void setActiveLayer(int activeLayer);
	void changeLayerVisibility(int layer, bool visible);
	void raiseActualLayer();
	void lowerActualLayer();

	void addLayer();
	void addSimulationLayer();
	void restoreLayer();
	void deleteActualLayer();
	void deactivateSizeButtons();
	void activateSizeButtons();

	void setmFeltpenColorW(QColor newColor);
	void setmFeltpenColorB(QColor newColor);

private:
	//Ui::ScratchUpClass ui;
	std::ofstream mLogFile;
	
	QScrollArea* mScrollArea;
	PaintArea* mPaintArea;

	ToolSettings* mToolSettings;
	LayersWindow* mLayersWindow;

	QToolButton* mNewDocumentButton;
	QToolButton* mSaveButton;
	QToolButton* mZoomInButton;
	QToolButton* mZoomOutButton;
	QToolButton* mResetSizeButton;
	QToolButton* mUndoButton;
	QToolButton* mRedoButton;
	QLabel* mUndoLabel;
	QSpinBox* mUndoSpinBox;
	QToolButton* mIncrWidthButton;
	QToolButton* mDecrWidthButton;
	QToolButton* mIncrHeightButton;
	QToolButton* mDecrHeightButton;
	QLabel* mSizeLabel;
	QSpinBox* mSizeSpinBox;
	QToolBar* mToolBar;

	QAction* newAction;
	QAction* openAction;
	QAction* saveAsAction;
	QAction* saveAction;
	QAction* printAct;
	QAction* exitAction;
	QAction* stackMergeAction;

	QAction* toolSettingsAction;
	QAction* layersWindowAction;

	QAction* smoothingFilterAction;
 
	QMenu* fileMenu;
	QMenu* viewMenu;
	QMenu* filterMenu;
	QMenu* stackMenu;

	QString mFileName; 
	QString mFileFormat;

	int mZoomCount;

	void MainWindow::keyPressEvent( QKeyEvent* evt );



public slots:
	void undo();
	void redo();
	void escButtonClicked();
	void setFeltpenWetness(int wetness);

private slots:
	void newDocument();
	void open();
	bool save();
	bool saveAs();
	void print();
	void quit();
	
	void showToolSettings();
	void showLayersWindow();

	void smoothingFilter();

	void stackMerge();

	void setToolWidth(int toolWidth);
	void setToolOpacity(int toolOpacity);
	void setToolColorFG();
	void setmFeltpenColorDialogW();
	void setmFeltpenColorDialogB();

	void setToolColorBG();

	void setUndoNumber(int undoNumber);
	
	void setLayerOpacity(int opacity);
	void mixingModeChanged(int index);

	//toolbar slots
	void zoomIn();
	void zoomOut(); 
	void resetSize(); 
	
	void incrHeight();
	void incrWidth();
	void decrHeight();
	void decrWidth();

};
