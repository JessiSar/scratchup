#pragma once

#include <qpixmap.h>
#include <QBrush>

#include "layer.h"
#include "papercell.h"

class SimulationLayer : public Layer
{

public:
	SimulationLayer(bool isSimulationLayer);
	SimulationLayer(QImage image, bool isSimulationLayer);
	SimulationLayer(QImage image, QImage heightImage, QImage viewImage, QBrush paperViewBrush, bool isSimulationLayer);
	SimulationLayer(int width, int height, QImage::Format format, QImage heightImage, QImage viewImage, QBrush paperViewBrush, bool isSimulationLayer);
	~SimulationLayer();

	QImage& getInkImageRef();
	void addInk(QImage inkImage);
	void doCapillaryFlow();
	bool isSimulating();
	bool isSimulationLayer();

private:
	QImage mHeightImage;
	QImage mCapacityImage;
	QImage mInkImage;
	QBrush mHeightBrush; 
	QBrush mViewBrush;

	bool mSimulating;
	bool mIsSimulationLayer;
};

