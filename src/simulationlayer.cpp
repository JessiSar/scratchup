#include "simulationlayer.h"

SimulationLayer::SimulationLayer(bool isSimulationLayer)
{
	mOpacity = 100;
	mVisible = true;
	mMixingMode = QPainter::CompositionMode_SourceOver;
	mIsSimulationLayer = isSimulationLayer;
}

SimulationLayer::SimulationLayer(QImage image, bool isSimulationLayer)
{
	mOpacity = 100;
	mVisible = true;
	mMixingMode = QPainter::CompositionMode_SourceOver;
	mImage = image;
	mIsSimulationLayer = isSimulationLayer;
}

SimulationLayer::SimulationLayer(QImage image, QImage heightImage, QImage viewImage, QBrush paperViewBrush, bool isSimulationLayer)
{
	mImage = image;
	mOpacity = 100;
	mVisible = true;
	mMixingMode = QPainter::CompositionMode_SourceOver;
	mIsSimulationLayer = isSimulationLayer;

	mHeightImage = heightImage;
	mCapacityImage = heightImage;
	mInkImage = QImage(heightImage.width(), heightImage.height(), QImage::Format_ARGB32);

    QPainter painter(&mImage);
		painter.setBrush(paperViewBrush);
		painter.drawRect( 0, 0, mImage.width(), mImage.height() );
	painter.end();
}

SimulationLayer::SimulationLayer(int width, int height, QImage::Format format, QImage heightImage, QImage viewImage, QBrush paperViewBrush, bool isSimulationLayer)
{
	mImage = QImage(width, height, format);

	mOpacity = 100;
	mVisible = true;
	mMixingMode = QPainter::CompositionMode_SourceOver;
	mIsSimulationLayer = isSimulationLayer;

	mHeightImage = heightImage;
	mCapacityImage = heightImage;
	mInkImage = QImage(heightImage.width(), heightImage.height(), QImage::Format_ARGB32);

    QPainter painter(&mImage);
		painter.setBrush(paperViewBrush);
		painter.drawRect( 0, 0, mImage.width(), mImage.height() );
	painter.end();

	mSimulating = false;
}

SimulationLayer::~SimulationLayer()
{
	
}

QImage& SimulationLayer::getInkImageRef()
{
	return mInkImage;
}

void SimulationLayer::addInk(QImage inkImage)
{
	mSimulating = true;

	QPainter inkPainter(&mInkImage);
		inkPainter.drawImage(0, 0, inkImage);
	inkPainter.end();
}

void SimulationLayer::doCapillaryFlow()
{

}

bool SimulationLayer::isSimulating()
{
	return mSimulating;
}

bool SimulationLayer::isSimulationLayer()
{
	return mIsSimulationLayer;
}