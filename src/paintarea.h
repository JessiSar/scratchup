#pragma once

#include <QTabletEvent>

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <qlist.h>
#include <qtimer.h>
#include <qerrormessage.h>
#include <qdir.h>
#include <iostream>
#include <fstream>
#include <vector>

#include "layer.h"

class PaintArea : public QWidget
{
Q_OBJECT

public:
	PaintArea();
	~PaintArea();

	void resizeImagesAndLayers(QSize size);
	void newProject();

	void zoomIn();
	void zoomOut();
	void resetSize();

	void setUndoNumber(int undoNumber);

	void setTabletDevice(QTabletEvent::TabletDevice device);
	void tabletEvent(QTabletEvent* event);

	bool openImage(const QString& fileName);
	bool saveImage(QString& fileName, const char* fileFormat);

	void setTool(int toolNumber);
	void setToolWidth(int n);
	void setToolOpacity(int n);
	void setFeltpenWetness(int wetness);
	void setPressureWidth(bool checked);
	void setPressureColor(bool checked);
	void setTiltWidth(bool checked);
	void setTiltColor(bool checked);
	void setToolColorFG(const QColor& newColor);
	void setToolColorBG(const QColor& newColor);
	void setKubelkaColorW(const QColor& newColor);
	void setKubelkaColorB(const QColor& newColor);
	void setNewOriginalSize(QSize newSize);

	void smoothingFilter(int radius);
	void stackMerge();

	void addLayer();
	void addSimulationLayer();
	void restoreLayer();
	void deleteActiveLayer();
	bool setActiveLayer(int activeLayerNum);
	void raiseActualLayer();
	void lowerActualLayer();
	void setLayerVisability(int layer, bool visible);
	void setLayerOpacity(int opacity);
	int getLayerOpacity();
	void setLayersMixingMode(QPainter::CompositionMode mixingMode);
	int getLayersMixingMode();

	void undo();
	void redo();
	
	QColor getToolColorFG() const { return mToolColorFG; }
	QColor getToolColorBG() const { return mToolColorBG; }

	QColor getKubelkaColorW() const { return mKubelkaColorW; }
	QColor getKubelkaColorB() const { return mKubelkaColorB; }

	void escape();
	void print();

public slots:

protected:

private:
	void computeKubelkaValues();
    void drawLineTo(const QPoint& endPoint);
	void drawPoint(const QPoint& newPoint);
	void drawFeltPenLine(QTabletEvent* evt);
	void drawFeltPenPoint();
	void drawEraserLineTo(const QPoint& endPoint);
	void drawEraserPoint(const QPoint& newPoint);
	void drawLine(const QPoint& secondPoint);
	void drawSpline(const QPoint& secondPoint);
	void drawRect(const QPoint& secondPoint);
	void drawEllipse(const QPoint& secondPoint);
	void drawExpressive(const QPoint& secondPoint);
	void fillContour(const QPoint& newPoint, QColor interiorColor);
    void resizeImage(QImage* image, const QSize& newSize);
	void resizeBackgroundImage(QImage* image, const QSize& newSize);
	void mousePressEvent(QMouseEvent* evt);
    void mouseMoveEvent(QMouseEvent* evt);
    void mouseReleaseEvent(QMouseEvent* evt);
    void paintEvent(QPaintEvent* evt);

	//button will be 0 if no button is pressed, 1 for left and 2 for right button 
	int mButton;
	int mTool;
	//zoom variable that holds negative value for counting zoomOuts and positive for counting zoomIns
	int mZoomCount;
	//zoom factor variable holds the factor for zooming in and out
	float mZoomFactor;
	//unzoom factor variable to adjust zoom in coordinates
    double mUnZoomFactor;
	//zoomRect for QImage's scaled() function
	QSize mZoomSize;
	QSize mOriginalSize;

	bool mPressureWidth;
	bool mPressureColor;
	bool mTiltWidth;
	bool mTiltColor;
	std::ofstream mLogFile;

	QTabletEvent::PointerType mPointerType;
	QTabletEvent::TabletDevice mTabletDevice;

    bool mModified;
    bool mPainting;
	bool mTransparency;
	bool mSplineMove;
	bool mDeviceDown;

	QPoint mSplinePoint1;
	QPoint mSplinePoint2;
	QPoint mSplinePoint3;

	int mFeltpenWetness;
    int mStrokeWidth;
	int mStrokeWidthTablet;
	int mStrokeOpacity;
    QColor mToolColorFG;
	QColor mToolColorBG;
	QColor mKubelkaColorW;
	QColor mKubelkaColorB;
	QColor mTabletPenColor;

	Layer* mStoreLayerP;
	QImage mBackgroundImage;
	QImage mViewImage;
	QImage mPaintImage;
	QImage mPaintPreviewImage;
	QStack<QImage> mUndoStack;
	QList<QPoint> mPointList;

	Layer mLayerArray[30];
	int mActiveLayerNum;
	int mLayerArraySize;
	int mMaxLayerArraySize;

	QPixmap mBGpattern;
	//var for saving last tool position
    QPoint mLastPoint;
	//vars for saving last tablet event entities
	QPoint mLastFeltPenPos;
	float mLastFeltPenPressure;
	int mLastFeltPenXtilt;
	int mLastFeltPenYtilt;
	QPoint mActualFeltPenPos;
	float mActualFeltPenPressure;
	int mActualFeltPenXtilt;
	int mActualFeltPenYtilt;

	//the paintArea's layer list
	QStack<Layer>* mRestoreStack;
	int mUndoNumber;

	//images for simulation layers
	QImage mPatternImage;
	QImage mViewPaperImage;
	QBrush mPaperViewBrush;

	//timer member object for continously activating the capillary flow simulation
	QTimer* mSimulationTimer;

	QDir mStackInput1Dir; 
	QDir mStackInput2Dir; 
	QDir mStackOutputDir;

	bool mSimulating;

	//kubelka munk properties
	float mRw_r;
	float mRw_g;
	float mRw_b;
	float mRb_r;
	float mRb_g;
	float mRb_b;

	float mS_r; 
	float mS_g; 
	float mS_b; 
	float mK_r;
	float mK_g;
	float mK_b;

	float ma_r; 
	float ma_g; 
	float ma_b; 
	float mb_r;
	float mb_g;
	float mb_b;
	float mc_r;
	float mc_g;
	float mc_b;

	float mR_r;
	float mR_g;
	float mR_b;
	float mT_r;
	float mT_g;
	float mT_b;

private slots:

	void simulationFunc();
	
};
