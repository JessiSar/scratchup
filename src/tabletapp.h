#ifndef TABLETAPPLICATION_H
#define TABLETAPPLICATION_H

#include <QApplication>
#include "paintarea.h"

class TabletApplication : public QApplication
{
Q_OBJECT

public:
    TabletApplication(int& argv, char** args) : QApplication(argv, args) {}

    bool event(QEvent* evt);
    void setCanvas(PaintArea* paintArea);

private:
    PaintArea* mPaintArea;
};

#endif
