#pragma once

#include <qpixmap.h>

#include "layer.h"
#include "papercell.h"
#include <qpainter.h>
#include <qstack.h>
#include <iostream>

#include <qtimer.h>
#include <qthread.h>

class Layer : public QObject
{
Q_OBJECT

public:
	Layer();
	Layer(const Layer& rhs);
	Layer(bool isSimulationLayer);
	Layer(QImage image, bool isSimulationLayer);
	Layer(QImage image, QImage heightImage, QBrush paperViewBrush, bool isSimulationLayer);
	Layer(int width, int height, QImage::Format format, QImage heightImage, QBrush paperViewBrush, bool isSimulationLayer);
	~Layer();

	Layer& operator= (const Layer& rhs);

	void setImage(QImage image);
	QImage& getImageRef();

	bool mIsSimulationLayer;
	bool mDeviceDown;
	void setOpacity(int opacity);
	int getOpacity();

	void setVisibility(bool visable);
	bool getVisibility();

	void newStroke();
	void endStroke();
	
	void setMixingMode(QPainter::CompositionMode mixingMode);
	QPainter::CompositionMode getMixingMode();

	void updateCells(QPoint upLeft, QPoint downRight, int addWater, QColor contourColor);
	void undo();
	void redo();
	void clearRedoStack();
	void pushToUndoStack();
	void pushBackToUndoStack();
	int getUndoStackCount(); 
	int mUndoCount;
	int mWetnessFactor;
    uint16_t mLastUserUpdate;

	void resize(QSize size);

	void setInkImage(QImage image);
	void setWetnessFactor(int wetnessFactor);
	QImage& getInkImageRef();
	QImage& getAddInkImageRef_R();
	QImage& getAddInkImageRef_T();
	QImage& getPatternImageRef();
	void doCapillaryFlow();
	bool isSimulating();
	bool isSimulationLayer();

	Cell*** mCellArray;

	void setKubelkaColorVars(float mK_r, float mK_g, float mK_b, 
							 float mS_r, float mS_g, float mS_b, 
							 float ma_r, float ma_g, float ma_b, 
							 float mb_r, float mb_g, float mb_b);

	//kubelka munk properties
	float mRw_r;
	float mRw_g;
	float mRw_b;
	float mRb_r;
	float mRb_g;
	float mRb_b;

	float mS_r; 
	float mS_g; 
	float mS_b; 
	float mK_r;
	float mK_g;
	float mK_b;

	float ma_r; 
	float ma_g; 
	float ma_b; 
	float mb_r;
	float mb_g;
	float mb_b;
	float mc_r;
	float mc_g;
	float mc_b;

	float mR_r;
	float mR_g;
	float mR_b;
	float mT_r;
	float mT_g;
	float mT_b;

protected:
    int mOpacity;
    bool mVisible;
    QPainter::CompositionMode mMixingMode;
    QImage mImage;


private:

	int mStrokeNumber;
	QStack<QImage> mUndoStack;
	QStack<QImage> mRedoStack;

	QBrush mPaperViewBrush;

	QImage* mPatternImage;
	QImage* mAddInkImage_R;
	QImage* mAddInkImage_T;
	QImage* mInkImage;
	QImage* mDriedInkImage;

	bool mSimulating;
	std::vector<Cell*> mWetCells;
	std::vector<Cell*> mWetNeighbCells;
	std::vector<Cell*> mWetNeighbNeighbCells;
	std::vector<Cell*> mMarkedToDryCells;
	std::vector<Cell*> mActualStrokeCells;
	std::vector<std::pair< int, std::vector<Cell*>> > mStrokeCells;
	std::vector<QTimer*> mDryTimers;
	int mDryingSpeed;
	QTimer* mSimulationTimer;
	QTimer* mDryTimer;

	QThread* mSimulationThread;

	std::ofstream mLogFile;

private slots:
	void timerFunction();
	void startSimulationThread();
	void dryCells();
};

