#include "mainwindow.h"
#include "tabletapp.h"

int main(int argc, char *argv[])
{
	 TabletApplication app(argc, argv);
     PaintArea* paintArea = new PaintArea();
     app.setCanvas(paintArea);

     MainWindow mainWindow(paintArea);
     mainWindow.show();

     return app.exec();
}
