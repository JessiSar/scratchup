#pragma once

#include <QWidget>
#include <qgridlayout.h>
#include <qpushbutton.h>
#include <qgroupbox.h>
#include <qslider.h>
#include <qlabel.h>
#include <qspinbox.h>
#include <qdockwidget.h>
#include <qsplitter.h>
#include <qtoolbutton.h>
#include <qcheckbox.h>
#include <qimage.h>

#include <iostream>
#include <fstream>

class ToolSettings : public QWidget
{
Q_OBJECT

public:
	ToolSettings(QWidget *parent = 0);
	~ToolSettings();
	
public slots:
	void setForegroundButtonColor( QColor color );
	void setBackgroundButtonColor( QColor color );
	void setmKubelkaColorW( QColor color );
	void setmKubelkaColorB( QColor color );

	void feltPenButtonClicked();
	void hideOpacitySlide();
	void showOpacitySlide();

	
private:

	QVBoxLayout* mToolWindowLayout;

	QSplitter* mToolWindowSplitter;
	QWidget* mToolsWidget;
	QWidget* mToolOptionsWidget;

	QGridLayout* mToolsLayout;
	QVBoxLayout* mToolSettingsLayout;

	QHBoxLayout* mToolWidthLayout;
	QHBoxLayout* mToolOpacityLayout;
	QHBoxLayout* mTabletLayout;
	QHBoxLayout* mToolAdditionLayout;
	QHBoxLayout* mColorLayout;
	QHBoxLayout* mSimulationLayout;
	QVBoxLayout* mFeltPenColorLayout;
	QGridLayout* mFeltPenChoseColorLayout;
	QVBoxLayout* mFeltPenCustomColorLayout;
	QHBoxLayout* mFeltPenChoseCustomColorLayout;
	QHBoxLayout* mCustomColorVis;

	QVBoxLayout* mChoseColorLayout;
	QToolButton* mFeltPenColorA;
	QToolButton* mFeltPenColorB;
	QToolButton* mFeltPenColorC;
	QToolButton* mFeltPenColorD;
	QToolButton* mFeltPenColorE;
	QToolButton* mFeltPenColorF;
	QToolButton* mFeltPenColorG;
	QToolButton* mFeltPenColorH;
	QToolButton* mFeltPenColorI;
	QToolButton* mFeltPenColorJ;
	QToolButton* mFeltPenColorK;
	QToolButton* mFeltPenColorL;
	
	QGroupBox* mToolWidthGB;
	QGroupBox* mToolOpacityGB;
	QGroupBox* mFeltpenSimulationGB;
	QGroupBox* mTabletGB;
	QGroupBox* mToolAdditionGB;
	QGroupBox* mToolColorGB;
	QGroupBox* mFeltPenToolColorGB;
	QGroupBox* mKubelkaColorGB;

	QToolButton* mPenButton;
	QToolButton* mFeltPenButton;
	QToolButton* mEraserButton; 
	QToolButton* mFillButton; 
	QToolButton* mExpressiveButton;
	QToolButton* mLineButton;
	QToolButton* mSplineButton;
	QToolButton* mRectButton; 
	QToolButton* mEllipseButton; 

	QCheckBox* mPressureWidth;
	QCheckBox* mPressureOpacity;
	QCheckBox* mPressureColor;
	QCheckBox* mTiltWidth;
	QCheckBox* mTiltOpacity;
	QCheckBox* mTiltColor;
	QSlider* mWetnesSlider;
	QSpinBox* mWetnesSpinBox;

	QPushButton* mForegroundColor;
	QPushButton* mBackgroundColor;
	QPushButton* mKubelkaColorW;
	QPushButton* mKubelkaColorB;
	QPushButton* mEscButton;

	QSlider* mToolWidthSlider;
	QSpinBox* mToolWidthSpinBox;

	QSlider* mToolOpacitySlider;
	QSpinBox* mToolOpacitySpinBox;
	
	QPixmap mKubelkaPixmap;
	QLabel mKubelkaLabel;

	QColor mKubelkaW;
	QColor mKubelkaB;

	std::ofstream mLogFile;

private slots:
	void penButtonClicked();
	void eraserButtonClicked();
	void fillButtonClicked();
	void exprButtonClicked();
	void lineButtonClicked();
	void splineButtonClicked();
	void rectButtonClicked();
	void ellipseButtonClicked();

	void setButtonsUnchecked();

	void feltPenColorAClicked();
	void feltPenColorBClicked();
	void feltPenColorCClicked();
	void feltPenColorDClicked();
	void feltPenColorEClicked();
	void feltPenColorFClicked();
	void feltPenColorGClicked();
	void feltPenColorHClicked();
	void feltPenColorIClicked();
	void feltPenColorJClicked();
	void feltPenColorKClicked();
	void feltPenColorLClicked();

	void setPenColorsUnchecked();

	void pressureWidthCBoxClicked(int n);
	void pressureColorCBoxClicked(int n);
	void tiltWidthCBoxClicked(int n);
	void tiltColorCBoxClicked(int n);
	
	void drawFeltPenLine();

    void quit();
};

