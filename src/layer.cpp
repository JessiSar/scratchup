#include "layer.h"
#include <algorithm>

std::ofstream mLogFile;

Layer::Layer()
{
	mOpacity = 100;
	mWetnessFactor = 100;
	mStrokeNumber = 0;
	mVisible = true;
	mDeviceDown = false;
	mMixingMode = QPainter::CompositionMode_SourceOver;
	mUndoCount = 0;
	mLastUserUpdate = 0;
	mIsSimulationLayer = false;

	mDryTimers = std::vector<QTimer*>();

	mSimulationTimer = new QTimer();
	mSimulationTimer->setInterval(500);
	mSimulationTimer->setSingleShot(false);
	connect(mSimulationTimer, SIGNAL(timeout()), this, SLOT(timerFunction()));

	mSimulationThread = new QThread(this);
	mSimulationThread->setPriority(QThread::IdlePriority);
	connect(mSimulationThread, SIGNAL(started()), this, SLOT(startSimulationThread()));
}

Layer::Layer(const Layer& rhs) // Üblicherweise rhs: "Right Hand Side"
{
	this->mImage = rhs.mImage;

	mOpacity = rhs.mOpacity;
	mVisible = rhs.mVisible;
	mMixingMode = rhs.mMixingMode;
	mUndoStack = rhs.mUndoStack;
	mRedoStack = rhs.mRedoStack;

	mPatternImage = rhs.mPatternImage;
	mInkImage = rhs.mInkImage;
	mAddInkImage_R = rhs.mAddInkImage_R;
	mAddInkImage_T = rhs.mAddInkImage_T;
	mDriedInkImage = rhs.mDriedInkImage;

	mSimulating = rhs.mSimulating;
	mIsSimulationLayer = rhs.mIsSimulationLayer;

	Cell*** mCellArray = rhs.mCellArray;
}

Layer::Layer(bool isSimulationLayer = false)
{
	mOpacity = 100;
	mVisible = true;
	mMixingMode = QPainter::CompositionMode_SourceOver;
	mIsSimulationLayer = isSimulationLayer;

	//initialize PaintAreas logfile
	mLogFile.open("LayerLog.txt");
	mLogFile << "Layer's logfile created! \n";
}

Layer::Layer(QImage image, bool isSimulationLayer = false)
{
	mOpacity = 100;
	mVisible = true;
	mMixingMode = QPainter::CompositionMode_SourceOver;
	mImage = image;
	mIsSimulationLayer = isSimulationLayer;

	//initialize PaintAreas logfile
	mLogFile.open("LayerLog.txt");
	mLogFile << "Layer's logfile created! \n";
}

Layer::Layer(QImage image, QImage patternImage, QBrush paperViewBrush, bool isSimulationLayer = true)
{
	mPaperViewBrush = paperViewBrush;

	mImage = image;
	mOpacity = 100;
	mVisible = true;
	mMixingMode = QPainter::CompositionMode_SourceOver;
	mIsSimulationLayer = isSimulationLayer;

	if(mIsSimulationLayer)
	{
		mAddInkImage_R = new QImage(image);
		mAddInkImage_T = new QImage(image);
		mDriedInkImage = new QImage(image);
		QImage viewPatternImage = QImage(image);
		mPatternImage = &patternImage;
		
		QPainter painter(&viewPatternImage);
			painter.setPen(QPen(Qt::transparent, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
			painter.setBrush(paperViewBrush);
			painter.drawRect( 0, 0, viewPatternImage.width(), viewPatternImage.height() );
		painter.end();
		
		mInkImage = new QImage(viewPatternImage);

		//to do: change mImage to InkImage when layers get resizable
		mCellArray = new Cell**[mImage.height()];
		for(int i = 0; i < mImage.height(); i++) 
		{
			mCellArray[i] = new Cell*[mImage.width()];
		}

		for(int i = 0; i < mImage.height(); i++)
		{
			for(int j = 0; j < mImage.width(); j++)
			{
				mCellArray[i][j] = new Cell();
			}
		}
		
		mWetCells = std::vector<Cell*>();
		mWetNeighbCells = std::vector<Cell*>();
		mWetNeighbNeighbCells = std::vector<Cell*>();
		mMarkedToDryCells = std::vector<Cell*>();

		QColor color;
		unsigned char* inkDataP = mInkImage->bits();
		unsigned char* rAddedInkDataP = mAddInkImage_R->bits();
		unsigned char* tAddedInkDataP = mAddInkImage_T->bits();
		unsigned char* driedInkDataP = mDriedInkImage->bits();

		for(int i = 0; i < mImage.height(); i++)
		{
			for(int j = 0; j < mImage.width(); j++)
			{
				QColor viewColor = viewPatternImage.pixel(j, i);
				//kubelka munk vars
				(mCellArray[i][j])->mR_b = (viewColor.blue() / 255.0) * (float(viewColor.blue()) / 255.0);
				(mCellArray[i][j])->mR_g = (viewColor.blue() / 255.0) * (float(viewColor.green()) / 255.0);
				(mCellArray[i][j])->mR_r = (viewColor.blue() / 255.0) * (float(viewColor.red()) / 255.0);
				(mCellArray[i][j])->mT_b = 1.0;
				(mCellArray[i][j])->mT_g = 1.0;
				(mCellArray[i][j])->mT_r = 1.0;
				//dried vars
				(mCellArray[i][j])->mDried_R_b = (viewColor.blue() / 255.0) * (float(viewColor.blue()) / 255.0);
				(mCellArray[i][j])->mDried_R_g = (viewColor.blue() / 255.0) * (float(viewColor.green()) / 255.0);
				(mCellArray[i][j])->mDried_R_r = (viewColor.blue() / 255.0) * (float(viewColor.red()) / 255.0);
				(mCellArray[i][j])->mDried_T_b = 1.0;
				(mCellArray[i][j])->mDried_T_g = 1.0;
				(mCellArray[i][j])->mDried_T_r = 1.0;
				
				if(i == 0 || j == 0 || i == mImage.height()-1 || j == mImage.width()-1)
				{
					(mCellArray[i][j])->mIsMarginalCell = true;
					(mCellArray[i][j])->mHeight = 1.0;
				}
				else
				{
					color = mPatternImage->pixel(j, i);
					unsigned short height = color.red();

					(mCellArray[i][j])->mHeight = float(height) / 255.0;
					(mCellArray[i][j])->mIsMarginalCell = false;

					int position = i * mImage.width() + j;
					(mCellArray[i][j])->mColor = &inkDataP[4 * position];
					(mCellArray[i][j])->mAddedColor_R = &rAddedInkDataP[4 * position];
					(mCellArray[i][j])->mAddedColor_T = &tAddedInkDataP[4 * position];
				
					(mCellArray[i][j])->mNeighbCells.push_back(mCellArray[i-1][j-1]);
					(mCellArray[i][j])->mNeighbCells.push_back(mCellArray[i][j-1]);
					(mCellArray[i][j])->mNeighbCells.push_back(mCellArray[i+1][j-1]);
					(mCellArray[i][j])->mNeighbCells.push_back(mCellArray[i+1][j]);
					(mCellArray[i][j])->mNeighbCells.push_back(mCellArray[i+1][j+1]);
					(mCellArray[i][j])->mNeighbCells.push_back(mCellArray[i][j+1]);
					(mCellArray[i][j])->mNeighbCells.push_back(mCellArray[i-1][j+1]);
					(mCellArray[i][j])->mNeighbCells.push_back(mCellArray[i-1][j]);
				}
			}
		}
	}
}

Layer::Layer(int width, int height, QImage::Format format, QImage patternImage, QBrush paperViewBrush, bool isSimulationLayer)
{
	mImage = QImage(width, height, format);

	mOpacity = 100;
	mVisible = true;
	mMixingMode = QPainter::CompositionMode_SourceOver;
	mIsSimulationLayer = isSimulationLayer;

	mPatternImage = &patternImage;

    QPainter painter(&mImage);
		painter.setBrush(paperViewBrush);
		painter.drawRect( 0, 0, mImage.width(), mImage.height() );
	painter.end();

	mSimulating = false;
}

Layer::~Layer()
{
/*
	if(mIsSimulationLayer)
	{
		if(mSimulationTimer->isActive())
		{
			mSimulationTimer->stop();
		}
		delete mSimulationTimer;
	}
	
	if(mIsSimulationLayer)
	{
		for(int i = 0; i < mImage.height(); i++) 
		{
			delete[] mCellArray[i];
		}
		delete[] mCellArray;	
	}
	*/
}

Layer& Layer::operator= (const Layer& rhs)
{
	if(this != &rhs) // protect against invalid self-assignment
	{
		this->mImage = rhs.mImage;

		mOpacity = rhs.mOpacity;
		mVisible = rhs.mVisible;
		mMixingMode = rhs.mMixingMode;
		mUndoStack = rhs.mUndoStack;
		mRedoStack = rhs.mRedoStack;

		mPatternImage = rhs.mPatternImage;
		mAddInkImage_R = rhs.mAddInkImage_R;
		mAddInkImage_T = rhs.mAddInkImage_T;
		mInkImage = rhs.mInkImage;
		mDriedInkImage = rhs.mDriedInkImage;

		mSimulating = rhs.mSimulating;
		mIsSimulationLayer = rhs.mIsSimulationLayer;

		mCellArray = rhs.mCellArray;
	}
	// by convention, always return *this
	return *this;
}

void Layer::setImage( QImage image )
{
	mImage = image;
}

void Layer::setInkImage( QImage image )
{
	mInkImage = &image;
}

QImage& Layer::getImageRef()
{
	return mImage;
}

QImage& Layer::getPatternImageRef(){
	return *mPatternImage;
}
	
void Layer::setOpacity(int opacity)
{
	mOpacity = opacity;
}

int Layer::getOpacity()
{
	return mOpacity;
}

void Layer::setVisibility(bool visible)
{
	mVisible = visible;
}

void Layer::setWetnessFactor(int wetnessFactor)
{
	mWetnessFactor = wetnessFactor;

	for(int i = 0; i < mImage.height(); i++)
	{
		for(int j = 0; j < mImage.width(); j++)
		{
			(mCellArray[i][j])->mWetnessFactor = wetnessFactor;
		}
	}
}

bool Layer::getVisibility()
{
	return mVisible;
}

void Layer::setMixingMode(QPainter::CompositionMode mixingMode)
{
	mMixingMode = mixingMode;
}

QPainter::CompositionMode Layer::getMixingMode()
{
	return mMixingMode;
}

void Layer::newStroke()
{
	mActualStrokeCells = std::vector<Cell*>();
	mStrokeNumber++;

	pair< int, std::vector<Cell*>> p(mStrokeNumber, mActualStrokeCells);
	mStrokeCells.push_back(p);
	mActualStrokeCells = std::vector<Cell*>();

	cout << "new stroke. stroke number " << mStrokeNumber << endl;
}

void Layer::endStroke()
{
	QTimer* dryTimer = new QTimer();
	dryTimer->setInterval(30 * mWetnessFactor);
	dryTimer->setSingleShot(true);
	connect(dryTimer, SIGNAL(timeout()), this, SLOT(dryCells()));
	dryTimer->start();

	mDryTimers.push_back(dryTimer);
}

void Layer::updateCells(QPoint upLeft, QPoint downRight, int addWater, QColor contourColor)
{
	if(!mIsSimulationLayer)
	{
		return;
	}

	if(addWater < -1 || upLeft.x() < 0 || upLeft.y() < 0 || downRight.x() >= mImage.width() || downRight.y() >= mImage.height())
	{
		//randbeandlung - clipping
		return;
	}

	if(addWater == -1)
	{
		for(int i = upLeft.y(); i <= downRight.y(); i++)
		{
			for(int j = upLeft.x(); j <= downRight.x(); j++)
			{
				unsigned char cellBlue = (mCellArray[i][j])->mColor[0];
				unsigned char cellGreen = (mCellArray[i][j])->mColor[1];
				unsigned char cellRed = (mCellArray[i][j])->mColor[2];

				unsigned char contBlue = contourColor.blue();
				unsigned char contGreen = contourColor.green();
				unsigned char contRed = contourColor.red();

				if(cellBlue == contBlue && cellGreen == contGreen && cellRed == contRed)
				{
					(mCellArray[i][j])->mIsContour = true;
					//(mCellArray[i][j])->mColor[0] = 0;
					//(mCellArray[i][j])->mColor[1] = 0;
					//(mCellArray[i][j])->mColor[2] = 0;
				}
			}
		}
	}
	else
	{
		for(int i = upLeft.y(); i <= downRight.y(); i++)
		{
			for(int j = upLeft.x(); j <= downRight.x(); j++)
			{
				if(!(mCellArray[i][j])->mIsContour)
				{
					if(mWetnessFactor > 15)
					{
						(mCellArray[i][j])->update(addWater, false, true);
						if((mCellArray[i][j])->mIsInWetCells)
						{
							mStrokeCells.back().second.push_back(mCellArray[i][j]);
							mCellArray[i][j]->mStrokeNumber = mStrokeNumber;
						}
					}
					else
					{
						(mCellArray[i][j])->update(addWater, false, false);
					}
				}
			}
		}
	}

	if(!mSimulationThread->isRunning())
	{
		mSimulationThread->start();
	}
}

void Layer::startSimulationThread()
{
	mSimulationTimer->start();
}

void Layer::setKubelkaColorVars(float K_r, float K_g, float K_b, 
								float S_r, float S_g, float S_b, 
								float a_r, float a_g, float a_b, 
								float b_r, float b_g, float b_b)
{
	for(int i = 0; i < mImage.height(); i++)
	{
		for(int j = 0; j < mImage.width(); j++)
		{
			(mCellArray[i][j])->mK_r = K_r;
			(mCellArray[i][j])->mK_g = K_g;
			(mCellArray[i][j])->mK_b = K_b;

			(mCellArray[i][j])->mS_r = S_r;
			(mCellArray[i][j])->mS_g = S_g;
			(mCellArray[i][j])->mS_b = S_b;

			(mCellArray[i][j])->ma_r = a_r;
			(mCellArray[i][j])->ma_g = a_g;
			(mCellArray[i][j])->ma_b = a_b;

			(mCellArray[i][j])->mb_r = b_r;
			(mCellArray[i][j])->mb_g = b_g;
			(mCellArray[i][j])->mb_b = b_b;
		}
	}
}

void Layer::dryCells()
{
	for(int i = 0; i < mStrokeCells.front().second.size(); i++)
	{
		if(mStrokeCells.front().second[i]->mGotInk && mStrokeCells.front().second[i]->mStrokeNumber == mStrokeCells.front().first)
		{
			mStrokeCells.front().second[i]->dry();
		}
	}
	mStrokeCells.erase(mStrokeCells.begin());
}

void Layer::timerFunction()
{
	std::vector<Cell*> mWetNeighbCells1 = std::vector<Cell*>();
	std::vector<Cell*> mWetNeighbCells2 = std::vector<Cell*>();

	for(int k = 0; k < mStrokeCells.size(); k++)
	{
		for( int i = 0; i < mStrokeCells[k].second.size(); i++)
		{	
			mStrokeCells[k].second[i]->doMeanFlowWater( mWetNeighbCells1, true );
		}
		mStrokeCells[k].second.insert(mStrokeCells[k].second.end(), mWetNeighbCells1.begin(), mWetNeighbCells1.end());

		for(int j = 0; j < 10; j++)
		{
			bool capillary = true;
			if( j+1 == 10)
			{
				capillary = false;
			}

			if( j % 2 != 0)
			{
				for( int i = 0; i < mWetNeighbCells1.size(); i++)
				{	
					mWetNeighbCells1[i]->doMeanFlowWater( mWetNeighbCells2, capillary );
				}

				mStrokeCells[k].second.insert(mStrokeCells[k].second.end(), mWetNeighbCells2.begin(), mWetNeighbCells2.end());
				mWetNeighbCells1.erase(mWetNeighbCells1.begin(), mWetNeighbCells1.end());
			}
			else
			{
				for( int i = 0; i < mWetNeighbCells2.size(); i++)
				{	
					mWetNeighbCells2[i]->doMeanFlowWater( mWetNeighbCells1, capillary );
				}
				mStrokeCells[k].second.insert(mStrokeCells[k].second.end(), mWetNeighbCells1.begin(), mWetNeighbCells1.end());
				mWetNeighbCells2.erase(mWetNeighbCells2.begin(), mWetNeighbCells2.end());
			}
		}

		for( int i = mStrokeCells[k].second.size()-1; i >= 0; i--)
		{	
			mStrokeCells[k].second[i]->doMeanFlow( mMarkedToDryCells );
			//mStrokeCells[k][i]->mColor[0] = 0;
		}

		if(mStrokeCells[k].second.size() == 0 && mWetNeighbCells.size() == 0)
		{
			if(mSimulationThread->isRunning())
			{
				mSimulationThread->terminate();
			}
			if(mSimulationTimer->isActive())
			{
				mSimulationTimer->stop();
			}
		}
		else
		{
			mSimulationTimer->setInterval(50000 / mWetnessFactor);
		}
	}
}

void Layer::undo()
{
	if(!mUndoStack.isEmpty())
	{
		if(mIsSimulationLayer)
		{
			// to do: to implemente this, each changed cell has to be saved... 
		}
		else
		{
			mRedoStack.push(mImage);
			mImage.fill(qRgba(0, 0, 0, 0));
			QPainter painter(&mImage);
				painter.drawImage(0, 0, mUndoStack.pop());
			painter.end();
		}
		mUndoCount--;
	}
}

void Layer::redo()
{
	if(!mRedoStack.isEmpty())
	{
		if(mIsSimulationLayer)
		{
			mUndoStack.push(*mInkImage);
			QPainter painter(mInkImage);
				painter.drawImage(0, 0, mRedoStack.pop());
			painter.end();
		}
		else
		{
			mUndoStack.push(mImage);
			mImage.fill(qRgba(0, 0, 0, 0));
			QPainter painter(&mImage);
				painter.drawImage(0, 0, mRedoStack.pop());
			painter.end();
		}
		mUndoCount++;
	}
}

void Layer::clearRedoStack()
{
	if(!mRedoStack.isEmpty())
	{
		mRedoStack.clear();
	}
}

void Layer::pushToUndoStack()
{
	if(mIsSimulationLayer)
	{
		mUndoStack.push(*mInkImage);	
	}
	else
	{
		mUndoStack.push(mImage);
	}
	mUndoCount++;
}

void Layer::pushBackToUndoStack()
{
	if(!mUndoStack.isEmpty())
	{
		mUndoStack.pop_front();
	}

	if(mIsSimulationLayer)
	{
		mUndoStack.push(*mInkImage);
	}
	else
	{
		mUndoStack.push(mImage);
	}
}

int Layer::getUndoStackCount()
{
	return mUndoCount;
}

void Layer::resize(QSize newSize)
{
	//function (re)paints the image, it is also called when the PaintArea is being created
	if(mImage.size() == newSize)
	{
		return;
	}

	QImage newImage(newSize, QImage::Format_ARGB32);
	//set the newImage to transparent
	newImage.fill(qRgba(0, 0, 0, 0));

	QPainter viewPainter(&newImage);
		viewPainter.drawImage(0, 0, mImage);
	viewPainter.end();

	//old image = new Image
    mImage = newImage;
}

QImage& Layer::getInkImageRef()
{
	return *mInkImage;
}

QImage& Layer::getAddInkImageRef_R()
{
	return *mAddInkImage_R;
}

QImage& Layer::getAddInkImageRef_T()
{
	return *mAddInkImage_T;
}

void Layer::doCapillaryFlow()
{
	
}

bool Layer::isSimulating()
{
	return mSimulating;
}

bool Layer::isSimulationLayer()
{
	return mIsSimulationLayer;
}