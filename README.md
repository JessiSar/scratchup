# scratchup

Interactive painting software.

## Build

* `cmake -B build -G Ninja`
* `ninja -C build`

## Dependencies

* Qt5
